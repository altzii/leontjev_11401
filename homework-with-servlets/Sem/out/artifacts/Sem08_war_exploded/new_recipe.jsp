<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 16/11/15
  Time: 04:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<%@ include file="navbar.jsp" %>

<div id="wrapper">
    <section>
        <div class="center-block">
            <div align="center">
                <%if (request.getSession().getAttribute("current_user") != null) { %>
                <form action="add_recipe" method="post">
                    <p align="center"><b>Добавьте свой рецепт:</b></p>

                    <div class="col-sm-9" style="width:40%">
                        <input type="text" class="form-control" name="name"
                               placeholder="Название рецепта" required maxlength="100">
                    </div>

                    <p><textarea rows="8" cols="70" name="short_description" placeholder="Краткое описание"
                                 required></textarea></p>

                    <div class="col-sm-9" style="width:61%">
                        <input type="text" class="form-control" name="image"
                               placeholder="Ссылка на фото блюда" required>
                    </div>

                    <p><textarea rows="10" cols="70" name="ingredients" placeholder="Ингридиенты" required></textarea>
                    </p>

                    <p><textarea rows="15" cols="70" name="desciption" placeholder="Описание рецепта"
                                 required></textarea></p>

                    <p><input type="submit" class="btn btn-warning btn-lg btn-block" value="Добавить рецепт"
                              style="margin:15px 0 0 15px; width: 35%"></p>
                </form>

                <%} else { %>

                <div class="alert alert-warning " align="center" style="margin-bottom:10px"><h2>Чтобы добавлять рецепты,
                    пройдите регистрацию/залогиньтесь</h2>
                </div>

                <%}%>
            </div>

        </div>
    </section>
</div>


</body>
</html>
