<%@ page import="ru.kpfu.itis.alexander_leontjev.models.Recipe" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="ru.kpfu.itis.alexander_leontjev.repository.RecipesRepository" %>
<%@ page import="ru.kpfu.itis.alexander_leontjev.repository.UsersRepository" %>
<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 12/10/15
  Time: 22:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Рецепты</title>
</head>
<body>
<%@ include file="navbar.jsp" %>

<% if (request.getParameter("count") != null) {
    int count = Integer.parseInt(request.getParameter("count"));
    ArrayList<Recipe> recipes = RecipesRepository.getRecipes();
    int k = recipes.size();
    int maxPage = k % 6 == 0 ? k / 6 : k / 6 + 1;
    int maxId;
    if (count != maxPage) {
        maxId = count * 6;
    } else {
        if (recipes.size() % 6 != 0) {
            maxId = recipes.size() % 6 + (count - 1) * 6;
        } else {
            maxId = recipes.size();
        }
    }

    if (count > maxPage) {
        response.sendRedirect("/error404");
    } else {%>


<div id="wrapper">
    <section>
        <div class="row">
            <div align="center"><img
                    src="/images/recipes_title.png">
            </div>
            <div align="right" style="margin-right: 20px;"><a href="/new_recipe"><img height="7%"
                    src="/images/add_recipe.png"></a>
            </div>

            <br>
        </div>

        <div>
            <% for (int i = (count - 1) * 6; i < maxId; i++) {
                Recipe recipe = recipes.get(i);%>

            <div class="well"
                 style="height: auto; overflow: hidden; background-image: url(http://static.hdw.eweb4.com/media/wallpapers_dl/1/123/1221331-blur.jpg);">
                <div class="col-xs-6 col-md-3" style="float: left;">
                    <img width="270px" height="180px" class="img-thumbnail" border="3" src="<%=recipe.getImage()%>"
                         alt="...">
                </div>
                <div class="thumbnail" style="margin-left: 300px; padding:7px 30px; height: auto; overflow: hidden;">
                    <div class="caption">
                        <h3 align="center"><%=recipe.getName()%>
                        </h3>
                        <hr>

                        <p align="justify "><%=recipe.getShortDescription()%>
                        </p>


                        <div align="right"><a href="#"
                                              class="btn btn-default"
                                              role="button"
                                              data-toggle="modal"
                                              data-target=".bs-example-modal-lg<%=recipe.getId()%>">Посмотреть
                            рецепт</a></div>


                    </div>


                    <div class="modal fade bs-example-modal-lg<%=recipe.getId()%>" tabindex="-1" role="dialog"
                         aria-labelledby="myLargeModalLabel">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h2 align="center" class="modal-title" id="myModalLabel"><%=recipe.getName()%>
                                    </h2>

                                    <h4 align="center">
                                        <font style="font-style: italic">Автор: </font>
                                        <%=UsersRepository.getUserById(recipe.getAuthor()).getName()%>
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <img width="50%" src="<%=recipe.getImage()%>">
                                </div>
                                <hr>
                                <div class="modal-body">
                                    <table class="table table-bordered" style="background-color: ghostwhite">
                                        <tbody>
                                        <%for (String ingredient : recipe.getIngredientsList()) {%>
                                        <tr>
                                            <td><%=ingredient%>
                                            </td>

                                        </tr>
                                        <%}%>
                                        </tbody>
                                    </table>
                                </div>
                                <hr>


                                <div class="modal-body">
                                    <table class="table table-bordered" style="background-color: ghostwhite">
                                        <tr>
                                            <td>
                                                <%=recipe.getDescription()%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <% }%>


        </div>
        <%if (maxPage != 1) {%>

        <ul class="pagination">
            <% if (count - 1 != 0) {
                if (count - 2 != -1 && count - 2 != 0) {
                    if (1 != count - 2) {%>
            <li><a href="/recipes?count=1">&laquo;</a></li>
            <%}%>
            <li><a href="/recipes?count=<%=count-2%>"><%=count - 2%>
            </a></li>
            <%}%>
            <li><a href="/recipes?count=<%=count-1%>"><%=count - 1%>
            </a></li>
            <%}%>
            <li><a href="/recipes?count=<%=count%>"><%=count%>
            </a></li>
            <%if (maxPage >= count + 1) {%>
            <li><a href="/recipes?count=<%=count + 1%>"><%=count + 1%>
            </a></li>
            <%if (maxPage >= count + 2) {%>
            <li><a href="/recipes?count=<%=count + 2%>"><%=count + 2%>
            </a></li>
            <%if (maxPage != count + 2) {%>
            <li><a href="/recipes?count=<%=maxPage%>">&raquo;</a></li>
            <%}%>
            <%}%>
            <%}%>
        </ul>

        <%}%>
    </section>
</div>
<% }
} else {
    response.sendRedirect("/recipes?count=1");
}
%>

</body>
</html>
