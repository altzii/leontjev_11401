<%@ page import="ru.kpfu.itis.alexander_leontjev.models.User" %>
<%@ page import="ru.kpfu.itis.alexander_leontjev.repository.UsersRepository" %>
<%@ page import="java.security.MessageDigest" %>
<%@ page import="java.security.NoSuchAlgorithmException" %>

<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 19/10/15
  Time: 16:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Регистрация</title>

</head>
<body>
<%@ include file="navbar.jsp" %>
<% request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    if (request.getMethod().equals("GET")) { %>

<div id="wrapper">
    <section>

        <div class="alert alert-warning" align="center" style="margin-bottom:10px">Пройдите регистрацию, чтобы
            воспользоваться полной функциональностю сайта!
        </div>
        <br>

        <div class="jumbotron" style="margin-top: -10px;">
            <form action="/signup" method="POST" class="form-horizontal"
                  onsubmit="return checkEmail() && checkLogin() && checkPassword() && equalsPasswords()"
                  style="margin:auto; width:400px ">
                <div class="form-group" style="margin:5px 0 5px 0">
                    <div class="col-sm-9" style="width:100%">
                        <input type="text" id="login" class="form-control" name="login"
                               placeholder="Логин (от 5 до 20 символов)"
                               onblur="checkLogin()" maxlength="20" required>
                    </div>
                </div>

                <div align="center"><h5 id="login_status"></h5></div>

                <div class="form-group" style="margin:5px 0 5px 0">
                    <div class="col-sm-9" style="width:100%">
                        <input type="text" class="form-control" name="name" placeholder="Имя" maxlength="16" required>
                    </div>
                </div>

                <div align="center"><h5 id="name_status"></h5></div>

                <div class="form-group" style="margin:5px 0 5px 0">
                    <div class="col-sm-9" style="width:100%">
                        <input type="email" id="email" class="form-control" name="email" placeholder="email"
                               onblur="checkEmail()" required>
                    </div>
                </div>

                <div align="center"><h5 id="email_status"></h5></div>

                <div class="form-group" style="margin:5px 0 5px 0">
                    <div class="col-sm-9" style="width:100%">
                        <input type="password" id="password" class="form-control" name="password"
                               placeholder="Придумайте пароль (от 5 до 50 симолов)" oninput="checkPassword()"
                               required>
                    </div>
                </div>

                <div align="center"><h5 id="password_status"></h5></div>

                <div class="form-group" style="margin:5px 0 5px 0">
                    <div class="col-sm-9" style="width:100%">
                        <input type="password" class="form-control" id="password_repeat" name="password_repeat"
                               placeholder="Повторите пароль еще раз" oninput="equalsPasswords()" required>
                    </div>
                </div>

                <div align="center"><h5 id="repeat_status"></h5></div>

                <p><input type="submit" class="btn btn-primary btn-lg btn-block" value="Зарегистрироваться!"
                          style="margin:15px 0 0 15px; width: 92.5%"></p>
            </form>
            <script type="application/javascript">
                checkEmail = function (request, response) {
                    var checked;
                    $.ajax({
                        url: "/checkemail",
                        data: {"q": $("#email").val()},
                        dataType: "json",
                        async: false,
                        success: function (response_data) {
                            var email = $("#email").val();
                            if (response_data.results.length > 0) {
                                checked = false;
                                $("#email_status").html("Адрес " + email + " уже зарегистрирован");
                            } else {
                                checked = true;
                                $("#email_status").html("Адрес " + email + " свободен");
                            }
                        }
                    });
                    return checked;
                };

                checkLogin = function (request, response) {
                    var checked;
                    var regexp = new RegExp('^([a-zA-Z]?[a-zA-Z\\d_\\-.]*[a-zA-Z\\d_\\-]){5,20}$');
                    $.ajax({
                        url: "/checklogin",
                        data: {"q": $("#login").val()},
                        dataType: "json",
                        async: false,
                        success: function (response_data) {
                            var login = $("#login").val();

                            if (!regexp.test(login)) {
                                checked = false;
                                $("#login_status").html("Некорректный ввод логина");
                            } else {
                                if (response_data.results.length > 0) {
                                    checked = false;
                                    $("#login_status").html("Логин " + login + " уже зарегистрирован");
                                } else {
                                    checked = true;
                                    $("#login_status").html("Логин " + login + " свободен");
                                }
                            }
                        }
                    });
                    return checked;
                };

                checkPassword = function () {
                    var password = $("#password").val();
                    var regexp = new RegExp('[а-яА-ЯёЁ]');
                    var regexp2 = new RegExp('^.{5,50}$');
                    var checked;

                    if (regexp2.test(password) && !regexp.test(password)) {
                        checked = true;
                        $("#password_status").html("");

                    } else {
                        checked = false;
                        $("#password_status").html("Пароль должен состоять не менее, чем из 5 симолов и не должен содержать символы кириллицы");

                    }
                    return checked;
                };


                equalsPasswords = function () {
                    var checked;
                    if ($("#password").val() != $("#password_repeat").val()) {
                        checked = false;
                        $("#repeat_status").html("Пароли не совпадают");
                    } else {
                        checked = true;
                        $("#repeat_status").html("");
                    }
                    return checked;
                };


            </script>
        </div>
    </section>
</div>

<%
    } else {
        User signup_user = new User(request.getParameter("login"), request.getParameter("name"), request.getParameter("email"), request.getParameter("password"));
        if (UsersRepository.signup(signup_user)) {
            response.sendRedirect("/login?status=from_signup");
        } else {
            response.sendRedirect("/signup");
        }
    }
%>


</body>
</html>
