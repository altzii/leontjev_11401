package ru.kpfu.itis.alexander_leontjev.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Alexander on 19/10/15.
 */
public class DatabaseConnection {
    static public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/sem", "alt", "itiskfu");
    }
}
