package ru.kpfu.itis.alexander_leontjev.models;

/**
 * Created by Alexander on 19/10/15.
 */
public class NutritionalSupplement {
    String number;
    String engName;
    String rusName;
    String status;
    String notation;

    public NutritionalSupplement(String number, String engName, String rusName, String status, String notation) {
        this.number = number;
        this.engName = engName;
        this.rusName = rusName;
        this.status = status;
        this.notation = notation;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public String getRusName() {
        return rusName;
    }

    public void setRusName(String rusName) {
        this.rusName = rusName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotation() {
        return notation;
    }

    public void setNotation(String notation) {
        this.notation = notation;
    }
}
