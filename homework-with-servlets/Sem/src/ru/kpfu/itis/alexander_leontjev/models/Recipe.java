package ru.kpfu.itis.alexander_leontjev.models;

import java.util.ArrayList;

/**
 * Created by Alexander on 19/10/15.
 */
public class Recipe {
    int id;
    int author;
    String name;
    String image;
    String shortDescription;
    String description;
    String ingredients;


    public Recipe(int author, String name, String image, String shortDescription, String description, String ingredients) {
        this.author = author;
        this.name = name;
        this.image = image;
        this.shortDescription = shortDescription;
        this.description = description;
        this.ingredients = ingredients;

    }

    public Recipe(int id, int author, String name, String image, String shortDescription, String description, String ingredients) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.image = image;
        this.shortDescription = shortDescription;
        this.description = description;
        this.ingredients = ingredients;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public ArrayList<String> getIngredientsList() {
        String ingredients = this.getIngredients();
        ArrayList<String> listOfIngredients = new ArrayList<String>();

        for (String ingredient : ingredients.split("\n")) {
            listOfIngredients.add(ingredient);
        }

        return listOfIngredients;

    }

}
