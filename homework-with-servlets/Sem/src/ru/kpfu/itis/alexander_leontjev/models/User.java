package ru.kpfu.itis.alexander_leontjev.models;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 19/10/15.
 */
public class User {
    int id;
    String login;
    String name;
    String email;
    String password;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String login, String password) {
        this.password = password;
        this.login = login;
    }

    public User(int id) {
        this.id = id;
    }


    public User(String login, String name, String email, String password) {
        this.login = login;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public static boolean validate(User user) {
        Pattern loginPattern = Pattern.compile("([a-zA-Z]?[a-zA-Z\\d_\\-.]*[a-zA-Z\\d_\\-]){5,16}");
        Pattern passwordPattern = Pattern.compile("[^а-яА-ЯёЁ]{5,50}");
        Pattern emailPattern = Pattern.compile("^([a-zA-Z0-9_-]+\\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*\\.[a-zA-Z]{2,6}$");

        Matcher matcherLogin = loginPattern.matcher(user.getLogin());
        Matcher matcherPassword = passwordPattern.matcher(user.getPassword());
        Matcher matcherEmail = emailPattern.matcher(user.getEmail());


        return matcherLogin.matches() && matcherPassword.matches() && matcherEmail.matches() && user.getName().length() <= 16 && user.getEmail().length() <= 80;
    }
}
