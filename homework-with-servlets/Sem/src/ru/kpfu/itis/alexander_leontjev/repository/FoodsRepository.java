package ru.kpfu.itis.alexander_leontjev.repository;

import ru.kpfu.itis.alexander_leontjev.config.DatabaseConnection;
import ru.kpfu.itis.alexander_leontjev.models.Food;
import ru.kpfu.itis.alexander_leontjev.models.Recipe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Alexander on 12/11/15.
 */
public class FoodsRepository {
    static Connection connection = null;
    public static ArrayList<Food> getFoodsWithCondition(String input) {
        ArrayList<Food> arrayList = new ArrayList<>();

        try {
            connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM foods " + input);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Food food = new Food();
                arrayList.add(food);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
