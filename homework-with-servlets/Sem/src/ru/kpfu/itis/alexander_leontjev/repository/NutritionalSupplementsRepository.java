package ru.kpfu.itis.alexander_leontjev.repository;

import ru.kpfu.itis.alexander_leontjev.config.DatabaseConnection;
import ru.kpfu.itis.alexander_leontjev.models.NutritionalSupplement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Alexander on 19/10/15.
 */
public class NutritionalSupplementsRepository {
    static Connection connection = null;

    public static ArrayList<NutritionalSupplement> getNutritionalSupplements() {
        ArrayList<NutritionalSupplement> arrayList = new ArrayList<>();

        try {
            connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM nutritional_supplements ORDER BY number");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                NutritionalSupplement nutritionalSupplement = new NutritionalSupplement(resultSet.getString("number"), resultSet.getString("eng_name"), resultSet.getString("rus_name"), resultSet.getString("status"), resultSet.getString("notation"));
                arrayList.add(nutritionalSupplement);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return arrayList;
    }
}
