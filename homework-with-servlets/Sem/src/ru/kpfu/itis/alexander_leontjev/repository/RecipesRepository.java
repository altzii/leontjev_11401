package ru.kpfu.itis.alexander_leontjev.repository;

import org.apache.commons.lang3.StringEscapeUtils;
import ru.kpfu.itis.alexander_leontjev.config.DatabaseConnection;
import ru.kpfu.itis.alexander_leontjev.models.Recipe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Alexander on 19/10/15.
 */
public class RecipesRepository {
    static Connection connection = null;

    public static ArrayList<Recipe> getRecipes() {
        ArrayList<Recipe> arrayList = new ArrayList<>();
        try {
            connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM recipes");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Recipe recipe = new Recipe(resultSet.getInt("id"), resultSet.getInt("user_id"), resultSet.getString("name"), resultSet.getString("image"),
                        resultSet.getString("short_description"), resultSet.getString("description"), resultSet.getString("ingredients"));
                arrayList.add(recipe);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static boolean addRecipe(Recipe recipe) {
        try {

            connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into recipes (user_id, name, image, short_description, description, ingredients) VALUES (?, ?, ?, ?, ?, ?)");

            preparedStatement.setInt(1, recipe.getAuthor());
            preparedStatement.setString(2, recipe.getName());
            preparedStatement.setString(3, recipe.getImage());
            preparedStatement.setString(4, recipe.getShortDescription());
            preparedStatement.setString(5, recipe.getDescription());
            preparedStatement.setString(6, recipe.getIngredients());
            preparedStatement.execute();

            connection.close();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
}
