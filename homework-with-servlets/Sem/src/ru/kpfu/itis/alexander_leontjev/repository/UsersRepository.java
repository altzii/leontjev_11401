package ru.kpfu.itis.alexander_leontjev.repository;

import org.apache.commons.codec.digest.DigestUtils;
import ru.kpfu.itis.alexander_leontjev.config.DatabaseConnection;
import ru.kpfu.itis.alexander_leontjev.models.User;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Alexander on 19/10/15.
 */
public class UsersRepository {
    public static int authorize(String login, String password) {
        String hash_password = DigestUtils.md5Hex(password);
        int status = -1;
        User user = getUserByLogin(login);

        if (user != null) {
            status = 0;
            if (user.getPassword().equals(hash_password)) {
                status = 1;
            }
        }
        return status;
    }

    public static boolean signup(User user) {
        if (User.validate(user)) {
            try {
                String hash_password = DigestUtils.md5Hex(user.getPassword());
                Connection connection = DatabaseConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("insert into users (login, name, email, password) VALUES (?, ?, ?, ?)");
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.setString(2, user.getName());
                preparedStatement.setString(3, user.getEmail());
                preparedStatement.setString(4, hash_password);
                preparedStatement.execute();
                return true;
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static User getUserByLogin(String login) {
        try {
            Connection connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE login=?");
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new User(resultSet.getString("login"), resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getIdByLogin(String login) {
        try {
            Connection connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE login=?");
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;

    }

    public static User getUserById(int id) {
        try {
            Connection connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE id=?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new User(resultSet.getString("login"), resultSet.getString("name"), resultSet.getString("email"), resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
