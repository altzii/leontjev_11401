package ru.kpfu.itis.alexander_leontjev.servlets;

import org.apache.commons.lang3.StringEscapeUtils;
import ru.kpfu.itis.alexander_leontjev.models.Recipe;
import ru.kpfu.itis.alexander_leontjev.models.User;
import ru.kpfu.itis.alexander_leontjev.repository.RecipesRepository;
import ru.kpfu.itis.alexander_leontjev.repository.UsersRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexander on 16/11/15.
 */
@WebServlet(name = "AddRecipeServlet")
public class AddRecipeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        String login = StringEscapeUtils.escapeHtml4((String) request.getSession().getAttribute("current_user"));
        String name = StringEscapeUtils.escapeHtml4(request.getParameter("name"));
        String image = StringEscapeUtils.escapeHtml4(request.getParameter("image"));
        String short_description = StringEscapeUtils.escapeHtml4(request.getParameter("short_description"));
        String description = StringEscapeUtils.escapeHtml4(request.getParameter("description"));
        String ingredients = StringEscapeUtils.escapeHtml4(request.getParameter("ingredients"));


        Recipe recipe = new Recipe(UsersRepository.getIdByLogin(login), name, image, short_description, description, ingredients);

        if (RecipesRepository.addRecipe(recipe)) {
            response.sendRedirect("/recipes");
        } else {
            response.sendRedirect("/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
