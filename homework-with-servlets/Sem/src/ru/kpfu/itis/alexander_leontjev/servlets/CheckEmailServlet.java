package ru.kpfu.itis.alexander_leontjev.servlets;

import ru.kpfu.itis.alexander_leontjev.config.DatabaseConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Alexander on 08/11/15.
 */
@WebServlet(name = "CheckEmailServlet")
public class CheckEmailServlet extends HttpServlet {
    Connection connection;

    public void init() {
        try {
            connection = DatabaseConnection.getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String q = request.getParameter("q");

        try {

            PreparedStatement ps = connection.prepareStatement("select email from users where email=?");
            ps.setString(1, q);
            ResultSet rs = ps.executeQuery();

            JSONArray ja = new JSONArray();

            while (rs.next()) {
                ja.put(rs.getString("email"));
            }

            JSONObject jo = new JSONObject();
            jo.put("results", ja);
            response.setContentType("text/json");
            response.getWriter().print(jo.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

