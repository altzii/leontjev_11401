package ru.kpfu.itis.alexander_leontjev.servlets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.kpfu.itis.alexander_leontjev.config.DatabaseConnection;
import ru.kpfu.itis.alexander_leontjev.models.Food;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Alexander on 12/11/15.
 */
@WebServlet(name = "FoodSearchServlet")
public class FoodSearchServlet extends HttpServlet {
    Connection connection;

    public void init() {
        try {
            connection = DatabaseConnection.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String q = request.getParameter("q");
        String food_type = request.getParameter("food_type");
        String kcal = request.getParameter("kcal");
        String condition = "";


        if (food_type != null) {
            if (food_type.equals("protein")) {
                condition = "AND protein > fat AND protein > carbohydrates";
            }

            if (food_type.equals("fat")) {
                condition = "AND fat > protein AND fat > carbohydrates";
            }

            if (food_type.equals("carbohydrates")) {
                condition = "AND carbohydrates > fat AND carbohydrates > protein";
            }
        } else {
            condition = "";
        }

        if (kcal != null && kcal.equals("true")) {
            condition += " AND kcal > 250";
        }


        try {
            PreparedStatement ps = connection.prepareStatement("select * from foods where name ilike ? " + condition);
            ps.setString(1, "%" + q + "%");
            ResultSet rs = ps.executeQuery();

            JSONArray ja = new JSONArray();

            while (rs.next()) {

                ja.put(rs.getString("name"));
                ja.put(rs.getDouble("protein"));
                ja.put(rs.getDouble("fat"));
                ja.put(rs.getDouble("carbohydrates"));
                ja.put(rs.getDouble("kcal"));
            }

            JSONObject jo = new JSONObject();
            jo.put("results", ja);
            response.setContentType("text/json");
            response.getWriter().print(jo.toString());

        } catch (SQLException | JSONException e) {
            e.printStackTrace();
            response.sendRedirect("/error404");
        }

    }
}
