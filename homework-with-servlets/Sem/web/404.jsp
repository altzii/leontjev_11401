<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 10/11/15
  Time: 00:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>404</title>
    <%@ include file="navbar.jsp" %>
</head>
<body>

<div id="wrapper">
    <section>
        <div align="center"><h1>404 Page Not Found</h1></div>
    </section>

    <div align="center"><img src="images/404.jpg"></div>

</div>

</body>
</html>
