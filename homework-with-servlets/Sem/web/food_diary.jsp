<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 11/11/15
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Дневник питания</title>

</head>
<body>
<%@ include file="navbar.jsp" %>

<div id="wrapper">
    <section>
        <%
            String current_user = (String) request.getSession().getAttribute("current_user");
            if (current_user != null) {
        %>

        <div class="alert alert-warning " align="center" style="margin-bottom:10px"><h2><%=current_user%>, добро
            пожаловать в ваш личный дневник питания!</h2>
        </div>

        <% } else { %>

        <div class="alert alert-warning " align="center" style="margin-bottom:10px"><h2>Чтобы воспользоваться личным
            дневником питания, пройдите регистрацию/залогиньтесь</h2>
        </div>


        <% } %>


    </section>

</div>


</body>


</html>
