<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 12/10/15
  Time: 22:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Продукты</title>

</head>
<body onload="f();">
<%@ include file="navbar.jsp" %>


<div id="wrapper">
    <section>
        <div align="center"><h1>Продукты</h1></div>
        <hr>
        <div class="row">
            <div style="margin-left: 33.5%">
                <div class="col-sm-6" align="center" style="float: left;">
                    <input type="text" class="form-control" id="s" oninput="f()"
                           placeholder="Введите название продукта">
                </div>

            </div>


            <br><br>

            <div align="center">
                Приоритет:
                <input type="radio" name="food_type" onclick="setFoodType('protein'); f()" value="protein"> Белки
                <input type="radio" name="food_type" onclick="setFoodType('fat'); f()" value="fat"> Жиры
                <input type="radio" name="food_type" onclick="setFoodType('carbohydrates'); f()" value="carbohydrates">
                Углеводы

            </div>

            <br>

            <input type="hidden" id="food_type">

            <div align="center">
                <input type="radio" name="food_type" onclick="setFoodType(''); f()" value=""> Без приоритета
                <input type="checkbox" id="kcal" onclick="setKcal(); f()" name="kcal" value="false"> Калорийные

            </div>


        </div>
        <hr>
        <div class="well" style="margin-top: 20px;">
            <h3 style="margin-bottom: 5px;" id="message" align="center"></h3>

            <table class="table table-bordered" style="background-color: ghostwhite">
                <thead id="head">
                </thead>
                <tbody id="res">

                <script type="application/javascript">
                    setFoodType = function (type) {
                        $("#food_type").val(type);
                    };

                    setKcal = function () {
                        var kcal = $("#kcal");
                        if (kcal.prop("checked")) {
                            kcal.val("true");
                        } else {
                            kcal.val("false");
                        }
                    };


                    f = function (request, response) {
                        $.ajax({
                                    url: "/food_search",
                                    data: {
                                        "q": $("#s").val(),
                                        "food_type": $("#food_type").val(),
                                        "kcal": $("#kcal").val()
                                    },
                                    dataType: "json",
                                    async: false,

                                    success: function (response_data) {
                                        var string;
                                        if (response_data.results.length > 0) {

                                            for (var i = 0; i < response_data.results.length / 5; i++) {
                                                string = string + "<tr>";
                                                for (var j = i * 5; j < i * 5 + 5; j++) {
                                                    string = string + "<td><p style='text-align: justify'>" + response_data.results[j] + "</p></td>";
                                                }
                                                string = string + "</tr>";
                                            }
                                            $("#head").html("<tr><th><p align='center'>ПРОДУКТ (100 гр)</p></th>" +
                                                    "<th><p align='center'>БЕЛКИ</p></th>" +
                                                    "<th><p align='center'>ЖИРЫ</p></th>" +
                                                    "<th><p align='center'>УГЛЕВОДЫ</p></th>" +
                                                    "<th><p align='center'>ККАЛ</p></th></tr>");
                                            $("#res").html(string);
                                        } else {
                                            $("#res").html("");
                                        }
                                    }
                                }
                        );
                    }


                </script>
                </tbody>
            </table>
        </div>
        <hr>
    </section>
</div>
</body>
</html>
