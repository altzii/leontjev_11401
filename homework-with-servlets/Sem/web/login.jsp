<%@ page import="ru.kpfu.itis.alexander_leontjev.repository.UsersRepository" %>
<%@ page import="java.io.PrintWriter" %>
<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 19/10/15
  Time: 16:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Вход</title>
</head>
<body onload="autoinput()">
<%@ include file="navbar.jsp" %>


<% if (request.getMethod().equals("GET")) {


    response.setCharacterEncoding("UTF-8");
    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html");

    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                httpSession.setAttribute("current_user", cookie.getValue());
            }
        }
    }
    if (user != null) {
        response.sendRedirect("/recipes");
    } else {
%>



<div id="wrapper">
    <section>
        <div class="jumbotron" style="margin-top: 100px;">
            <div class="container">

                <form action="/login" method="POST" class="form-horizontal" style="margin:auto; width:450px ">
                    <% if (request.getParameter("status") != null) {
                        if (request.getParameter("status").equals("from_signup")) {%>
                    <h2 style="width: 600px; align: center; justify-content: center;">Вы успешно
                        зарегистрировались.</h2>
                    <%
                            }
                        }
                    %>

                    <h3 style="text-align:center;"><i>Пожалуйста залогинтесь!</i></h3>


                    <% String error_msg = "";
                        if (request.getParameter("error_msg") != null) {
                            if (request.getParameter("error_msg").equals("1")) {
                                error_msg = "Неправильный пароль.";
                            }
                            if (request.getParameter("error_msg").equals("2")) {
                                error_msg = "Такой пользователь не сущесвует.";

                            }
                        }
                    %>

                    <h5 style="text-align: center"><%=error_msg%>
                    </h5>

                    <script type="application/javascript">
                        autoinput = function () {
                            <%if (request.getParameter("login") != null) {  %>
                            $("#login").val("<%=request.getParameter("login")%>");
                            <% } %>
                        }
                    </script>

                    <div class="form-group " style="margin:5px 0 5px 0">
                        <div class="col-sm-9" style="width:100%">
                            <input type="text" id="login" class="form-control" name="login" placeholder="Введите логин" required
                                   value="">
                        </div>
                    </div>
                    <div class="form-group" style="margin:5px 0 5px 0">
                        <div class="col-sm-9" style="width:100%">
                            <input type="password" class="form-control" name="password" placeholder="Введите пароль"
                                   required>
                        </div>
                    </div>
                    <p style="width:370px; padding-left:15px; margin-bottom:0px; font-size:15px">
                        <input type="checkbox" name="remember" value="1">&nbsp;Запомнить меня</p>

                    <p><input type="submit" class="btn btn-success btn-lg btn-block"
                              style="margin:15px 0 0 15px; width: 92.5%" value="Войти"/></p>
                </form>
            </div>
        </div>
    </section>
</div>

<% }
} else {
    if (user != null) {
        response.sendRedirect("/profile");
    } else {
        if (UsersRepository.authorize(request.getParameter("login"), request.getParameter("password")) == -1) {
            response.sendRedirect("/login?error_msg=2&login=" + request.getParameter("login"));
        } else if (UsersRepository.authorize(request.getParameter("login"), request.getParameter("password")) == 0) {
            response.sendRedirect("/login?error_msg=1&login=" + request.getParameter("login"));
        } else {
            httpSession.setAttribute("current_user", request.getParameter("login"));
            if (request.getParameter("remember") != null) {
                httpSession.setAttribute("current_user", request.getParameter("login"));
                Cookie cookie = new Cookie("login", request.getParameter("login"));
                cookie.setMaxAge(24 * 60 * 60);
                response.addCookie(cookie);
            }
            response.sendRedirect("/recipes");
        }
    }
}

%>

</body>
</html>
