<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 12/10/15
  Time: 21:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="application/javascript" src="js/jquery-2.1.4.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/npm.js"></script>
    <link href="css/bootstrap-theme.css" rel="stylesheet"/>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<nav>
    <ul class="top-menu">
        <li>
            <div class="btn-group-lg">
                <a href="/">
                    <button type="button" class="btn btn-default btn-lg">Домой</button>
                </a>
                <a href="/foods">
                    <button type="button" class="btn btn-default btn-lg">Продукты</button>
                </a>
                <a href="/nutritional_supplements">
                    <button type="button" class="btn btn-default btn-lg">Пищевые добавки</button>
                </a>
                <a href="/recipes">
                    <button type="button" class="btn btn-default btn-lg">Рецепты</button>
                </a>
                <a href="/food_diary">
                    <button type="button" class="btn btn-default btn-lg">Дневник</button>
                </a>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <%
                    HttpSession httpSession = request.getSession();
                    String user = (String) httpSession.getAttribute("current_user");

                    if (user == null) {
                %>
                <a href="/login">
                    <button type="button" class="btn btn-success btn-lg">Вход</button>
                </a>
                <a href="/signup">
                    <button type="button" class="btn btn-primary btn-lg">Регистрация</button>
                </a>

                <% } else {%>
                <a href="/logout">
                    <button type="button" class="btn btn-danger btn-lg">Выйти</button>
                </a>
                <%}%>

            </div>
        </li>
    </ul>
</nav>

</body>
</html>
