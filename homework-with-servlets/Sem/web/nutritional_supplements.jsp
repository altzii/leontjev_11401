<%@ page import="ru.kpfu.itis.alexander_leontjev.models.NutritionalSupplement" %>
<%@ page import="ru.kpfu.itis.alexander_leontjev.repository.NutritionalSupplementsRepository" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 12/10/15
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Пищевые добавки</title>
</head>
<body>
<%@ include file="navbar.jsp" %>

<div id="wrapper">
    <section>
        <div align="center"><h1>Пищевые добавки</h1></div>
        <hr>
        <div class="row">
            <div style="float: left; margin-left: 34.5%">
                <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseExample2"
                        aria-expanded="false" aria-controls="collapseExample">
                    Посмотреть все пищевые добавки
                </button>
            </div>
            <div style="float: left; margin-left: 25px;">
                <button class="btn btn-danger" type="button" data-toggle="collapse" data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample">
                    Поиск
                </button>
            </div>
        </div>
        <div align="center" class="collapse" id="collapseExample">
            <div style="margin-top: 20px;">
                <div align="center"><input type="text" style="width: 300px;" id="s" oninput="f()" class="form-control"
                                           placeholder="Введите номер пищевой добавки">
                </div>
            </div>

            <div class="well" style="margin-top: 20px;">
                <h3 style="margin-bottom: 5px;" id="message" align="center"></h3>

                <table class="table table-bordered" style="background-color: ghostwhite">
                    <thead id="head">
                    </thead>
                    <tbody id="res">

                    <script type="application/javascript">
                        f = function (request, response) {
                            var input = $("#s").val();
                            var regexp = new RegExp('^([EЕeе][- ]?)?([\\d]{1,4}[a-zA-Z]?)$');
                            var match = regexp.exec(input);
                            if (match != null) {
                                q = match[2];
                            } else {
                                q = null;
                            }
                            $.ajax({
                                        url: "/nssearch",
                                        data: {"q": q},
                                        dataType: "json",

                                        success: function (response_data) {
                                            var string;
                                            var checked = regexp.test(input);

                                            if (input.length > 0) {
                                                if (checked) {
                                                    if (response_data.results.length > 0) {
                                                        for (var i = 0; i < response_data.results.length / 5; i++) {
                                                            string = string + "<tr>";
                                                            for (var j = i * 5; j < i * 5 + 5; j++) {
                                                                string = string + "<td><p style='text-align: justify'>" + response_data.results[j] + "</p></td>";
                                                            }
                                                            string = string + "</tr>";
                                                        }
                                                        $("#head").html("<tr><th><p>Номер</p></th>" +
                                                                "<th><p align='center'>Английское название</p></th>" +
                                                                "<th><p align='center'>Русское название</p></th>" +
                                                                "<th><p align='center'>INFO Миндзрав</p></th>" +
                                                                "<th><p align='center'>Примечание</p></th></tr>");
                                                        $("#res").html(string);
                                                        $("#message").html("Результаты поиска для " + input);


                                                    } else {
                                                        $("#res").html("");
                                                        $("#message").html("Нет результатов для " + input)


                                                    }
                                                } else {
                                                    $("#res").html("");
                                                    $("#message").html("Некорретный ввод, пищевая добавка не может быть задана в таком формате");

                                                }
                                            } else {
                                                $("#res").html("");
                                            }
                                        }
                                    }
                            );
                        }
                    </script>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="collapse" id="collapseExample2">
            <div class="well" style="margin-top: 20px;">
                <table class="table table-bordered" style="background-color: ghostwhite">
                    <thead>
                    <tr>
                        <th><p align='center'>Номер</p></th>
                        <th><p align='center'>Английское название</p></th>
                        <th><p align='center'>Русское название</p></th>
                        <th><p align='center'>INFO Миндзрав</p></th>
                        <th><p align='center'>Примечание</p></th>
                    </tr>
                    </thead>
                    <tbody>

                    <% ArrayList<NutritionalSupplement> nutritionalSupplements = NutritionalSupplementsRepository.getNutritionalSupplements();

                        for (NutritionalSupplement nutritionalSupplement : nutritionalSupplements) { %>
                    <tr>
                        <td>E-<%=nutritionalSupplement.getNumber()%>
                        </td>
                        <td><%=nutritionalSupplement.getEngName()%>
                        </td>
                        <td><%=nutritionalSupplement.getRusName()%>
                        </td>
                        <td><%=nutritionalSupplement.getStatus()%>
                        </td>
                        <td><%=nutritionalSupplement.getNotation()%>
                        </td>
                    </tr>
                    <%}%>


                    </tbody>
                </table>
            </div>
        </div>

        <hr>


    </section>


</div>

</body>
</html>
