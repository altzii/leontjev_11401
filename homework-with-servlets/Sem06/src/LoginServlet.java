import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.CookieHandler;
import java.sql.*;
import java.util.HashMap;

/**
 * Created by Alexander on 28/09/15.
 */

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    public void init() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        String user = (String) httpSession.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {
            if (UsersRepository.authorize(request.getParameter("login"), request.getParameter("password")) == -1) {
                response.sendRedirect("/login?error_msg=2&login=" + request.getParameter("login"));
            } else if (UsersRepository.authorize(request.getParameter("login"), request.getParameter("password")) == 0) {
                response.sendRedirect("/login?error_msg=1&login=" + request.getParameter("login"));
            } else {
                httpSession.setAttribute("current_user", request.getParameter("login"));
                if (request.getParameter("remember") != null) {
                    httpSession.setAttribute("current_user", request.getParameter("login"));
                    Cookie cookie = new Cookie("login", request.getParameter("login"));
                    cookie.setMaxAge(24 * 60 * 60);
                    response.addCookie(cookie);
                }
                response.sendRedirect("/profile");
            }
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    httpSession.setAttribute("current_user", cookie.getValue());
                }
            }
        }

        String user = (String) httpSession.getAttribute("current_user");
        if (user != null) {
            response.sendRedirect("/profile");
        } else {
            if (request.getParameter("error_msg") != null) {
                if (request.getParameter("error_msg").equals("1")) {
                    writer.println("Password isn't correct");
                }
                if (request.getParameter("error_msg").equals("2")) {
                    writer.println("This user doesn't exist");
                }
            }
            writer.print("<form action= \"/login\" method=\"POST\"><input type=\"text\" placeholder=\"login\" name = " +
                    "\"login\"");

            if (request.getParameter("login") != null) {
                writer.print("value=\"" + request.getParameter("login") + "\"");
            }

            writer.println("/><br><input type=\"text\" name=\"password\" placeholder=\"password\"/>");
            writer.println("<br><input type=\"checkbox\" name=\"remember\" value=\"1\">remember me<br>");
            writer.println("<input type=\"submit\" value=\"Login\"/>");
        }
    }
}
