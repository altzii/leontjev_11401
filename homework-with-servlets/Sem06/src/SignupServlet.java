import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Alexander on 18/10/15.
 */
@WebServlet(name = "SignupServlet")
public class SignupServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsersRepository.signup(request.getParameter("login"), request.getParameter("password"));







    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();

        writer.print("<form action= \"/signup\" method=\"POST\">" +
                "<input type=\"text\" placeholder=\"login\" name = " +
                "\"login\"/>");
        writer.println("<br><input type=\"text\" name=\"name\" placeholder=\"name\"/>");
        writer.println("<br><input type=\"text\" name=\"email\" placeholder=\"email\"/>");
        writer.println("<br><input type=\"text\" name=\"password\" placeholder=\"password\"/>");
        writer.println("<br><input type=\"submit\" value=\"Signup\"/>");
    }
}
