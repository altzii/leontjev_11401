import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Alexander on 18/10/15.
 */
public class UsersRepository {
    protected static int authorize(String login, String password) {
        int status = -1;
        try {
            Connection connection = MyConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE login=?");
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                status = 0;
                if (resultSet.getString("password").equals(password)) {
                    status = 1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return status;
    }

    protected static void signup(String login, String password) {
        try {
            Connection connection = MyConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into users values(5, ?, ?)");
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
