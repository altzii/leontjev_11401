create table "users" (
        user_id integer not null  PRIMARY KEY,
        name varchar(16)
        login varchar(16)
        email VARCHAR(80) NOT NULL,
        password varchar(50) not null
);

create table "nutritional_supplements" (
        number integer not null primary key,
        eng_name varchar(250) not null,
        rus_name varchar(250) not null,
        status varchar(250),
        notation varchar(250)
);


create table "foods" (
        id integer not null primary key,
        name varchar(100) not null,
        protein float,
        fat float,
        carbohydrates float,
        kcal float
);

create table "recipes" {
         id integer not null primary key,
         author integer,
         name varchar(100)  not null,
         image varchar(100),
         description varchar,
         date datetime
};

create table "days" {
        day date,            /* день */
        value integer,       /* сколько съел */
        user_id integer      /* кто съел */
};
