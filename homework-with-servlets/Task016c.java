import java.sql.*;

/**
 * Created by Alexander on 17/10/15.
 */
public class Task016c {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/virtual-school", "alt", "itiskfu");

        Statement stmt = connection.createStatement();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT DISTINCT students.name AS student_name, classes.name as class_name FROM " +
                "attendance, students, classes WHERE year<=? AND year>=? AND " +
                "attendance.student_id = students.id AND attendance.class_id = classes.id");
        preparedStatement.setInt(1, 1996);
        preparedStatement.setInt(2, 1992);

        ResultSet resultSet = preparedStatement.executeQuery();



        while (resultSet.next()) {
            System.out.println(resultSet.getString("student_name") + " - " + resultSet.getString("class_name"));
        }

        PreparedStatement preparedStatement2 = connection.prepareStatement("SELECT DISTINCT students.name AS student_name, attendance.year AS year FROM" +
                " attendance, students, teachers WHERE attendance.student_id = students.id AND" +
                " attendance.teacher_id = teachers.id AND teachers.name =?");
        preparedStatement2.setString(1, "Horace Slughorn");

        resultSet = preparedStatement2.executeQuery();


        while (resultSet.next()) {
            System.out.println(resultSet.getString("student_name") + " - " + resultSet.getString("year"));
        }

        stmt.close();


    }
}
