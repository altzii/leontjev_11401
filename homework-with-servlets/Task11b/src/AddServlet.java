import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 01/10/2015.
 */
@WebServlet(name = "AddServlet")
public class AddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");

        Pattern pattern = Pattern.compile("/add/(\\d+)/(\\d+)/?");
        Matcher matcher = pattern.matcher(request.getRequestURI());

        if (matcher.matches()) {
            response.getWriter().print(Integer.parseInt(matcher.group(1)) + Integer.parseInt(matcher.group(2)));

        } else {
            response.sendRedirect("/error404");
        }



    }
}
