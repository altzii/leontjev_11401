import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 01/10/2015.
 */

@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("/baidu", "wd");
        hashMap.put("/bing", "q");
        hashMap.put("/yahoo", "p");
        hashMap.put("/aol", "q");

        Pattern pattern = Pattern.compile("/search/(baidu|aol|yahoo|bing)/?");
        Matcher matcher = pattern.matcher(request.getRequestURI());


        if (matcher.matches()) {
            if (request.getPathInfo().equals("/baidu")) {
                response.getWriter().print("<form action=\"http://www.baidu.com/s?ie=utf-8&wd=\" method=\"GET\">");
            }

            if (request.getPathInfo().equals("/bing")) {
                response.getWriter().print("<form action=\"http://www.bing.com/search?q=f\" method=\"GET\">");
            }

            if (request.getPathInfo().equals("/yahoo")) {
                response.getWriter().print("<form action=\"https://search.yahoo.com/search?p=\" method=\"GET\">");
            }

            if (request.getPathInfo().equals("/aol")) {
                response.getWriter().print("<form action=\"http://search.aol.com/search?q=\" method=\"GET\">");
            }
            response.getWriter().print("\n<input type=\"text\" name=\"" + hashMap.get(request.getPathInfo()) + "\"/><br>");
            response.getWriter().print("<input type=\"submit\" value=\"Поиск на " + request.getPathInfo().substring(1, request.getPathInfo().length()) + "\"/>");

        } else {
            response.sendRedirect("/error404");
        }
    }
}
