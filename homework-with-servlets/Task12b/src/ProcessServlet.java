import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 01/10/2015.
 */

@WebServlet(name = "ProcessServlet")
public class ProcessServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        request.setCharacterEncoding("UTF-8");

        int result = 0;
        String text = request.getParameter("text");

        if (request.getParameter("oper").equals("symbols")) {
            result = text.length();
        }
        if (request.getParameter("oper").equals("words")) {
            result = (text.split(" ")).length;
        }
        if (request.getParameter("oper").equals("paragraphs")) {
            result = (text.split("\\n")).length;
        }
        if (request.getParameter("oper").equals("sentences")) {
            Pattern pattern = Pattern.compile("[\\.!?]+");
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                result++;
            }

        }

        httpSession.setAttribute("result", result);
        response.sendRedirect("/result");



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");

        response.getWriter().println("<form action=\"/process\" method=\"POST\">");
        response.getWriter().print("<textarea name=\"text\">Input text</textarea><br><br>");
        response.getWriter().print("<select name=\"oper\">\n" +
                "  <option oper=\"symbols\">symbols</option>\n" +
                "  <option oper=\"words\">words</option>\n" +
                "  <option oper=\"sentences\">sentences</option>\n" +
                "  <option oper=\"paragraphs\">paragraphs</option>\n" +
                "</select><br><br>");
        response.getWriter().println("<input type=\"submit\" value=\"process\"/>" + "</form>");
    }
}
