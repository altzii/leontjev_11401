<%@ page import="java.util.HashMap" %>
<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 10/10/15
  Time: 03:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<% HashMap<String, String> hashMap = new HashMap<String, String>();
    hashMap.put("sasha", "123");
    if (request.getMethod().equals("GET")) {

        HttpSession httpSession = request.getSession();
        Cookie[] cookies = request.getCookies();

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                httpSession.setAttribute("current_user", cookie.getValue());
            }
        }

        String user = (String) httpSession.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {
            if (request.getParameter("error_msg") != null) {
                if (request.getParameter("error_msg").equals("1")) {
%>

Password isn't correct

<% }
    if (request.getParameter("error_msg").equals("2")) {
%>
This user doesn't exist

<% }
}
%>

<form action="/login" method="POST">
    <input type="text" placeholder="login" name="login"

            <% if (request.getParameter("login") != null) { %>
           value="<%=request.getParameter("login")%>"
            <%} %>
            />
    <br>
    <input type="text" name="password" placeholder="password"/><br>
    <input type="checkbox" name="remember" value="1">remember me<br>
    <input type="submit" value="Login"/>

        <%}
        } else {

         HttpSession httpSession = request.getSession();
        String user = (String) httpSession.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else if (request.getParameter("remember") != null) {
            Cookie cookie = new Cookie(request.getParameter("login"), request.getParameter("password"));
            cookie.setMaxAge(24 * 60 * 60);
            response.addCookie(cookie);
        }

        if (hashMap.containsKey(request.getParameter("login"))) {
            if (hashMap.get(request.getParameter("login")).equals(request.getParameter("password"))) {
                httpSession.setAttribute("current_user", request.getParameter("login"));
                response.sendRedirect("/profile");
            } else {
                response.sendRedirect("/login?error_msg=1&login=" + request.getParameter("login"));
            }
        } else {
            response.sendRedirect("/login?error_msg=2&login=" + request.getParameter("login"));
        }
        }
        %>


</body>
</html>
