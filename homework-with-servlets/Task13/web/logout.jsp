<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 10/10/15
  Time: 03:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>logout</title>
</head>
<body>

<%
    HttpSession hs = request.getSession();
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
        cookie.setMaxAge(0);
        cookie.setPath("/");
    }
    hs.setAttribute("current_user", null);
    response.sendRedirect("/login");
%>

</body>
</html>
