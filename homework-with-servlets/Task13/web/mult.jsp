<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>
<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 10/10/15
  Time: 03:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>mult</title>
</head>
<body>

<% Pattern pattern = Pattern.compile("/mult/(\\d+)/(\\d+)/?");
  Matcher matcher = pattern.matcher(request.getRequestURI());
%>
<% if (matcher.matches()) { %>
<%=Integer.parseInt(matcher.group(1)) * Integer.parseInt(matcher.group(2))%>

<%
  } else {
    response.sendRedirect("/error404");
  }
%>
</body>
</html>
