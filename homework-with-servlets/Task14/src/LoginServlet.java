import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by Alexander on 10/10/15.
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    private HashMap<String, String> hashMap = new HashMap<String, String>();

    public void init() {
        hashMap.put("sasha", "123");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        String user = (String) httpSession.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {
            if (hashMap.containsKey(request.getParameter("login"))) {
                if (hashMap.get(request.getParameter("login")).equals(request.getParameter("password"))) {
                    if (request.getParameter("remember") != null) {
                        httpSession.setAttribute("current_user", request.getParameter("login"));
                        Cookie cookie = new Cookie("login", request.getParameter("login"));
                        cookie.setMaxAge(24 * 60 * 60);
                        response.addCookie(cookie);
                    }
                    response.sendRedirect("/profile");
                } else {
                    response.sendRedirect("/login?error_msg=1&login=" + request.getParameter("login"));
                }
            } else {
                response.sendRedirect("/login?error_msg=2&login=" + request.getParameter("login"));
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Configuration configuration = ConfigSingleton.getConfiguration(getServletContext());
        Template template = configuration.getTemplate("login.ftl");
        HashMap<String, Object> root = new HashMap<>();

        HttpSession httpSession = request.getSession();

        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                httpSession.setAttribute("current_user", cookie.getValue());
            }
        }

        String user = (String) httpSession.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {
            if (request.getParameter("error_msg") != null) {
                if (request.getParameter("error_msg").equals("1")) {
                    root.put("error_msg", "Password isn't correct");
                }
                if (request.getParameter("error_msg").equals("2")) {
                    root.put("error_msg", "This user doesn't exist");
                }
            }

            if (request.getParameter("login") != null) {
                root.put("login", request.getParameter("login"));

            }
        }

        try {
            template.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
