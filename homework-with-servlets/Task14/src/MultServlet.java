import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 10/10/15.
 */
@WebServlet(name = "MultServlet")
public class MultServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Configuration configuration = ConfigSingleton.getConfiguration(getServletContext());
        Template template = configuration.getTemplate("result.ftl");
        HashMap<String, Object> root = new HashMap<>();

        Pattern pattern = Pattern.compile("/mult/(\\d+)/(\\d+)/?");
        Matcher matcher = pattern.matcher(request.getRequestURI());


        if (matcher.matches()) {
            root.put("result", Integer.parseInt(matcher.group(1)) * Integer.parseInt(matcher.group(2)));
            try {
                template.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        } else {
            response.sendRedirect("/error404");
        }


    }
}
