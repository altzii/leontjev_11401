import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 10/10/15.
 */
@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Configuration configuration = ConfigSingleton.getConfiguration(getServletContext());
        Template template = configuration.getTemplate("search.ftl");
        HashMap<String, Object> root = new HashMap<>();

        root.put("name", request.getPathInfo().substring(1, request.getPathInfo().length()));
        if (request.getPathInfo().equals("/baidu")) {
            root.put("action", "http://www.baidu.com/s?ie=utf-8&wd=");
            root.put("param", "wd");
        }

        if (request.getPathInfo().equals("/bing")) {
            root.put("action", "http://www.bing.com/search?q=f");
            root.put("param", "q");
        }

        if (request.getPathInfo().equals("/aol")) {
            root.put("action", "http://search.aol.com/search?q=");
            root.put("param", "q");
        }

        if (request.getPathInfo().equals("/yahoo")) {
            root.put("action", "https://search.yahoo.com/search?p=");
            root.put("param", "p");
        }


        Pattern pattern = Pattern.compile("/search/(baidu|aol|yahoo|bing)/?");
        Matcher matcher = pattern.matcher(request.getRequestURI());


        if (matcher.matches()) {
            try {
                template.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        } else {
            response.sendRedirect("/error404");
        }
    }
}
