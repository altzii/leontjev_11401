import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 10/10/15.
 */
@WebServlet(name = "AddServlet")
public class AddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, "/Users/Alexander/HomeworkDirectory/homework with servlets/Task15/web/WEB-INF/templates");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        VelocityContext context = new VelocityContext();

        Pattern pattern = Pattern.compile("/add/(\\d+)/(\\d+)/?");
        Matcher matcher = pattern.matcher(request.getRequestURI());


        if (matcher.matches()) {
            Template t = velocityEngine.getTemplate("result.vm");
            context.put("result", Integer.parseInt(matcher.group(1)) + Integer.parseInt(matcher.group(2)));
            t.merge(context, response.getWriter());

        } else {
            response.sendRedirect("/error404");
        }

    }
}
