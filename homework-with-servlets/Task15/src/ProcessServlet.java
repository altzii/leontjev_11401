import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 11/10/15.
 */
@WebServlet(name = "ProcessServlet")
public class ProcessServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        request.setCharacterEncoding("UTF-8");

        int result = 0;
        String text = request.getParameter("text");

        if (request.getParameter("oper").equals("symbols")) {
            result = text.length();
        }
        if (request.getParameter("oper").equals("words")) {
            result = (text.split(" ")).length;
        }
        if (request.getParameter("oper").equals("paragraphs")) {
            result = (text.split("\\n")).length;
        }
        if (request.getParameter("oper").equals("sentences")) {
            Pattern pattern = Pattern.compile("[\\.!?]+");
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                result++;
            }

        }

        httpSession.setAttribute("result", result);
        response.sendRedirect("/result");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, "/Users/Alexander/HomeworkDirectory/homework with servlets/Task15/web/WEB-INF/templates");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        VelocityContext context = new VelocityContext();
        Template t = velocityEngine.getTemplate("process.vm");
        t.merge(context, response.getWriter());


    }
}
