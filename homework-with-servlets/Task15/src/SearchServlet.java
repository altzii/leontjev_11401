import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 10/10/15.
 */
@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, "/Users/Alexander/HomeworkDirectory/homework with servlets/Task15/web/WEB-INF/templates");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        VelocityContext context = new VelocityContext();

        context.put("name", request.getPathInfo().substring(1, request.getPathInfo().length()));
        if (request.getPathInfo().equals("/baidu")) {
            context.put("action", "http://www.baidu.com/s?ie=utf-8&wd=");
            context.put("param", "wd");
        }

        if (request.getPathInfo().equals("/bing")) {
            context.put("action", "http://www.bing.com/search?q=f");
            context.put("param", "q");
        }

        if (request.getPathInfo().equals("/aol")) {
            context.put("action", "http://search.aol.com/search?q=");
            context.put("param", "q");
        }

        if (request.getPathInfo().equals("/yahoo")) {
            context.put("action", "https://search.yahoo.com/search?p=");
            context.put("param", "p");
        }


        Pattern pattern = Pattern.compile("/search/(baidu|aol|yahoo|bing)/?");
        Matcher matcher = pattern.matcher(request.getRequestURI());




        if (matcher.matches()) {
            Template t = velocityEngine.getTemplate("search.vm");
            t.merge(context, response.getWriter());
        } else {
            response.sendRedirect("/error404");
        }

    }
}
