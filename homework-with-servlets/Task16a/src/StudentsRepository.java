import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Alexander on 19/10/15.
 */
public class StudentsRepository {
    static public ArrayList<String[]> getStudentsAndYearsByTeacher(String teacher) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual-school", "alt", "itiskfu");

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT DISTINCT students.name AS student_name, attendance.year AS year FROM" +
                " attendance, students, teachers WHERE attendance.student_id = students.id AND" +
                " attendance.teacher_id = teachers.id AND teachers.name =?");
        preparedStatement.setString(1, teacher);

        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<String[]> arrayList = new ArrayList();

        while (resultSet.next()) {
            String[] stringArray = new String[2];
            stringArray[0] = resultSet.getString("student_name");
            stringArray[1] = resultSet.getString("year");
            arrayList.add(stringArray);
        }
        return arrayList;
    }

    static public ArrayList<String[]> getStudentsAndClassesByYears(int year1, int year2) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual-school", "alt", "itiskfu");

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT DISTINCT students.name AS student_name, classes.name as class_name FROM attendance, students, classes WHERE year<=? " +
                "AND year>=? AND " +
                "attendance.student_id = students.id AND attendance.class_id = classes.id");
        preparedStatement.setInt(1, year2);
        preparedStatement.setInt(2, year1);

        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<String[]> arrayList = new ArrayList();

        while (resultSet.next()) {
            String[] stringArray = new String[2];
            stringArray[0] = resultSet.getString("student_name");
            stringArray[1] = resultSet.getString("class_name");
            arrayList.add(stringArray);
        }

        return arrayList;
    }


}
