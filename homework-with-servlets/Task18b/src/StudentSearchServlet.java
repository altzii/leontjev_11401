import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by Alexander on 21/10/15.
 */
@WebServlet(name = "StudentSearchServlet")
public class StudentSearchServlet extends HttpServlet {
    Connection connection;

    public void init() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/virtual-school", "alt", "itiskfu");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String q = request.getParameter("q");


        try {

            PreparedStatement ps = connection.prepareStatement("select name from "+request.getParameter("select")+" where name like ?");
            ps.setString(1, "%" + q + "%");
            ResultSet rs = ps.executeQuery();
            JSONArray  ja = new JSONArray();

            while (rs.next()) {
                ja.put(rs.getString("name"));
            }

            JSONObject jo = new JSONObject();
            jo.put("results", ja);
            response.setContentType("text/json");
            response.getWriter().print(jo.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
