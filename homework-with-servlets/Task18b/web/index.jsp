<%--
  Created by IntelliJ IDEA.
  User: Alexander
  Date: 21/10/15
  Time: 13:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <script type="application/javascript" src="js/jquery-2.1.4.js"></script>
    <link href="css/bootstrap-theme.css" rel="stylesheet"/>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="js/bootstrap.js"></script>
    <script src="js/npm.js"></script>
</head>
<body>

<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
        Make your choice
        <span class="caret"></span>
    </button>

    <input id="select" type="hidden" value=""/>
    <ul class="dropdown-menu">
        <li><a onclick="change_select('students')">Students</a></li>
        <li><a onclick="change_select('teachers')">Teachers</a></li>
        <li><a onclick="change_select('classes')">Classes</a></li>
    </ul>
</div>
<br><br>

<input type="text" id="s" oninput="f()"/>

<div id="res"></div>

<script type="application/javascript">
    f = function (request, response) {
        $.ajax({
                    url: "/search",
                    data: {"q": $("#s").val(), "select": $("#select").val()},
                    dataType: "json",
                    success: function (response_data) {
                        if (response_data.results.length > 0) {
                            $("#res").html("Search results:");
                            for (var i = 0; i < response_data.results.length; i++) {
                                $("#res").append("<li>" + response_data.results[i] + "</li>")
                            }
                        } else {
                            $("#res").html("No results");
                        }
                    }
                }
        );

    };

    change_select = function (param) {
        $("#select").val(param);
    }
</script>

</body>
</html>
