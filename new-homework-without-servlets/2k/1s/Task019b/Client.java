import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by Alexander on 31/10/15.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        PrintWriter os = new PrintWriter(s.getOutputStream(), true);
        BufferedReader is = new BufferedReader(new InputStreamReader(s.getInputStream()));
        Scanner scanner = new Scanner(System.in);

        HashSet<String> data = new HashSet<String>();
        boolean added = false;


        while (true) {
            String x = is.readLine();
            data.add(x);
            System.out.println("SERVER: " + x);
            while (!added) {
                String b = scanner.nextLine();
                if ((x.charAt(x.length() - 1)) == b.charAt(0) && data.add(b)) {
                    os.println(b);
                    added = true;
                } else {
                    System.out.println("Повторите ввод");
                }
            }
            added = false;

        }
    }
}
