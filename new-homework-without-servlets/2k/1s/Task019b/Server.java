import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by Alexander on 31/10/15.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        final int PORT = 3456;

        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Starting listening on port: " + PORT);
        Socket client = s.accept();
        System.out.println("Client connected");

        PrintWriter os = new PrintWriter(client.getOutputStream(), true);
        BufferedReader is = new BufferedReader(new InputStreamReader(client.getInputStream()));
        Scanner scanner = new Scanner(System.in);

        HashSet<String> data = new HashSet<String>();
        boolean added = false;

        String b = scanner.nextLine();
        os.println(b);
        data.add(b);
        String x = is.readLine();
        data.add(x);
        System.out.println("CLIENT: " + x);


        while (true) {
            while (!added) {
                b = scanner.nextLine();
                if ((x.charAt(x.length() - 1)) == b.charAt(0) && data.add(b)) {
                    os.println(b);
                    added = true;
                } else {
                    System.out.println("Повторите ввод");
                }
            }
            added = false;
            x = is.readLine();
            data.add(x);
            System.out.println("CLIENT: " + x);
        }
    }
}
