import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Alexander on 28/10/15.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        PrintWriter os = new PrintWriter(s.getOutputStream(), true);
        BufferedReader is = new BufferedReader(new InputStreamReader(s.getInputStream()));
        Scanner scanner = new Scanner(System.in);

        String serverName = is.readLine();

        System.out.println("Please type your name:");
        String name = scanner.nextLine();
        os.println(name);

        while (true) {
            String x = is.readLine();
            System.out.println(serverName + ": " + x);
            String b = scanner.nextLine();
            os.println(b);

        }
    }
}
