import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Alexander on 28/10/15.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        final int PORT = 3456;

        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Starting listening on port: " + PORT);
        Socket client = s.accept();
        System.out.println("Client connected");

        PrintWriter os = new PrintWriter(client.getOutputStream(), true);
        BufferedReader is = new BufferedReader(new InputStreamReader(client.getInputStream()));
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please type your name");
        String name = scanner.nextLine();
        os.println(name);

        String clientName = is.readLine();


        while (true) {
            String b = scanner.nextLine();
            os.println(b);
            String x = is.readLine();
            System.out.println(clientName + ": " + x);
        }
    }
}
