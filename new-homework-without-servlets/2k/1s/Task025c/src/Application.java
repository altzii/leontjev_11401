import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Alexander on 12/12/2015.
 */
public class Application {
    public static void main(String[] args) throws ClassNotFoundException {
        Class[] classes = new Class[3];
        classes[0] = ComplexNumber.class;
        classes[1] = RationalFraction.class;
        classes[2] = Vector2D.class;

        HashMap<String, Info> authors = new HashMap<String, Info>();

        for (Class $class : classes) {
            Method[] methods = $class.getDeclaredMethods();
            for (Method method : methods) {
                Annotation[] annotations = method.getAnnotations();

                for (Annotation annotation : annotations) {
                    if (annotation instanceof Author) {
                        String authorName = ((Author) annotation).name();
                        if (!authors.containsKey(authorName)) {
                            authors.put(authorName, new Info());
                        }

                        Info authorInfo = authors.get(authorName);
                        authorInfo.name = authorName;

                        authorInfo.allMethodsLength += method.getName().length();
                        authorInfo.methodsCount++;
                        for (java.lang.reflect.Parameter parameter : method.getParameters()) {
                            authorInfo.allParamsLength += parameter.getName().length();
                        }
                        authorInfo.paramsCount += method.getParameterCount();

                        if (method.getReturnType().toString().equals("void")) {
                            authorInfo.voidMethodsCount++;
                        }


                        int methodParams = method.getParameterCount();
                        if (methodParams > authorInfo.maxParamsInMethod) {
                            authorInfo.setMaxParamsInMethod(methodParams);
                        }
                        if (authorInfo.methodsCount != 0 && authorInfo.paramsCount != 0) {
                            authorInfo.setAverageMethodLength((double) authorInfo.allMethodsLength / authorInfo.methodsCount);
                            authorInfo.setAverageParamLength((double) authorInfo.allParamsLength / authorInfo.paramsCount);
                        }
                    }
                }
            }
        }
        List<Info> authorList = new ArrayList<Info>(authors.values());

        System.out.println("По средней длине названия метода (по всем методам одного автора)");
        Collections.sort(authorList, new Comparator<Info>() {

            @Override
            public int compare(Info o1, Info o2) {
                return (int) Math.signum(o2.getAverageMethodLength() - o1.getAverageMethodLength());
            }
        });

        for (Info author : authorList) {
            System.out.println(author.name + " - " + author.getAverageMethodLength());
        }
        System.out.println("\n");

        System.out.println("По средней длине имени параметра в методе (по всем параметрам\n" +
                "всех методов одного автора)");
        Collections.sort(authorList, new Comparator<Info>() {

            @Override
            public int compare(Info o1, Info o2) {
                return (int) Math.signum(o2.getAverageParamLength() - o1.getAverageParamLength());
            }
        });

        for (Info author : authorList) {
            System.out.println(author.name + " - " + author.getAverageParamLength());
        }
        System.out.println("\n");

        System.out.println("По количеству void-функций");
        Collections.sort(authorList, new Comparator<Info>() {

            @Override
            public int compare(Info o1, Info o2) {
                return (int) Math.signum(o2.getVoidMethodsCount() - o1.getVoidMethodsCount());
            }
        });

        for (Info author : authorList) {
            System.out.println(author.name + " - " + author.getVoidMethodsCount());
        }
        System.out.println("\n");

        System.out.println("По общему количество параметров в написанной функции");
        Collections.sort(authorList, new Comparator<Info>() {

            @Override
            public int compare(Info o1, Info o2) {
                return (int) Math.signum(o2.getMaxParamsInMethod() - o1.getMaxParamsInMethod());
            }
        });

        for (Info author : authorList) {
            System.out.println(author.name + " - " + author.getMaxParamsInMethod());
        }
        System.out.println("\n");
    }
}
