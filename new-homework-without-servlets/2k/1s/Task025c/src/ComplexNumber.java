/**
 * Created by Alexander on 30/11/15.
 */
public class ComplexNumber {
    private double a;
    private double b;
    private int id;

    @Author(name = "Alexander")
    public double getA() {
        return this.a;
    }

    @Author(name = "Marat")
    public double getB() {
        return this.b;
    }

    @Author(name = "Rustam")
    public void setA(double a) {
        this.a = a;
    }

    @Author(name = "Yura")
    public void setB(double b) {
        this.b = b;
    }

    public ComplexNumber() {
        this(0.0, 0.0);
    }

    public ComplexNumber(Double a, Double b) {
        this.a = a;
        this.b = b;
    }

    @Author(name = "Eugene")
    public String toString() {
        if (b > 0) {
            return a + " + " + b + "i";
        } else {
            return a + " - " + Math.abs(b) + "i";
        }
    }

    @Author(name = "Alexander")
    public boolean equals(ComplexNumber cn) {
        return this.a == cn.getA() && this.b == cn.getB();

    }

    @Author(name = "Eugene")
    public void add2(ComplexNumber cn) {
        this.a += cn.a;
        this.b += cn.b;
    }

    @Author(name = "Rustam")
    public ComplexNumber add(ComplexNumber cn2) {
        ComplexNumber cn = new ComplexNumber(this.getA() + cn2.getA(), this.getB() + cn2.getB());
        return cn;
    }

    @Author(name = "Alexander")
    public void sub2(ComplexNumber cn) {
        this.a -= cn.a;
        this.b -= cn.b;
    }


    @Author(name = "Eugene")
    public ComplexNumber sub(ComplexNumber cn2) {
        ComplexNumber cn = new ComplexNumber(this.getA() - cn2.getA(), this.getB() - cn2.getB());
        return cn;
    }

    @Author(name = "Marat")
    public void mult2(ComplexNumber cn) {
        double temp = this.a * cn.getA() - this.b * cn.getB();
        this.b = this.a * cn.getB() + this.b * cn.getA();
        this.a = temp;
    }

    @Author(name = "Yura")
    public ComplexNumber mult(ComplexNumber cn2) {
        ComplexNumber cn = new ComplexNumber(this.a * cn2.getA() - this.b * cn2.getB(), this.a * cn2.getB() + this.b * cn2.getA());
        return cn;
    }

    @Author(name = "Alexander")
    public void div2(ComplexNumber cn) {
        double temp = (this.a * cn.getA() + this.b * cn.getB()) / (cn.getA() * cn.getA() + cn.getB() * cn.getB());
        this.b = (cn.getA() * this.b - cn.getB() * this.a) / (cn.getA() * cn.getA() + cn.getB() * cn.getB());
        this.a = temp;
    }

    @Author(name = "Rustam")
    public ComplexNumber div(ComplexNumber cn2) {
        ComplexNumber cn = new ComplexNumber(((this.a * cn2.getA() + this.b * cn2.getB()) / (cn2.getA() * cn2.getA() + cn2.getB() * cn2.getB())), (cn2.getA() * this.b - cn2.getB() * this.a) / (cn2.getA() * cn2.getA() + cn2.getB() * cn2.getB()));
        return cn;
    }


    @Author(name = "Alexander")
    public void multNumber2(double k) {
        this.a = k * this.a;
        this.b = k * this.b;
    }

    @Author(name = "Eugene")
    public ComplexNumber multNumber(double k) {
        ComplexNumber cn = new ComplexNumber(this.a * k, this.b * k);
        return cn;
    }

    @Author(name = "Marat")
    public double length() {
        return Math.sqrt(this.getA() * this.getA() + this.getB() * this.getB());
    }

    @Author(name = "Yura")
    public double cos() {
        return this.getA() / this.length();
    }

    @Author(name = "Rustam")
    public double sin() {
        return this.getB() / this.length();
    }

    @Author(name = "Alexander")
    public double arg() {
        return Math.toDegrees(Math.atan(this.b / this.a));
    }

    @Author(name = "Eugene")
    public void pow(double n) {
        double length = this.length();
        double arg = Math.toRadians(this.arg());
        this.a = Math.pow(length, n) * Math.cos(n * arg);
        this.b = Math.pow(length, n) * Math.sin(n * arg);
    }

    @Author(name = "Marat")
    public ComplexNumber pow2(double n) {
        ComplexNumber cn = new ComplexNumber(Math.pow(this.length(), n) * Math.cos(n * Math.toRadians(this.arg())), Math.pow(this.length(), n) * Math.sin(n * Math.toRadians(this.arg())));
        return cn;
    }
}