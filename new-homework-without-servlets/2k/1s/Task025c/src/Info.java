/**
 * Created by Alexander on 12/12/2015.
 */
public class Info {
    String name;
    int allMethodsLength;
    int methodsCount;
    double averageMethodLength;
    double averageParamLength;
    int allParamsLength;
    int paramsCount;
    int voidMethodsCount;
    int maxParamsInMethod;

    public int getMaxParamsInMethod() {
        return maxParamsInMethod;
    }

    public void setMaxParamsInMethod(int maxParamsInMethod) {
        this.maxParamsInMethod = maxParamsInMethod;
    }

    public int getVoidMethodsCount() {
        return voidMethodsCount;
    }

    public double getAverageParamLength() {
        return averageParamLength;
    }

    public void setAverageParamLength(double averageParamLength) {
        this.averageParamLength = averageParamLength;
    }

    public double getAverageMethodLength() {
        return averageMethodLength;
    }

    public void setAverageMethodLength(double averageMethodLength) {
        this.averageMethodLength = averageMethodLength;
    }

}
