/**
 * Created by Alexander on 30/11/15.
 */
public class RationalFraction implements Comparable<RationalFraction> {
    private double n;
    private double d;

    @Author(name = "Eugene")
    public double getN() {
        return this.n;
    }

    @Author(name = "Eugene")
    public double getD() {
        return this.d;
    }

    @Author(name = "Eugene")
    public void setN(int n) {
        this.n = n;
        this.reduce();
    }

    @Author(name = "Alexander")
    public void setD(int d) {
        this.d = d;
        this.reduce();
    }

    public RationalFraction() {
        this(0.0, 1.0);
    }

    public RationalFraction(Double n, Double d) {
        this.n = n;
        this.d = d;
        this.reduce();
    }

    @Author(name = "Rustam")
    public String toString() {
        return n + "/" + d;
    }

    @Author(name = "Rustam")
    public void reduce() {
        if (this.d < 0) {
            this.n = -this.n;
            this.d = -this.d;
        }

        if (this.n == 0) {
            this.d = 1;
        }


        double a = Math.abs(this.n);
        double b = this.d;
        double k = 1;

        while (a != 0 && b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
            k = a + b;
        }

        this.n /= k;
        this.d /= k;
    }


    @Author(name = "Rustam")
    public RationalFraction add(RationalFraction rf) {
        RationalFraction r = new RationalFraction(this.n * rf.getD() + rf.getN() * this.d, this.d * rf.getD());
        r.reduce();
        return r;
    }

    @Author(name = "Yura")
    public void add2(RationalFraction rf) {
        this.n = this.n * rf.getD() + rf.getN() * this.d;
        this.d *= rf.getD();
        this.reduce();
    }

    @Author(name = "Marat")
    public RationalFraction sub(RationalFraction rf) {
        RationalFraction r = new RationalFraction(this.n * rf.getD() - rf.getN() * this.d, this.d * rf.getD());
        r.reduce();
        return r;
    }

    @Author(name = "Marat")
    public void sub2(RationalFraction rf) {
        this.n = this.n * rf.getD() - rf.getN() * this.d;
        this.d *= rf.getD();
        this.reduce();
    }

    @Author(name = "Marat")
    public RationalFraction mult(RationalFraction rf) {
        RationalFraction r = new RationalFraction(this.n * rf.getN(), this.d * rf.getD());
        r.reduce();
        return r;
    }

    @Author(name = "Alexander")
    public void mult2(RationalFraction rf) {
        this.n *= rf.getN();
        this.d *= rf.getD();
        this.reduce();
    }

    @Author(name = "Yura")
    public RationalFraction div(RationalFraction rf) {

        RationalFraction r = new RationalFraction(this.n * rf.getD(), this.d * rf.getN());
        r.reduce();
        return r;
    }

    @Author(name = "Alexander")
    public void div2(RationalFraction rf) {
        this.n *= rf.getD();
        this.d *= rf.getN();
        this.reduce();
    }

    @Author(name = "Yura")
    public double value() {
        return (double) this.getN() / this.getD();
    }

    @Author(name = "Marat")
    public boolean equals(RationalFraction rf) {
        return this.getN() == rf.getN() && this.getD() == rf.getD();
    }

    @Author(name = "Alexander")
    public double numberPart() {
        return this.n / this.d;
    }

    @Author(name = "Marat")
    @Override
    public int compareTo(RationalFraction o) {
        final double EPS = 1e-9;
        double d1 = this.value();
        double d2 = o.value();
        if (Math.abs(d1 - d2) < EPS) {
            return 0;
        }
        if (d1 > d2) {
            return 1;
        } else {
            return -1;
        }
    }
}

