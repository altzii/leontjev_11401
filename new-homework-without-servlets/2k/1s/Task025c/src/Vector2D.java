/**
 * Created by Alexander on 30/11/15.
 */
public class Vector2D implements Comparable<Vector2D> {
    private double x;
    private double y;

    @Author(name = "Eugene")
    public double getX() {
        return this.x;
    }

    @Author(name = "Marat")
    public double getY() {
        return this.y;
    }

    @Author(name = "Rustam")
    public void setX(double x) {
        this.x = x;
    }

    @Author(name = "Yura")
    public void setY(double y) {
        this.y = y;
    }


    public Vector2D() {
        this(0, 0);
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Author(name = "Alexander")
    public String toString() {
        return "{" + x + "," + y + "}";
    }

    public Vector2D add(Vector2D v) {
        Vector2D cn = new Vector2D(this.x + v.getX(), this.y + v.getY());
        return cn;
    }

    @Author(name = "Yura")
    public void add2(Vector2D v) {
        this.x = this.x + v.getX();
        this.y = this.y + v.getY();
    }

    @Author(name = "Rustam")
    public Vector2D sub(Vector2D v) {
        Vector2D cn = new Vector2D(this.x - v.getX(), this.y - v.getY());
        return cn;
    }

    @Author(name = "Marat")
    public void sub2(Vector2D v, int b) {
        this.x = this.x - v.getX();
        this.y = this.y - v.getY();
    }

    @Author(name = "Yura")
    public Vector2D mult(double k) {
        Vector2D cn = new Vector2D(this.x * k, this.y * k);
        return cn;
    }

    @Author(name = "Eugene")
    public void mult2(double k) {
        this.x = this.x * k;
        this.y = this.y * k;
    }

    @Author(name = "Alexander")
    public double length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    @Author(name = "Yura")
    public double scalarProduct(Vector2D v) {
        return this.x * v.getX() + this.y * v.getY();
    }

    @Author(name = "Rustam")
    public double cos(Vector2D v) {
        return this.scalarProduct(v) / (this.length() * v.length());
    }

    @Author(name = "Marat")
    public boolean equals(Vector2D v) {
        return this.x == v.getX() && this.y == v.getY();
    }

    @Author(name = "Eugene")
    @Override
    public int compareTo(Vector2D v) {
        return (int) Math.signum(this.length() - v.length());
    }
}