import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by Alexander on 31/10/15.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        Scanner scanner = new Scanner(System.in);
        ObjectOutputStream os = new ObjectOutputStream(s.getOutputStream());
        ObjectInputStream is = new ObjectInputStream(s.getInputStream());

        System.out.println("Введите имя:");
        String username = scanner.nextLine();
        System.out.println(username + ", добро пожаловать в чат, можете начинать общение! :)\n");

        try {
            for (Message message : (ArrayList<Message>) is.readObject()) {
                System.out.println(message);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        while (true) {
            String input = scanner.nextLine();
            Message input_message = new Message(username, input);

            os.writeObject(input_message);
            os.flush();
            os.reset();

            try {
                ArrayList<Message> messages = (ArrayList<Message>) is.readObject();
                for (Message message : messages) {
                    System.out.println(message);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
