import java.io.*;
import java.net.Socket;
import java.util.ArrayList;


/**
 * Created by Alexander on 09/11/15.
 */
class Connection implements Runnable {
    Socket socket;
    Thread thread;
    Server server;


    public Connection(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        ObjectInputStream is = null;
        ObjectOutputStream os = null;
        try {
            os = new ObjectOutputStream(socket.getOutputStream());
            is = new ObjectInputStream(socket.getInputStream());

            os.reset();
            os.flush();
            os.writeObject(server.data);

            int i = server.data.size();

            while (true) {
                Message message = (Message) is.readObject();
                server.data.add(message);
                int j = 0;
                ArrayList<Message> new_data = new ArrayList<Message>();

                for (j = i; j < server.data.size(); j++) {
                    new_data.add(server.data.get(j));
                }
                i = j;
                os.reset();
                os.flush();
                os.writeObject(new_data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
