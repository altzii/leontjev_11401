import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Alexander on 14/11/15.
 */
public class Message implements Serializable {
    String username;
    String message;
    String date;

    public String toString() {
        return this.date + " " + username + ": " + message;
    }

    public Message(String username, String message) {
        Date d = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm");

        this.username = username;
        this.message = message;
        this.date = format.format(d);
    }
}
