import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


/**
 * Created by Alexander on 31/10/15.
 */
public class Server {
    final int PORT = 3456;
    ArrayList<Connection> connections;
    ArrayList<Message> data = new ArrayList<Message>();

    public Server() throws IOException {
        connections = new ArrayList<Connection>();
        go();
    }

    public void go() throws IOException {
        ServerSocket s1 = new ServerSocket(PORT);
        while (true) {
            Socket client = s1.accept();
            connections.add(new Connection(this, client));
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
    }
}