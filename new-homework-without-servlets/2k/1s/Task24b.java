import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexander on 23/11/15.
 */
public class Task24b {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://ru.wikipedia.org/wiki/%D0%9B%D0%BE%D0%BD%D0%B4%D0%BE%D0%BD");
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(url.openStream()));

        String s = reader.readLine();
        Pattern pattern = Pattern.compile("\"(?<url>https?://[^\"\' ,]+?/(?<filename>[^/]+?\\.(pdf|mp3))).*?\"");

        while (s != null) {
            Matcher matcher = pattern.matcher(s);
            while (matcher.find()) {
                try {
                    URL fileUrl = new URL(matcher.group("url"));

                    BufferedInputStream s1 = new BufferedInputStream(fileUrl.openStream());
                    FileOutputStream f1 = new FileOutputStream(new File(matcher.group("filename")));

                    byte[] mas = new byte[4096];
                    int n = s1.read(mas);

                    while (n >= 0) {
                        f1.write(mas, 0, n);
                        n = s1.read(mas);
                    }
                    s1.close();
                    f1.close();
                } catch (IOException e) {
                    System.out.println("Can't download, wrong link: " + matcher.group("url"));
                }
            }
            s = reader.readLine();
        }
        reader.close();
    }
}
