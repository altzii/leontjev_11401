import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Sapper
 */
public class Sapper extends JFrame {
    private int size = 0;
    private int minesNumber;
    private int buttonSize = 40;
    private byte[][] field;
    JButton[][] buttons;
    int winCount;


    public Sapper(int size, int minesNumber) {
        this.size = size;
        this.minesNumber = minesNumber;
        this.buttons = new JButton[size][size];
        setBounds(50, 50, buttonSize * size + 1, buttonSize * size + 25);
        setLayout(new GroupLayout(getContentPane()));
        generateField();


        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                JButton jb = new JButton();
                jb.setBounds(j * buttonSize, i * buttonSize, buttonSize, buttonSize);
                buttons[i][j] = jb;

                jb.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON1) {
                            JButton jb1 = (JButton) e.getSource();
                            int i = jb1.getY() / buttonSize;
                            int j = jb1.getX() / buttonSize;

                            if (field[i][j] == 1) {
                                for (int m = 0; m < size; m++) {
                                    for (int n = 0; n < size; n++) {
                                        if (field[m][n] == 1) {
                                            buttons[m][n].setText("X");
                                        }
                                        buttons[m][n].setEnabled(false);
                                    }
                                }
                                JOptionPane.showMessageDialog(null, "GAME OVER!");
                                newGame();


                            } else {
                                Queue<JButton> queue = new LinkedList<>();
                                queue.add(jb1);

                                while (!queue.isEmpty()) {
                                    JButton jb = queue.poll();

                                    int x = jb.getY() / buttonSize;

                                    int y = jb.getX() / buttonSize;

                                    if (jb.isEnabled() && field[i][j] == 0) {
                                        int count = howManyMines(x, y);
                                        if (count > 0) {
                                            jb.setText(String.valueOf(count));
                                        } else {
                                            for (int k = -1; k <= 1; k++) {
                                                if (x + k >= 0 && x + k < size) {
                                                    for (int l = -1; l <= 1; l++) {
                                                        if (y + l >= 0 && y + l < size) {
                                                            queue.add(buttons[x + k][y + l]);
                                                        }
                                                    }
                                                }

                                            }

                                        }
                                        winCount++;
                                        System.out.println(winCount);
                                        jb.setText(String.valueOf(count));
                                        jb.setEnabled(false);
                                    }
                                }
                                if (winCount == size * size - minesNumber) {
                                    JOptionPane.showMessageDialog(null, "CONGRATZ!");
                                    newGame();
                                }
                            }
                        }

                        if (e.getButton() == MouseEvent.BUTTON3) {
                            JButton jb = (JButton) e.getSource();
                            if (jb.getText().equals("F")) {
                                jb.setText("");
                            } else if (jb.getText().equals("")) {
                                jb.setText("F");
                            }
                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                add(jb);
            }
        }
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }


    private int howManyMines(int x, int y) {
        int s = 0;
        for (int i = -1; i <= 1; i++) {
            if (x + i >= 0 && x + i < size) {
                for (int j = -1; j <= 1; j++) {
                    if (y + j >= 0 && y + j < size) {
                        s += field[x + i][y + j];
                    }
                }
            }
        }
        return s;
    }


    private void generateField() {
        field = new byte[size][size];
        int k = 0;
        Random r = new Random();

        while (k < minesNumber) {
            int i = r.nextInt(size);
            int j = r.nextInt(size);

            if (field[i][j] == 0) {
                field[i][j] = 1;
                k++;
            }
        }
    }

    private void newGame() {
        generateField();
        winCount = 0;
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++) {
                buttons[i][j].setEnabled(true);
                buttons[i][j].setText("");
            }
        update(getGraphics());
    }

    public static void main(String[] args) {
        new Sapper(10, 10);
    }
}
