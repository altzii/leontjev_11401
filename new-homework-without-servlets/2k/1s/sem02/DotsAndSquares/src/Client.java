import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by Alexander on 19/12/2015.
 */
public class Client {
    public static DataOutputStream os;
    public static DataInputStream is;
    public static Socket socket;
    public static DotsAndSquares dotsAndSquares;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
         /*

         boolean inputIp = false;

        while (!inputIp) {
            String input = JOptionPane.showInputDialog(null, "Введите ip адрес сервера", null, JOptionPane.CLOSED_OPTION);

            if (input.length() > 0) {
                try {
                    socket = new Socket(input.split(":")[0], Integer.parseInt(input.split(":")[1]));
                    inputIp = true;
                } catch (SocketException s) {
                    inputIp = false;
                } catch (UnknownHostException u) {
                    inputIp = false;
                }
            }
        }
        */

        socket = new Socket("localhost", 3456);

        dotsAndSquares = new DotsAndSquares(3);
        os = new DataOutputStream(socket.getOutputStream());
        is = new DataInputStream(socket.getInputStream());

        while (true) {
            int inputId = is.readInt();
            Stick stick = dotsAndSquares.sticks[inputId];
            stick.setBackground(Color.BLUE);
            stick.setEnabled(false);


            dotsAndSquares.info.setForeground(Color.GREEN);
            dotsAndSquares.info.setText("Ваш ход");
            Client.dotsAndSquares.setEnabled(true);

            if (stick.getLeft() != null) {
                ++stick.getLeft().counter;
                stick.setEnabled(false);
                if (stick.getLeft().counter == 4) {
                    stick.getLeft().setBackground(Color.BLUE);
                    stick.getLeft().own = Step.Enemy;
                }
            }
            if (stick.getRight() != null) {
                ++stick.getRight().counter;
                stick.setEnabled(false);
                if (stick.getRight().counter == 4) {
                    stick.getRight().setBackground(Color.BLUE);
                    stick.getRight().own = Step.Enemy;
                }
            }
        }
    }
}
