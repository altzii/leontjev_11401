import java.io.*;
import java.net.Socket;

/**
 * Created by Alexander on 19/12/2015.
 */
public class Connection implements Runnable, Serializable {
    Socket socket1;
    Socket socket2;
    Thread thread;
    Server server;
    DataInputStream in1;
    DataOutputStream out1;
    DataInputStream in2;
    DataOutputStream out2;


    public Connection(Server server, Socket socket1, Socket socket2) {
        this.socket1 = socket1;
        this.socket2 = socket2;
        this.server = server;

        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try {
            out1 = new DataOutputStream(socket1.getOutputStream());
            in1 = new DataInputStream(socket1.getInputStream());
            out2 = new DataOutputStream(socket2.getOutputStream());
            in2 = new DataInputStream(socket2.getInputStream());

            while (true) {
                int inputId = in1.readInt();
                out2.flush();
                out2.writeInt(inputId);

                int inputId2 = in2.readInt();
                out1.flush();
                out1.writeInt(inputId2);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
