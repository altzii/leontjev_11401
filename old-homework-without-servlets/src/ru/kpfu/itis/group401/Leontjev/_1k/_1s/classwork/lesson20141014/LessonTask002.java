package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141014;

import java.util.Scanner;

public class LessonTask002 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int b;

        int[][] a = new int[m][n];


        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = i; j < n; j++) {
                b = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = b;

            }
        }

        for (int i = 0; i < m; i++) {
            System.out.println();
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j]);

            }
        }
    }
}
