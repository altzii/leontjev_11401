package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141111;

public class Student {
    private String fio;
    private int year;
    private int money;
    private int id;
    private static int letItBe = 0; //кол-во студентов

    public int getMoney() {
        return this.money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Student(String fio, int year, int money, int id) {
        letItBe++;
        this.fio = fio;
        this.year = year;
        this.money = money;
        this.id = letItBe;

    }

    public String getFio() {
        return this.fio;
    }

    public int getYear() {
        return this.year;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void saysHiTo(Student stud) {
        System.out.println("Я, " + this.fio +
                ", приветсвую студента с именем " + stud.getFio());

    }

    public void donateMoney(Student student2, int i) {
        if (money >= i) {
            student2.setMoney(student2.getMoney() + i);
            this.money -= i;
        } else {
            System.out.println("Sorry, no money");
        }
    }

    public String toString() {
        return fio;
    }

    public boolean equals(Student s) {
        return (this.fio.equals(s.getFio()) && this.year == s.getYear());
    }

    public static int getLetItBe() {
        return letItBe;
    }
}




