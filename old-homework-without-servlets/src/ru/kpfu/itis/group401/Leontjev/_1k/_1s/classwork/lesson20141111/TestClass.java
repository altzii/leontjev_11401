package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141111;

public class TestClass {

    public static void main(String[] args) {

        Student student = new Student("Каримова А.И.", 1996, 250, 0);
        Student student2 = new Student("Татарских Р.А.", 1996, 100, 0);

        System.out.println(student2);


        student.saysHiTo(student2);
        student2.saysHiTo(student);

        student.donateMoney(student2, 100);

        System.out.println(student2);

        System.out.println(student2.equals(student));

        System.out.println(student2.getFio() + " - Id = " + student2.getId());
        System.out.println(student.getFio() + " - Id = " + student.getId());

        System.out.println("Количество студентов = " + Student.getLetItBe());
    }
}

