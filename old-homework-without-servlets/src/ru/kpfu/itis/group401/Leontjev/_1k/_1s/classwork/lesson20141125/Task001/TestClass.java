package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task001;

public class TestClass {
    public static void main(String[] args) {
        int n = 10;
        Voiceable[] animals = new Voiceable[n];

        for (int i = 0; i < n; i += 2) {
            animals[i] = new Dog();
            animals[i + 1] = new Cat();
        }

        for (Voiceable animal : animals) {
            animal.voice();
        }
    }
}
