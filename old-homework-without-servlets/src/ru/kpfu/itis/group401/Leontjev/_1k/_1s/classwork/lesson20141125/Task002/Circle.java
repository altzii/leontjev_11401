package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002;

/**
 * Created by Alexander on 25.11.14.
 */

public class Circle implements Perimeterable, Meosurable {

    @Override
    public double Perimeter() {
        return 2 * Math.PI * this.r;
    }

    @Override
    public double Square() {
        return Math.PI * this.r * this.r;
    }

    private double r;

    public Circle(double r) throws GeometricException {
        if (r <= 0) {
            throw new GeometricException("oops! r cannot be negative");
        }
        this.r = r;
    }


}

