package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002;

/**
 * Created by Alexander on 03.12.14.
 */

public class GeometricException extends Exception {

    public GeometricException(String s) {
        super(s);
    }
}
