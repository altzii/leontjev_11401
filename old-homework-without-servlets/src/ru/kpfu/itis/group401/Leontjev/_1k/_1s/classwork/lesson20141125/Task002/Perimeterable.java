package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002;

/**
 * Created by Alexander on 25.11.14.
 */

public interface Perimeterable {
    public double Perimeter();
}
