package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002;

/**
 * Created by Alexander on 25.11.14.
 */

public class Rectangle implements Meosurable, Perimeterable {
    private double a;
    private double b;

    public Rectangle(double a, double b) throws GeometricException {

        if ((a <= 0) || (b <= 0)) {
            throw new GeometricException("oops! a and b cannot be negative");
        }

        this.a = a;
        this.b = b;
    }

    @Override
    public double Square() {
        return this.a * this.b;
    }

    @Override
    public double Perimeter() {
        return 2 * (this.a + this.b);
    }
}
