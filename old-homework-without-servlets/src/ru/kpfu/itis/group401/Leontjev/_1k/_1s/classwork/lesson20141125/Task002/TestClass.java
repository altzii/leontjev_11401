package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002;

/**
 * Created by Alexander on 25.11.14.
 */

public class TestClass {
    public static void main(String[] args) throws Exception {
        try {
            Circle c = new Circle(-5);
            System.out.println("c.Perimeter  = " + c.Perimeter() + "\n" + "Square = " + c.Square());

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Triangle t = new Triangle(3, 4, 5);
            System.out.println("t.Perimeter = " + t.Perimeter() + "\n" + "Square = " + t.Square());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Rectangle r = new Rectangle(3, -4);
            System.out.println("t.Perimeter = " + r.Perimeter() + "\n" + "Square = " + r.Square());

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*

        Circle c = new Circle(-5);
        System.out.println("c.Perimeter  = " + c.Perimeter() + "\n" + "Square = " + c.Square());

        Triangle t = new Triangle(3, 4, 5);
        System.out.println("t.Perimeter = " + t.Perimeter() + "\n" + "Square = " + t.Square());

        Rectangle r = new Rectangle(3, -4);
        System.out.println("t.Perimeter = " + r.Perimeter() + "\n" + "Square = " + r.Square());

        */
    }
}
