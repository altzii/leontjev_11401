package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002;

/**
 * Created by Alexander on 25.11.14.
 */

public class Triangle implements Meosurable, Perimeterable {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) throws GeometricException {

        if ((a <= 0) || (b <= 0) || (c <= 0)) {
            throw new GeometricException("oops! a,b,c cannot be negative");
        }

        if ((a + b <= c) || (a + c <= b) || (b + c <= a)) {
            throw new GeometricException("oops! triangle with such a,b,c doesn't exist");
        }

        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double Square() {
        double p = this.Perimeter() / 2;
        return Math.sqrt(p * (p - this.a) * (p - this.b) * (p - this.c));
    }

    @Override
    public double Perimeter() {
        return this.a + this.b + this.c;
    }

}
