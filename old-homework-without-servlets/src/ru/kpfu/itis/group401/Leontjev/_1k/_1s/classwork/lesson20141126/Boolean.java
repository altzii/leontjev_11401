package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141126;

/**
 * Created by Alexander on 26.11.14.
 */

public class Boolean {
    private boolean bool;

    public Boolean() {
        this("false");
    }

    public Boolean(String string) {
        if (string.equals("true")) {
            this.bool = true;
        }
        if (string.equals("false")) {
            this.bool = false;
        }
    }

    public Boolean and(Boolean b) {
        Boolean bln = new Boolean();
        bln.bool = this.bool && b.bool;
        return bln;
    }

    public Boolean or(Boolean b) {
        Boolean bln = new Boolean();
        bln.bool = this.bool || b.bool;
        return bln;
    }

    public Boolean not() {
        Boolean bln = new Boolean();
        bln.bool = !this.bool;
        return bln;
    }

    public String toString() {
        return this.bool + "";
    }
}

