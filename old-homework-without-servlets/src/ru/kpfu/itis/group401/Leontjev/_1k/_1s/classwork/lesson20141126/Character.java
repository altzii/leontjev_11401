package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141126;

/**
 * Created by Alexander on 26.11.14.
 */

public class Character {
    private char ch;

    public Character(char c) {
        this.ch = c;
    }

    public Character() {
        this(' ');
    }

    public char Char() {
        return this.ch;
    }


    public int ord() {
        return (int) this.ch;
    }

    public boolean isDigit() {
        return (this.ch <= '9') && (this.ch >= '0');
    }

    public boolean isLower() {
        return (this.ch <= 'z') && (this.ch >= 'a') || (this.ch <= 'я') && (this.ch >= 'а');
    }

    public boolean isUpper() {
        return (this.ch <= 'Z') && (this.ch >= 'A') || (this.ch <= 'Я') && (this.ch >= 'А');
    }
}
