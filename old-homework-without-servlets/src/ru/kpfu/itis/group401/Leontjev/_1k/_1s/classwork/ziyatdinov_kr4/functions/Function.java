package ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.ziyatdinov_kr4.functions;


abstract class Function {
    abstract public double doubleEval(double x);
}

class Sin extends Function {
    private double a;
    private int b;
    private double x;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public int getB() {
        return b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public Sin(double a, int b) {
        this.a = a;
        this.b = b;
    }

    public double doubleEval(double x) {
        this.x = x;
        return this.a * Math.sin(this.b * x);
    }

    public void stringShow() {
        System.out.println(a + " * sin(" + x * b + ")");

    }
}

class Exp extends Function {
    private double a;
    private int b;
    private double x;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public int getB() {
        return b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public Exp(double a, int b) {
        this.a = a;
        this.b = b;
    }

    public double doubleEval(double x) {
        this.x = x;
        return this.a * Math.pow(Math.E, x * this.b);
    }

    public void stringShow() {
        System.out.println(a + " * e^" + x * b);
    }
}

class TestClass {
    public static void main(String[] args) {
        Sin sin = new Sin(2, 3);
        Exp exp = new Exp(2, 3);

        System.out.println(sin.doubleEval(3));
        sin.stringShow();

        System.out.println(exp.doubleEval(3));
        exp.stringShow();
    }
}










