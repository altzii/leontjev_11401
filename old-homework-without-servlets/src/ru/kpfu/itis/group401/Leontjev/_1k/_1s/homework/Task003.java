package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         003
 */

public class Task003 {
    public static void main(String[] args) {

        int R = 2;

        System.out.println(Math.PI * R * R * R * 4 / 3);

    }
}
