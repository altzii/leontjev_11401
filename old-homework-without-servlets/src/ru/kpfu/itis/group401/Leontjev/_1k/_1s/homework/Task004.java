package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         004
 */

public class Task004 {
    public static void main(String[] args) {

        double x = 5;
        double y = 2;
        double a, b;

        a = x * x;
        a -= 4;
        a = 1 / a;
        a += y;
        b = x + y;
        a = b / a;
        b = y * y;
        b += x;    //2*x заменил на x + x, чтобы не вводить новую переменную
        b += x;
        a = b - a;
        b = 1 + y;
        a *= b;


        System.out.println(a);
    }

}
