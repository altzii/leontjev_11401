package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         005
 */

public class Task005 {
    public static void main(String[] args) {

        double x = 3;
        double y = 8;
        double z = 2;

        x = x + 2;
        x = x * y;
        x = x - z;
        x = x / y;
        z = z * y;
        y = z + x;

        System.out.println(y);

    }
}
