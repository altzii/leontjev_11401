package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         006
 */

public class Task006 {
    public static void main(String[] args) {

        int x = 2;
        int y;

        y = x + 6;
        y *= x;
        y += 10;
        y *= x;
        y += 25;
        y *= x;
        y += 30;
        y *= x;
        y += 101;

        System.out.println(y);

    }
}
