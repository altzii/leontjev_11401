package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         008
 */

public class Task008 {
    public static void main(String[] args) {

        int k = 3;

        System.out.println("2 * " + k + " = " + (k * 2));
        System.out.println("3 * " + k + " = " + (k * 3));
        System.out.println("4 * " + k + " = " + (k * 4));
        System.out.println("5 * " + k + " = " + (k * 5));
        System.out.println("6 * " + k + " = " + (k * 6));
        System.out.println("7 * " + k + " = " + (k * 7));
        System.out.println("8 * " + k + " = " + (k * 8));
        System.out.println("9 * " + k + " = " + (k * 9));

    }
}

