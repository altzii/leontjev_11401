package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         009
 */

import java.util.Scanner;

public class Task009 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        double y;

        if (x > 2) {
            y = (x * x - 1) / (x + 2);
        } else if (x <= 0) {
            y = x * x * (1 + 2 * x);
        } else {
            y = (x * x - 1) * (x + 2);
        }

        System.out.println(y);
        scanner.close();
    }
}
