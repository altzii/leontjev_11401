package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;
/**
 * @author Alexander Leontjev
 *         11-401
 *         010
 */

import java.util.Scanner;

public class Task010 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        double y;

        y = (x > 2) ? (x * x - 1) / (x + 2) : (x <= 0) ? x * x * (1 + 2 * x) : (x * x - 1) * (x + 2);

        System.out.println(y);
        scanner.close();
    }
}
