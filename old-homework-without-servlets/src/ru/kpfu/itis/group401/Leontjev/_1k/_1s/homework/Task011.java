package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         011
 */

import java.util.Scanner;

public class Task011 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int result = 1;
        int k = n % 2;

        for (int i = 1; i <= n / 2; i++) {
            k += 2;
            result *= k;
        }

        System.out.println(result);
        scanner.close();
    }
}

