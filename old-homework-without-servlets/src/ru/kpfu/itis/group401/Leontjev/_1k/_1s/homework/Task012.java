package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         012
 */

import java.util.Scanner;

public class Task012 {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        double s = 0;
        int n = scanner.nextInt();
        int k = 1;

        for (int i = 1; i <= n; i++) {

            s += (1.0 / ((2 * i - 1) * (2 * i - 1))) * k;
            k *= -1;

        }

        System.out.println(s);
        scanner.close();


    }
}

