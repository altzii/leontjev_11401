package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         013
 */

import java.util.Scanner;

public class Task013 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double r = 1;
        int n = scanner.nextInt();

        for (int i = 1; i <= n; i++) {
            r *= (4.0 * i * i) / (4 * i * i - 1);
        }

        System.out.println(r);
        scanner.close();
    }
}

