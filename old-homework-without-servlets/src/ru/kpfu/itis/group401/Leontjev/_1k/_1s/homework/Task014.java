package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         014
 */

import java.util.Scanner;

public class Task014 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double result = 0;
        int n = scanner.nextInt();
        double x = scanner.nextDouble();


        for (int i = 1; i <= n; i++) {
            result = Math.cos(result + x);
            // 1 2 telo 3 2 telo 3 2 telo...
        }

        System.out.println(result);
        scanner.close();
    }
}
