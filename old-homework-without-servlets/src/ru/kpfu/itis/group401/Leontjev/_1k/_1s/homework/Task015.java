package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;
/**
 * @author Alexander Leontjev
 *         11-401
 *         015
 */

import java.util.Scanner;

public class Task015 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double x = scanner.nextDouble();
        double result = 0;
        double a = x;

        // 1 2 telo 3 2 telo 3 2 telo...
        for (int i = n; i >= 1; i--) {
            result = i + a;
            a = x / (i + a);
        }

        System.out.println(result);
        scanner.close();
    }
}