package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         016
 */

import java.util.Scanner;

public class Task016 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double x = scanner.nextDouble();
        double result = 0;
        double k = 1;

        for (int i = 1; i <= n; i++) {


            k = k * (x + i);
            result = result + k;
        }

        System.out.println(result);
        scanner.close();
    }
}