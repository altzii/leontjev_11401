package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;
/**
 * @author Alexander Leontjev
 *         11-401
 *         017
 */

import java.util.Scanner;

public class Task017 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        double f1 = 1;
        double f2 = 2;

        double sum = f1 * f1 / f2;


        for (int i = 2; i <= n; i++) {


            f1 *= (i - 1);
            f2 *= 2 * i * (2 * i - 1);

            sum += f1 * f1 / f2;

        }

        System.out.println(sum);
        scanner.close();
    }
}