package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         018
 */

import java.util.Scanner;

public class Task018 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        double x = scanner.nextDouble();

        double result = 0;

        double a = scanner.nextDouble();

        result += a;

        for (int i = 1; i <= n; i++) {

            result *= x;
            a = scanner.nextInt();
            result += a;
        }

        scanner.close();
        System.out.println(result);
    }
}

