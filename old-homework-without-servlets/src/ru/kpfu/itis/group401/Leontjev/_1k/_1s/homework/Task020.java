package ru.kpfu.itis.group401.Leontjev._1k._1s.homework; /**
 * @author Alexander Leontjev
 *         11-401
 *         020
 */

import java.util.Scanner;

public class Task020 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();


        for (int i = 1; i <= n; i++) {


            for (int j = 1; j <= n - i; j++) {

                System.out.print('*');
            }

            for (int j = 1; j <= 2 * i - 1; j++) {


                System.out.print('0');
            }

            for (int j = 1; j <= n - i; j++) {


                System.out.print('*');
            }

            System.out.println();

        }

        for (int i = n - 1; i >= 1; i--) {


            for (int j = 1; j <= n - i; j++) {


                System.out.print('*');
            }

            for (int j = 1; j <= 2 * i - 1; j++) {


                System.out.print('0');
            }

            for (int j = 1; j <= n - i; j++) {


                System.out.print('*');
            }

            System.out.println();
        }

        scanner.close();
    }
}