package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         021
 */

import java.util.Scanner;

public class Task021 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 1; i <= 2 * n + 1; i++) {
            for (int j = 1; j <= 2 * n + 1; j++) {

                if ((j - (n + 1)) * (j - (n + 1)) + (i - n - 1) * (i - n - 1) <= n * n) {
                    System.out.print('0');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();


        }
    }
}


