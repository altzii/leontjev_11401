package ru.kpfu.itis.group401.Leontjev._1k._1s.homework; /**
 * @author Alexander Leontjev
 *         11-401
 *         022
 */

import java.util.Scanner;

public class Task022 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();


        for (int i = 1; i <= n; i++) {


            for (int j = 1; j <= 2 * n - i; j++) {
                System.out.print(' ');
            }

            for (int j = 1; j <= 2 * i - 1; j++) {
                System.out.print('0');
            }


            System.out.println();

        }

        System.out.println();

        for (int i = 1; i <= n; i++) {


            for (int j = 1; j <= n - i; j++) {
                System.out.print(' ');
            }

            for (int j = 1; j <= 2 * i - 1; j++) {
                System.out.print('0');
            }

            for (int j = 1; j <= 2 * n - 2 * i + 1; j++) {
                System.out.print(' ');
            }


            for (int j = 1; j <= 2 * i - 1; j++) {
                System.out.print('0');
            }


            System.out.println();

        }

        scanner.close();

    }
}