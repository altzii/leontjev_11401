package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         023
 */

public class Task023 {
    public static void main(String[] args) {

        final double EPS = 1e-9;
        double sum = 0;
        double k = 1;

        for (int n = 1; Math.abs(k) > EPS; n++) {
            k = (2.0 * n + 3) / (5.0 * n * n * n * n + 1);
            sum += k;
        }
        System.out.println(sum);
    }
}