package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         024
 */

public class Task024 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final double EPS = 1e-9;
        double sum = 0;
        double k = 1;
        double x = scanner.nextDouble();
        x = (x - 1) * (x - 1);
        double d = 1;

        for (int n = 1; Math.abs(k) > EPS; n++) {
            d = d * x * 9;
            k = 1.0 / (n * d);
            sum += k;
        }
        System.out.println(sum);
        scanner.close();
    }
}

