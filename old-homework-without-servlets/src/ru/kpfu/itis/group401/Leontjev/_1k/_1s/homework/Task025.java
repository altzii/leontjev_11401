package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         025
 */

public class Task025 {
    public static void main(String[] args) {

        final double EPS = 1e-9;
        double sum = 0;
        double k = 1;
        double z = 1;

        for (int n = 1; Math.abs(k) > EPS; n++) {

            k = z / (n * (n + 3));
            sum += k;
            z *= -1;
        }
        System.out.println(sum);
    }
}

