package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         026
 */

public class Task026 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final double EPS = 1e-9;
        double sum = 0;
        double k = 1;
        double x = scanner.nextDouble();
        double z = 1;
        double f = 1;

        for (int n = 1; Math.abs(k) > EPS; n++) {
            z = z * (x - 1) / 3;
            f *= n;
            k = z / ((n * n + 3) * f);
            sum += k;
        }
        System.out.println(sum);
        scanner.close();
    }
}



