package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         027
 */

public class Task027 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int i = 1;
        boolean flag = false;

        while ((i <= n) && (!flag)) {
            int a = scanner.nextInt();
            flag = a % 6 == 0;
            i++;
        }

        System.out.println(flag);
    }
}
