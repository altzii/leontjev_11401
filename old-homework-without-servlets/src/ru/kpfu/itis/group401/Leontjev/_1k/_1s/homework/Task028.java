package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         028
 */

public class Task028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int k = scanner.nextInt();
        int m = scanner.nextInt();

        while (k % 3 != 0) {
            k++;
        }

        while (k <= m) {
            System.out.println(k);
            k += 3;
        }
    }
}


