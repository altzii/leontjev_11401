package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         029
 */

public class Task029 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int k = scanner.nextInt();
        int n = scanner.nextInt();
        int answer = 0;
        int i = 1;

        while (n > 0) {
            answer += n % 10 * i;
            i *= k;
            n /= 10;
        }
        System.out.println(answer);
    }
}
