package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         030
 */

public class Task030 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        boolean flag = true;
        boolean flag1;
        boolean flag2;
        int i = 1;
        int k = 0;

        while ((k < 3) && (i <= n)) {

            int x = scanner.nextInt();
            flag1 = ((x > 99) && (x < 1000)) || ((x > 9999) && (x < 100000));
            flag2 = x % 2 == 0;
            flag = flag1;

            while ((flag) && (x > 9)) {
                x /= 10;
                flag = ((x % 2 == 0) == flag2);
            }

            if (flag) {
                k++;
            }
            i++;
            flag = true;
        }
        if (k == 2) System.out.println("TRUE");
        else System.out.println("FALSE");
    }
}

