package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         031
 */

public class Task031 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] a = new int[n];
        boolean flag = true;
        int i;

        for (i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        i = 2;
        while ((i < n) & flag) {
            flag = a[i] % 3 == 0;
            i += 3;
        }

        if (flag) {
            int s = 0;
            i = 0;
            while (i < n) {
                if (a[i] > 0) {
                    s += a[i];
                }
                i++;
            }
            System.out.println(s);
        } else {
            i = 0;
            int p = 1;
            while (i < n) {
                if (a[i] > 0) {
                    p *= a[i];

                }
                i++;
            }
            System.out.println(p);
        }
    }
}

