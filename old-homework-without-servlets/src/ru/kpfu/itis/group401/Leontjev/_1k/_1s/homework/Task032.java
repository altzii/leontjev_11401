package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         032
 */

public class Task032 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int l = Math.max(m, n) + 1;
        int i;

        int[] a = new int[n];
        int[] b = new int[m];
        int[] c = new int[l];

        for (i = 0; i < n; i++)
            a[i] = scanner.nextInt();

        for (i = 0; i < m; i++)
            b[i] = scanner.nextInt();

        l = l - 1;
        for (i = n - 1; i >= 0; i--) {
            c[l] += a[i];
            l--;
        }

        l = Math.max(m, n);

        for (i = m - 1; i >= 0; i--) {
            c[l] += b[i];
            l--;
        }

        l = Math.max(m, n);

        for (i = l; i > 0; i--)

            if (c[i] >= 10) {
                c[i] = c[i] % 10;
                c[i - 1]++;
            }

        if (c[0] == 0) {
            i = 1;
        } else {
            i = 0;
        }


        while (i <= l) {
            System.out.print(c[i]);
            i++;
        }
    }
}
