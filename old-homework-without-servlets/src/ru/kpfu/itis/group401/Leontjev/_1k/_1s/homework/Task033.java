package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         033
 */

public class Task033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int a[] = new int[n];
        int b[] = new int[n];
        double s = 0;
        double s1 = 0;
        double s2 = 0;
        int i;

        for (i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        for (i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }

        for (i = 0; i < n; i++) {
            s += a[i] * b[i];
            s1 += a[i] * a[i];
            s2 += b[i] * b[i];
        }

        double cos = s / (Math.sqrt(s1) * Math.sqrt(s2));

        System.out.println("Скалярное произведение векторов = " + s);
        System.out.println("Косинус угла между векторами =  " + cos);
    }
}
