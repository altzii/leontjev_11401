package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         034
 */

public class Task034 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int a[] = new int[n];
        int i;
        int max = 0;
        int temp;

        for (i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        max = a[0] + a[1] + a[2];
        temp = max;

        for (i = 1; i < n - 2; i++) {
            temp = temp - a[i - 1] + a[i + 2];

            if (temp > max) {
                max = temp;
            }
        }
        System.out.println(max);
    }
}
