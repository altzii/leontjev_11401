package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         036
 */

public class Task036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int k = a[0];

        for (int i = 1; i < n; i++) {
            if (a[i] > k) {
                System.out.println(a[i]);
                k = a[i];
            }
        }
    }
}
