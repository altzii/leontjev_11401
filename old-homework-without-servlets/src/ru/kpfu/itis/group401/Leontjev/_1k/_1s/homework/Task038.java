package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         037
 */

public class Task038 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double k = 0;
        boolean flag = false;
        double[][] a = new double[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextDouble();
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i][i] == 0)
                    for (int m = i + 1; (m < n) & (!flag); m++) {

                        if (a[m][i] != 0) {
                            for (int r = i; r < n; r++) {
                                a[i][r] = a[i][r] + a[m][r];
                                a[m][r] = a[i][r] - a[m][r];
                                a[i][i] = a[i][r] - a[m][r];
                            }
                            flag = true;
                        }
                    }
                if (a[j][i] != 0) {
                    k = a[j][i] / a[i][i];
                    for (int m = i; m < n; m++)
                        a[j][m] = a[j][m] - k * a[i][m];
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}