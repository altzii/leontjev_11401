package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         044
 */

public class Task044 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        String[] a = string.split(" ");
        boolean flag;

        for (int i = 0; i < a.length; i++) {
            flag = false;
            if (a[i].substring(0, 1).equals(String.valueOf(a[i].charAt(0)).toUpperCase())) {
                flag = true;
                for (int j = 1; j < a[i].length(); j++) {
                    if (!(a[i].substring(j, j + 1).equals(String.valueOf(a[i].charAt(j)).toLowerCase()))) {
                        flag = false;
                    }
                }
            }
            if (flag) {
                System.out.println(a[i]);
            }
        }
    }
}


