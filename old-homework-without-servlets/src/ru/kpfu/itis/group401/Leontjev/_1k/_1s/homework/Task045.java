package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         045
 */

public class Task045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();
        String[] team = new String[n];

        for (int i = 0; i < n; i++) {
            team[i] = scanner.nextLine();
        }

        int k = scanner.nextInt();
        scanner.nextLine();

        int[] d = new int[n];

        for (int i = 0; i < k; i++) {
            String res = scanner.nextLine();
            String[] s = new String[2];
            s = res.split(" ");

            String[] score = new String[1];
            score = s[2].split(":");

            for (int j = 0; j < n; j++) {
                if (s[0].equals(team[j])) {
                    d[j] = d[j] + Integer.parseInt(score[0]) - Integer.parseInt(score[1]);
                }
            }

            for (int j = 0; j < n; j++) {
                if (s[1].equals(team[j])) {
                    d[j] = d[j] + Integer.parseInt(score[1]) - Integer.parseInt(score[0]);
                }
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.println(team[i] + " " + d[i]);
        }
    }
}


