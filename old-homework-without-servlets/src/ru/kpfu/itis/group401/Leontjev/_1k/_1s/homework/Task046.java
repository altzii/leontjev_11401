package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         046
 */

public class Task046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String str1 = scanner.nextLine();
        String s1 = str1.toLowerCase();
        String str2 = scanner.nextLine();
        String s2 = str2.toLowerCase();

        boolean flag;
        flag = true;
        int f = 0;

        for (int i = 0; (i < Math.min(s1.length(), s2.length()) & flag); i++) {

            if (s1.charAt(i) < s2.charAt(i)) {
                flag = false;
                f = 1;
            }

            if (s1.charAt(i) > s2.charAt(i)) {
                flag = false;
                f = -1;
            }
        }
        if (f == 1) {
            System.out.println(str1);
        }

        if (f == -1) {
            System.out.println(str2);
        }

        if (f == 0) {
            if (s1.length() < s2.length()) {
                System.out.println(str1);
            } else {
                System.out.println(str2);
            }
        }
    }
}


