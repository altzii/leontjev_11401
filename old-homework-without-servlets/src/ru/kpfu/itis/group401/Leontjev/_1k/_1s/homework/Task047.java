package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         047
 */

public class Task047 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите x");
        int x = scanner.nextInt();

        multiplicationTable(x);
        System.out.println();
        System.out.println("infinitySum is " + infinitySum(x));
        System.out.println();

        System.out.println("Введите размерность векторов");
        int n = scanner.nextInt();
        System.out.println("Введите два " + n + "-мерных вектора");
        int a[] = new int[n];
        int b[] = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }

        System.out.println("Их скалярное произведение равно " + scalarProduct(n, a, b));
        System.out.println("Косинус угла между ними равен " + cosine(n, a, b));
        System.out.println();

        System.out.println("Введите размер квадратной матрицы n x n");
        int n1 = scanner.nextInt();
        System.out.println("Введите матрицу");
        double[][] c = new double[n1][n1];
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n1; j++) {
                c[i][j] = scanner.nextDouble();
            }
        }

        System.out.println("Матрица, приведенная к треугольному виду");
        toTriangularType(n1, c);
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n1; j++) {
                System.out.print(c[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static void multiplicationTable(int k) {
        System.out.println("Таблица умножения для " + k);
        System.out.println("2 * " + k + " = " + (k * 2));
        System.out.println("3 * " + k + " = " + (k * 3));
        System.out.println("4 * " + k + " = " + (k * 4));
        System.out.println("5 * " + k + " = " + (k * 5));
        System.out.println("6 * " + k + " = " + (k * 6));
        System.out.println("7 * " + k + " = " + (k * 7));
        System.out.println("8 * " + k + " = " + (k * 8));
        System.out.println("9 * " + k + " = " + (k * 9));
    }

    public static double infinitySum(double x) {
        final double EPS = 1e-9;
        double sum = 0;
        double k = 1;
        x = (x - 1) * (x - 1);
        double d = 1;

        for (int n = 1; Math.abs(k) > EPS; n++) {
            d = d * x * 9;
            k = 1.0 / (n * d);
            sum += k;
        }
        return sum;
    }

    public static double scalarProduct(int n, int a[], int b[]) {
        double s = 0;

        for (int i = 0; i < n; i++) {
            s += a[i] * b[i];
        }
        return s;
    }

    public static double cosine(int n, int a[], int b[]) {
        double s = 0;
        double s1 = 0;
        double s2 = 0;

        for (int i = 0; i < n; i++) {
            s += a[i] * b[i];
            s1 += a[i] * a[i];
            s2 += b[i] * b[i];
        }

        double cos;
        cos = s / (Math.sqrt(s1) * Math.sqrt(s2));
        return cos;
    }

    public static double[][] toTriangularType(int n, double[][] a) {
        double k = 0;
        boolean flag = false;

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i][i] == 0)
                    for (int m = i + 1; (m < n) & (!flag); m++) {

                        if (a[m][i] != 0) {
                            for (int r = i; r < n; r++) {
                                a[i][r] = a[i][r] + a[m][r];
                                a[m][r] = a[i][r] - a[m][r];
                                a[i][i] = a[i][r] - a[m][r];
                            }
                            flag = true;
                        }
                    }
                if (a[j][i] != 0) {
                    k = a[j][i] / a[i][i];
                    for (int m = i; m < n; m++)
                        a[j][m] = a[j][m] - k * a[i][m];
                }
            }
        }
        return a;

    }
}

