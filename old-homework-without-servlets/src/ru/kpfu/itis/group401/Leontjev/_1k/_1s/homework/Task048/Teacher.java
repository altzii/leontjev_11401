package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task048;

/**
 * @author Alexander Leontjev
 *         11-401
 *         048
 */

public class Teacher {
    private String fio;
    private String subject;

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getFio() {
        return this.fio;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return this.subject;
    }

    public Teacher(String fio, String subject) {
        this.fio = fio;
        this.subject = subject;
    }

    public void rating(Student student) {
        long mark = Math.round(Math.random() * 100) % 4 + 2; //(int)(Math.randon() * 4) + 2
        String stringMark = "";
        if (mark == 2) {
            stringMark = "неудовлетворительно";
        }
        if (mark == 3) {
            stringMark = "удовлетворительно";
        }
        if (mark == 4) {
            stringMark = "хорошо";
        }
        if (mark == 5) {
            stringMark = "отлично";
        }
        System.out.println("Преподаватель " + this.fio + " оценил студента с именем " + student.getFio() + " по предмету " + this.subject + " на оценку " + stringMark);
    }
}
