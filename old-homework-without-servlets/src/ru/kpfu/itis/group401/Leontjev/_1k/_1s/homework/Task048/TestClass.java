package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task048;

/**
 * @author Alexander Leontjev
 *         11-401
 *         048
 */

public class TestClass {
    public static void main(String[] args) {
        Student student = new Student("Леонтьев А.Ю.", 1996, 1337);
        Teacher teacher = new Teacher("Абрамский М.М.", "программирование");
        teacher.rating(student);
    }
}

