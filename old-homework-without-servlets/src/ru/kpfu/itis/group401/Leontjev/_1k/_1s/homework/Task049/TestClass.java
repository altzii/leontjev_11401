package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task049;

/**
 * @author Alexander Leontjev
 *         11-401
 *         049
 */

public class TestClass {
    public static void main(String[] args) {
        Vector2D v1 = new Vector2D(7, 9);
        Vector2D v2 = new Vector2D(3, 4);
        Vector2D v = new Vector2D();

        v = v1.add(v2);
        System.out.println("add = " + v);
        v.setX(0);
        v.setY(0);

        v1.add2(v2);
        System.out.println("add2 = " + v1);
        v1.setX(3);
        v1.setY(4);

        v = v1.sub(v2);
        System.out.println("sub = " + v);
        v.setX(0);
        v.setY(0);

        v1.sub2(v2);
        System.out.println("sub2 = " + v1);
        v1.setX(3);
        v1.setY(4);

        v = v1.mult(4);
        System.out.println("mult = " + v);
        v.setX(0);
        v.setY(0);

        v1.mult2(4);
        System.out.println("mult2 = " + v1);
        v1.setX(3);
        v1.setY(4);

        System.out.println("length = " + v1.length());
        System.out.println("scalarProduct = " + v1.scalarProduct(v2));
        System.out.println("cos = " + v1.cos(v2));
        System.out.println("v1 equals to v2 is " + v1.equals(v2));
        System.out.println("v1 equals to v1 is " + v1.equals(v1));
    }
}



