package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050;

/**
 * @author Alexander Leontjev
 *         11-401
 *         050
 */

public class RationalFraction implements Comparable<RationalFraction> {
    private int n;
    private int d;

    public int getN() {
        return this.n;
    }

    public int getD() {
        return this.d;
    }

    public void setN(int n) {
        this.n = n;
        this.reduce();
    }

    public void setD(int d) {
        this.d = d;
        this.reduce();
    }

    public RationalFraction() {
        this(0, 1);
    }

    public RationalFraction(int n, int d) {
        this.n = n;
        this.d = d;
        this.reduce();
    }

    public String toString() {
        return n + "/" + d;
    }

    public void reduce() {
        if (this.d < 0) {
            this.n = -this.n;
            this.d = -this.d;
        }

        if (this.n == 0) {
            this.d = 1;
        }


        int a = Math.abs(this.n);
        int b = this.d;
        int k = 1;

        while (a != 0 && b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
            k = a + b;
        }

        this.n /= k;
        this.d /= k;
    }


    public RationalFraction add(RationalFraction rf) {
        RationalFraction r = new RationalFraction(this.n * rf.getD() + rf.getN() * this.d, this.d * rf.getD());
        r.reduce();
        return r;
    }

    public void add2(RationalFraction rf) {
        this.n = this.n * rf.getD() + rf.getN() * this.d;
        this.d *= rf.getD();
        this.reduce();
    }

    public RationalFraction sub(RationalFraction rf) {
        RationalFraction r = new RationalFraction(this.n * rf.getD() - rf.getN() * this.d, this.d * rf.getD());
        r.reduce();
        return r;
    }

    public void sub2(RationalFraction rf) {
        this.n = this.n * rf.getD() - rf.getN() * this.d;
        this.d *= rf.getD();
        this.reduce();
    }

    public RationalFraction mult(RationalFraction rf) {
        RationalFraction r = new RationalFraction(this.n * rf.getN(), this.d * rf.getD());
        r.reduce();
        return r;
    }

    public void mult2(RationalFraction rf) {
        this.n *= rf.getN();
        this.d *= rf.getD();
        this.reduce();
    }

    public RationalFraction div(RationalFraction rf) {

        RationalFraction r = new RationalFraction(this.n * rf.getD(), this.d * rf.getN());
        r.reduce();
        return r;
    }

    public void div2(RationalFraction rf) {
        this.n *= rf.getD();
        this.d *= rf.getN();
        this.reduce();
    }

    public double value() {
        return (double) this.getN() / this.getD();
    }

    public boolean equals(RationalFraction rf) {
        return this.getN() == rf.getN() && this.getD() == rf.getD();
    }

    public int numberPart() {
        return this.n / this.d;
    }

    @Override
    public int compareTo(RationalFraction o) {
        final double EPS = 1e-9;
        double d1 = this.value();
        double d2 = o.value();
        if (Math.abs(d1 - d2) < EPS) {
            return 0;
        }
        if (d1 > d2) {
            return 1;
        } else {
            return -1;
        }
    }
}

