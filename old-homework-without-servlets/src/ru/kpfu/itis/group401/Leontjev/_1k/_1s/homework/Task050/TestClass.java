package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050;

/**
 * @author Alexander Leontjev
 *         11-401
 *         050
 */

public class TestClass {
    public static void main(String[] args) {
        RationalFraction rf1 = new RationalFraction(1, 4);
        RationalFraction rf2 = new RationalFraction(3, 4);
        RationalFraction rf = new RationalFraction();

        rf1.reduce();
        System.out.println("rf1.reduce " + rf1);
        rf1.setN(1);
        rf1.setD(4);

        rf2.reduce();
        System.out.println("rf2.reduce " + rf2);
        rf2.setN(3);
        rf2.setD(4);

        rf = rf1.add(rf2);
        System.out.println("add = " + rf);

        rf1.add2(rf2);
        System.out.println("add2 = " + rf1);
        rf1.setN(1);
        rf1.setD(4);

        rf = rf1.sub(rf2);
        System.out.println("sub = " + rf);

        rf1.sub2(rf2);
        System.out.println("sub2 = " + rf1);
        rf1.setN(1);
        rf1.setD(4);

        rf = rf1.mult(rf2);
        System.out.println("mult = " + rf);

        rf1.mult2(rf2);
        System.out.println("mult2 = " + rf1);
        rf1.setN(1);
        rf1.setD(4);


        rf = rf1.div(rf2);
        System.out.println("div = " + rf);


        rf1.div2(rf2);
        System.out.println("div2 = " + rf1);
        rf1.setN(1);
        rf1.setD(4);

        System.out.println("rf1.value = " + rf1.value());
        System.out.println("rf2.value = " + rf2.value());
        System.out.println("rf1 equals to rf2 is " + rf1.equals(rf2));
        System.out.println("rf1.numberPart is " + rf1.numberPart());
        System.out.println("rf2.numberPart is " + rf2.numberPart());
    }
}
