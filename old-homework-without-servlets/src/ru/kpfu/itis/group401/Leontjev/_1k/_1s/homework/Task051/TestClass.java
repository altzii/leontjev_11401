package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task051;

/**
 * @author Alexander Leontjev
 *         11-401
 *         051
 */

public class TestClass {
    public static void main(String[] args) {

        ComplexNumber cn1 = new ComplexNumber(4, -9);
        ComplexNumber cn2 = new ComplexNumber(3, 6);

        cn1.add2(cn2);
        System.out.println("add2 = " + cn1);
        cn1.setA(4);
        cn1.setB(-9);

        cn1.sub2(cn2);
        System.out.println("sub2 = " + cn1);
        cn1.setA(4);
        cn1.setB(-9);

        ComplexNumber cn = new ComplexNumber();
        cn = cn1.add(cn2);
        System.out.println("add = " + cn);

        cn = cn1.sub(cn2);
        System.out.println("sub = " + cn);

        cn1.mult2(cn2);
        System.out.println("mult2 = " + cn1);
        cn1.setA(4);
        cn1.setB(-9);

        cn = cn1.mult(cn2);
        System.out.println("mul = " + cn);

        cn1.multNumber2(2);
        System.out.println("multNumber2 = " + cn1);
        cn1.setA(4);
        cn1.setB(-9);

        cn = cn1.multNumber(22);
        System.out.println("multNumber = " + cn);

        cn1.div2(cn2);
        System.out.println("div2 = " + cn1);
        cn1.setA(4);
        cn1.setB(-9);

        cn = cn1.div(cn2);
        System.out.println("div = " + cn);

        System.out.println("cn1's length = " + cn1.length());
        System.out.println("cn1's sin = " + cn1.sin());
        System.out.println("cn1's cos = " + cn1.cos());
        System.out.println("cn1's arg in degrees = " + cn1.arg());
        System.out.println("cn1 equals to cn2 is " + cn1.equals(cn2));
        System.out.println("cn1 equals to cn1 is " + cn1.equals(cn1));

        cn1.pow(3);
        System.out.println("pow, cn1^3 = " + cn1);
        cn1.setA(4);
        cn1.setB(-9);

        cn = cn1.pow2(3);
        System.out.println("pow2, cn1^3 = " + cn);
    }
}

