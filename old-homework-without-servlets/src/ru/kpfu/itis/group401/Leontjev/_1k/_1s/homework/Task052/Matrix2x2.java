package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task052;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task049.Vector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         052
 */

public class Matrix2x2 {
    private double a[][] = new double[2][2];

    public Matrix2x2(double x) {
        this(x, x, x, x);
    }

    public String toString() {
        return a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1];
    }

    public Matrix2x2(double b[][]) {
        this(b[0][0], b[0][1], b[1][0], b[1][1]);
    }

    public Matrix2x2(double x1, double x2, double x3, double x4) {
        a[0][0] = x1;
        a[0][1] = x2;
        a[1][0] = x3;
        a[1][1] = x4;
    }

    public Matrix2x2() {
        this(0);
    }

    public Matrix2x2 add(Matrix2x2 m2) {
        Matrix2x2 m = new Matrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                m.a[i][j] = this.a[i][j] + m2.a[i][j];
            }
        }
        return m;
    }

    public void add2(Matrix2x2 m2) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] = this.a[i][j] + m2.a[i][j];
            }
        }
    }


    public Matrix2x2 sub(Matrix2x2 m2) {
        Matrix2x2 m = new Matrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                m.a[i][j] = this.a[i][j] - m2.a[i][j];
            }
        }
        return m;
    }

    public void sub2(Matrix2x2 m2) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] = this.a[i][j] - m2.a[i][j];
            }
        }
    }

    public Matrix2x2 multNumber(double k) {
        Matrix2x2 m = new Matrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                m.a[i][j] = this.a[i][j] * k;
            }
        }
        return m;
    }

    public void multNumber2(double k) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] = this.a[i][j] * k;
            }
        }
    }

    public Matrix2x2 mult(Matrix2x2 m2) {
        Matrix2x2 m = new Matrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    m.a[i][j] += this.a[i][k] * m2.a[k][j];
                }
            }
        }

        return m;

    }

    public void mult2(Matrix2x2 m2) {
        double a = 0;
        double b = 0;
        double c = 0;
        double d = 0;

        for (int i = 0; i < 2; i++) {
            a += this.a[0][i] * m2.a[i][0];
            b += this.a[0][i] * m2.a[i][1];
            c += this.a[1][i] * m2.a[i][0];
            d += this.a[1][i] * m2.a[i][1];
        }

        this.a[0][0] = a;
        this.a[0][1] = b;
        this.a[1][0] = c;
        this.a[1][1] = d;
    }

    public double det() {
        return a[0][0] * a[1][1] - a[0][1] * a[1][0];
    }

    public void transpon() {
        double temp;
        temp = this.a[1][0];
        this.a[1][0] = this.a[0][1];
        this.a[0][1] = temp;
    }

    public Matrix2x2 inverseMatrix() {
        Matrix2x2 m = new Matrix2x2();
        if (this.det() != 0) {
            double det = this.det();
            m.a[0][0] = this.a[1][1] / this.det();
            m.a[0][1] = -this.a[1][0] / this.det();
            m.a[1][0] = -this.a[0][1] / this.det();
            m.a[1][1] = this.a[0][0] / this.det();
            m.transpon();

        } else {
            System.out.println("Error! Det = 0");
        }

        return m;
    }

    public Vector2D multVector(Vector2D v2) {
        Vector2D v = new Vector2D();
        v.setX(this.a[0][0] * v2.getX() + this.a[0][1] * v2.getY());
        v.setY(this.a[1][0] * v2.getX() + this.a[1][1] * v2.getY());
        return v;
    }

    //(a[0][0] - a[0][1] * a[0][1] / a[1][1], a[0][1] - a[1][1] * a[0][1] / a[1][1])
    //(a[1][0] - a[0][0] * a[0][1] / a[0][0], a[1][1] - a[0][1] * a[1][0] / a[0)[0])

    public Matrix2x2 equivalentDiagonal() {
        Matrix2x2 m = new Matrix2x2();
//        m.a[0][0] = this.a[0][0] - this.a[1][0] * this.a[0][1] / this.a[1][1];
//        m.a[0][1] = this.a[0][1] - this.a[1][1] * this.a[0][1] / this.a[1][1];
//        m.a[1][0] = this.a[1][0] - this.a[0][0] * this.a[1][0] / this.a[0][0];
//        m.a[1][1] = this.a[1][1] - this.a[0][1] * this.a[1][0] / this.a[0][0];

        m.a[0][0] = this.a[0][0];
        m.a[0][1] = 0;
        m.a[1][0] = 0;
        m.a[1][1] = this.a[1][1] - this.a[0][1] * this.a[1][0] / this.a[0][0];
        return m;
    }


}
