package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task052;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task049.Vector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         051
 */

//public class TestClass {
//    public static void main(String[] args) {
//        Matrix2x2 m1 = new Matrix2x2(1, 2, 3, 4);
//        Matrix2x2 m2 = new Matrix2x2(2, 3, 1, 2);
//        Matrix2x2 m = new Matrix2x2();
//
//        m = m1.add(m2);
//        System.out.println("add = " + m);
//
//        m1.add2(m2);
//        System.out.println("add2 = " + m1);
//
//        Matrix2x2 m3 = new Matrix2x2(1, 2, 3, 4);
//        System.out.println(m3.equivalentDiagonal());


public class TestClass {
    public static void main(String[] args) {
        Matrix2x2 n1 = new Matrix2x2(2, 3, 4, 1);
        Matrix2x2 n2 = new Matrix2x2(1, 2, 3, 4);

        Vector2D v = new Vector2D(2, 3);

        System.out.println("add  = " + "\n" + n1.add(n2));
        n1.add2(n2);
        System.out.println("add2 = " + "\n" + n1);

        System.out.println("sub = " + "\n" + n1.sub(n2));
        n1.sub2(n2);
        System.out.println("sub = " + "\n" + n1);

        System.out.println("n1.multNumber(5) = " + "\n" + n1.multNumber(5));

        n1.multNumber2(5);
        System.out.println("n1.multNumber2(5)" + "\n" + n1);

        System.out.println("mult = " + "\n" + n1.mult(n2));

        n1.mult2(n2);
        System.out.println("mul2 = " + "\n" + n1);

        System.out.println("n1's det = " + n1.det());
        System.out.println("n2's det = " + n2.det());

        n1.transpon();
        System.out.println("transpon = " + "\n" + n1);

        System.out.println("inverseMatrix =  " + "\n" + n1.inverseMatrix());

        System.out.println("equivalentDiafonal = " + "\n" + n1.equivalentDiagonal());
        System.out.println("n1.multVector(v) " + n1.multVector(v));


    }
}


//    }
//}
