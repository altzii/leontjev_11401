package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task053;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;

/**
 * @author Alexander Leontjev
 *         11-401
 *         053
 */

public class RationalVector2D {
    private RationalFraction x;
    private RationalFraction y;

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        this.x = x;
        this.y = y;
    }

    public RationalVector2D() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalFraction getX() {
        return this.x;
    }

    public RationalFraction getY() {
        return this.y;
    }

    public void setX(RationalFraction x) {
        this.x = x;
    }

    public void setY(RationalFraction y) {
        this.y = y;
    }


    public String toString() {
        return "{" + this.x + "," + this.y + "}";
    }

    public RationalVector2D add(RationalVector2D rv2d) {
        RationalVector2D n = new RationalVector2D();
        n.x = this.x.add(rv2d.x);
        n.y = this.y.add(rv2d.y);
        return n;
    }

    public double length() {
        RationalFraction rv = new RationalFraction();
        rv = this.x.mult(this.x).add(this.y.mult(this.y));
        return Math.sqrt(rv.value());
    }

    public RationalFraction scalarProduct(RationalVector2D rv2d) {
        return this.x.mult(rv2d.x).add(this.y.mult(rv2d.y));
    }

    public boolean equals(RationalVector2D rv2d) {
        return this.x.equals(rv2d.x) && this.y.equals(rv2d.y);
    }
}
