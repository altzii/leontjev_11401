package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task053;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;

/**
 * @author Alexander Leontjev
 *         11-401
 *         053
 */

public class TestClass {

    public static void main(String[] args) {
        RationalFraction n1 = new RationalFraction(3, 4);
        RationalFraction n2 = new RationalFraction(6, 7);
        RationalFraction n3 = new RationalFraction(5, 4);
        RationalFraction n4 = new RationalFraction(4, 3);

        RationalVector2D vector1 = new RationalVector2D(n1, n2);
        RationalVector2D vector2 = new RationalVector2D(n3, n4);

        System.out.println("add = " + vector1.add(vector2));
        System.out.println("length =" + vector1.length());
        System.out.println("length2 = " + vector2.length());
        System.out.println("scalarProduct = " + vector1.scalarProduct(vector2));
        System.out.println("equals = " + vector1.equals(vector2));
    }
}
