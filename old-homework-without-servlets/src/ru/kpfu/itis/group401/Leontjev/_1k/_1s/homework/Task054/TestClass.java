package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task054;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task051.ComplexNumber;

/**
 * @author Alexander Leontjev
 *         11-401
 *         054
 */

public class TestClass {


    public static void main(String[] args) {
        ComplexNumber cn1 = new ComplexNumber(3, 4);
        ComplexNumber cn2 = new ComplexNumber(6, 7);
        ComplexNumber cn3 = new ComplexNumber(5, 4);
        ComplexNumber cn4 = new ComplexNumber(4, 3);

        ComplexVector2D cv2d1 = new ComplexVector2D(cn1, cn2);
        ComplexVector2D cv2d2 = new ComplexVector2D(cn3, cn4);

        System.out.println("add = " + cv2d1.add(cv2d2));
        System.out.println("scalarProduct = " + cv2d1.scalarProduct(cv2d2));
        System.out.println("rv2d1 equals to rv2d2 is " + cv2d1.equals(cv2d2));
    }
}

