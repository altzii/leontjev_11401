package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task055;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task053.RationalVector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         055
 */

public class RationalMatrix2x2 {
    private RationalFraction a[][] = new RationalFraction[2][2];

    public RationalMatrix2x2(RationalFraction x) {
       this(x,x,x,x);
    }

    public RationalMatrix2x2(RationalFraction x1, RationalFraction x2, RationalFraction x3, RationalFraction x4) {
        a[0][0] = x1;
        a[0][1] = x2;
        a[1][0] = x3;
        a[1][1] = x4;
    }

    public RationalMatrix2x2() {
       this(new RationalFraction());
    }

    public String toString() {
        return a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1];
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 m2) {
        RationalMatrix2x2 m = new RationalMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                m.a[i][j] = this.a[i][j].add(m2.a[i][j]);
            }
        }
        return m;
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 m2) {
        RationalMatrix2x2 m = new RationalMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    m.a[i][j] = m.a[i][j].add(this.a[i][k].mult(m2.a[k][j]));
                }
            }
        }
        return m;
    }

    public RationalFraction det() {
        return a[0][0].mult(a[1][1]).sub(a[0][1].mult(a[1][0]));
    }

    public RationalVector2D multVector(RationalVector2D rv2) {
        RationalVector2D rv2d = new RationalVector2D();
        rv2d.setX(this.a[0][0].mult(rv2.getX()).add(this.a[0][1].mult(rv2.getY())));
        rv2d.setY(this.a[1][0].mult(rv2.getX()).add(this.a[1][1].mult(rv2.getY())));
        return rv2d;
    }
}
