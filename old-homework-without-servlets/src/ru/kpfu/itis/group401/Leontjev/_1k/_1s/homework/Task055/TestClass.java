package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task055;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task053.RationalVector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         055
 */

public class TestClass {
    public static void main(String[] args) {
        RationalFraction rf1 = new RationalFraction(3, 4);
        RationalFraction rf2 = new RationalFraction(6, 7);
        RationalFraction rf3 = new RationalFraction(5, 4);
        RationalFraction rf4 = new RationalFraction(4, 3);

        RationalVector2D rv2d1 = new RationalVector2D(rf1, rf3);

        RationalMatrix2x2 rm1 = new RationalMatrix2x2(rf1, rf2, rf3, rf4);
        RationalMatrix2x2 rm2 = new RationalMatrix2x2(rf4, rf3, rf2, rf1);

        System.out.println("add = " + "\n" + rm1.add(rm2));
        System.out.println("mult = " + "\n" + rm1.mult(rm2));
        System.out.println("rm1's det = " + rm1.det());
        System.out.println("rm2's det = " + rm2.det());
        System.out.println("multVector = " + rm1.multVector(rv2d1));
    }
}
