package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task056;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task051.ComplexNumber;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task054.ComplexVector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         056
 */

public class ComplexMatrix2x2 {
    private ComplexNumber a[][] = new ComplexNumber[2][2];

    public ComplexMatrix2x2(ComplexNumber x) {
      this(x,x,x,x);
    }

    public ComplexMatrix2x2(ComplexNumber x1, ComplexNumber x2, ComplexNumber x3, ComplexNumber x4) {
        a[0][0] = x1;
        a[0][1] = x2;
        a[1][0] = x3;
        a[1][1] = x4;
    }

    public ComplexMatrix2x2() {
        this(new ComplexNumber());
    }

    public String toString() {
        return a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1];
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 m2) {
        ComplexMatrix2x2 m = new ComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                m.a[i][j] = this.a[i][j].add(m2.a[i][j]);
            }
        }
        return m;
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 m2) {
        ComplexMatrix2x2 m = new ComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    m.a[i][j] = m.a[i][j].add(this.a[i][k].mult(m2.a[k][j]));
                }
            }
        }
        return m;
    }

    public ComplexNumber det() {
        return a[0][0].mult(a[1][1]).sub(a[0][1].mult(a[1][0]));
    }

    public ComplexVector2D multVector(ComplexVector2D cv2) {
        ComplexVector2D cv2d = new ComplexVector2D();
        cv2d.setX(this.a[0][0].mult(cv2.getX()).add(this.a[0][1].mult(cv2.getY())));
        cv2d.setY(this.a[1][0].mult(cv2.getX()).add(this.a[1][1].mult(cv2.getY())));
        return cv2d;
    }
}



