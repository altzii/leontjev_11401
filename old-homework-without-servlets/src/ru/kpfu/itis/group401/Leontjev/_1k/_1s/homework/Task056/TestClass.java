package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task056;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task051.ComplexNumber;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task054.ComplexVector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         056
 */

public class TestClass {
    public static void main(String[] args) {
        ComplexNumber n1 = new ComplexNumber(3, 4);
        ComplexNumber n2 = new ComplexNumber(6, 7);
        ComplexNumber n3 = new ComplexNumber(5, 4);
        ComplexNumber n4 = new ComplexNumber(4, 3);

        ComplexVector2D cv1 = new ComplexVector2D(n1, n3);
        ComplexVector2D cv2 = new ComplexVector2D(n2, n4);
        ;
        ComplexMatrix2x2 cm1 = new ComplexMatrix2x2(n1, n2, n3, n4);
        ComplexMatrix2x2 cm2 = new ComplexMatrix2x2(n1, n3, n2, n4);

        System.out.println("add = " + "\n" + cm1.add(cm2) + "\n");
        System.out.println("mult =" + "\n" + cm1.mult(cm2) + "\n");
        System.out.println("cm1's det = " + cm1.det());
        System.out.println("cm2's det = " + cm2.det());
        System.out.println("cm1.multVector(cv1) = " + cm1.multVector(cv1));
        System.out.println("cm2.multVector(cv2) = " + cm2.multVector(cv2));
    }
}


