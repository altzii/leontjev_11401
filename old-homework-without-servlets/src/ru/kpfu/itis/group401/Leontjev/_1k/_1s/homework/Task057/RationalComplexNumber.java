package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task057;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;

/**
 * @author Alexander Leontjev
 *         11-401
 *         057
 */

public class RationalComplexNumber {
    private RationalFraction a;
    private RationalFraction b;

    public RationalComplexNumber(RationalFraction a, RationalFraction b) {
        this.a = a;
        this.b = b;
    }

    public RationalComplexNumber() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalFraction getA() {
        return this.a;
    }

    public RationalFraction getB() {
        return this.b;
    }

    public String toString() {
        if (b.getN() > 0) {
            return a + " + " + b + "i";
        } else {
            RationalFraction temp = new RationalFraction(-b.getN(), b.getD());
            return a + " - " + temp + "i";
        }
    }

    public void setCn1(RationalFraction a) {
        this.a = a;
    }

    public void setCn2(RationalFraction a) {
        this.a = a;
    }


    public RationalComplexNumber add(RationalComplexNumber rcn2) {
        RationalComplexNumber rcn = new RationalComplexNumber(this.getA().add(rcn2.getA()), this.getB().add(rcn2.getB()));
        return rcn;
    }

    public RationalComplexNumber sub(RationalComplexNumber rcn2) {
        RationalComplexNumber rcn = new RationalComplexNumber(this.getA().sub(rcn2.getA()), this.getB().sub(rcn2.getB()));
        return rcn;
    }

    public RationalComplexNumber mult(RationalComplexNumber rcn2) {
        RationalComplexNumber cn = new RationalComplexNumber(this.a.mult(rcn2.getA()).sub(this.b.mult(rcn2.getB())), this.a.mult(rcn2.getB()).add(this.b.mult(rcn2.getA())));
        return cn;
    }


}
