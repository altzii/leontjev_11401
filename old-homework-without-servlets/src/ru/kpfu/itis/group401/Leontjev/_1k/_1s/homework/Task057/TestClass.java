package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task057;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;

/**
 * @author Alexander Leontjev
 *         11-401
 *         057
 */

public class TestClass {
    public static void main(String[] args) {
        RationalFraction n1 = new RationalFraction(1, 2);
        RationalFraction n2 = new RationalFraction(1, 2);
        RationalFraction n3 = new RationalFraction(5, 4);
        RationalFraction n4 = new RationalFraction(25, 7);

        RationalComplexNumber s1 = new RationalComplexNumber(n1, n2);
        RationalComplexNumber s2 = new RationalComplexNumber(n3, n4);

        System.out.println("add = " + s1.add(s2));
        System.out.println("sub = " + s1.sub(s2));
        System.out.println("mult = " + s1.mult(s2));
    }
}

