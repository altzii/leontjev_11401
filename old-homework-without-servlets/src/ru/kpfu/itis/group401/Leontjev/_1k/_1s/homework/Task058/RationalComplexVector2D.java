package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task058;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task057.RationalComplexNumber;

/**
 * @author Alexander Leontjev
 *         11-401
 *         058
 */

public class RationalComplexVector2D {
    private RationalComplexNumber x;
    private RationalComplexNumber y;

    public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public RationalComplexVector2D() {
        this(new RationalComplexNumber(), new RationalComplexNumber());
    }

    public RationalComplexNumber getX() {
        return this.x;
    }

    public RationalComplexNumber getY() {
        return this.y;
    }

    public void setX(RationalComplexNumber x) {
        this.x = x;
    }

    public void setY(RationalComplexNumber y) {
        this.y = y;
    }


    public String toString() {
        return "{" + this.x + ", " + this.y + "}";
    }

    public RationalComplexVector2D add(RationalComplexVector2D rcv2d) {
        RationalComplexVector2D n = new RationalComplexVector2D();
        n.x = this.x.add(rcv2d.x);
        n.y = this.y.add(rcv2d.y);
        return n;
    }

    public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv2d) {
        return this.x.mult(rcv2d.x).add(this.y.mult(rcv2d.y));
    }
}
