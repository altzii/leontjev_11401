package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task059;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task057.RationalComplexNumber;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task058.RationalComplexVector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         059
 */

public class RationalComplexMatrix2x2 {
    private RationalComplexNumber a[][] = new RationalComplexNumber[2][2];

    public RationalComplexMatrix2x2(RationalComplexNumber x) {
        this(x, x, x, x);
    }

    public RationalComplexMatrix2x2(RationalComplexNumber x1, RationalComplexNumber x2, RationalComplexNumber x3, RationalComplexNumber x4) {
        a[0][0] = x1;
        a[0][1] = x2;
        a[1][0] = x3;
        a[1][1] = x4;
    }

    public RationalComplexMatrix2x2() {
        this(new RationalComplexNumber());
    }


    public String toString() {
        return a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1];
    }

    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 m2) {
        RationalComplexMatrix2x2 m = new RationalComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                m.a[i][j] = this.a[i][j].add(m2.a[i][j]);
            }
        }
        return m;
    }

    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 m2) {
        RationalComplexMatrix2x2 m = new RationalComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    m.a[i][j] = m.a[i][j].add(this.a[i][k].mult(m2.a[k][j]));
                }
            }
        }
        return m;
    }

    public RationalComplexNumber det() {
        return a[0][0].mult(a[1][1]).sub(a[0][1].mult(a[1][0]));
    }

    public RationalComplexVector2D multVector(RationalComplexVector2D v2) {
        RationalComplexVector2D rcv2d = new RationalComplexVector2D();
        rcv2d.setX(this.a[0][0].mult(v2.getX()).add(this.a[0][1].mult(v2.getY())));
        rcv2d.setY(this.a[1][0].mult(v2.getX()).add(this.a[1][1].mult(v2.getY())));
        return rcv2d;
    }
}
