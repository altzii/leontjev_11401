package ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task059;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task057.RationalComplexNumber;
import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task058.RationalComplexVector2D;

/**
 * @author Alexander Leontjev
 *         11-401
 *         059
 */

public class TestClass {
    public static void main(String[] args) {
        RationalFraction n1 = new RationalFraction(3,4);
        RationalFraction n2 = new RationalFraction(6,7);
        RationalFraction n3 = new RationalFraction(5,4);
        RationalFraction n4 = new RationalFraction(4,3);

        RationalComplexNumber s1 = new RationalComplexNumber(n1,n2);
        RationalComplexNumber s2 = new RationalComplexNumber(n3,n4);
        RationalComplexNumber s3 = new RationalComplexNumber(n1,n4);
        RationalComplexNumber s4 = new RationalComplexNumber(n2,n3);

        RationalComplexVector2D t1 = new RationalComplexVector2D(s1,s2);
        RationalComplexVector2D t2 = new RationalComplexVector2D(s3,s4);

        RationalComplexMatrix2x2 matrix1 = new RationalComplexMatrix2x2(s1,s2,s3,s4);
        RationalComplexMatrix2x2 matrix2 = new RationalComplexMatrix2x2(s1,s3,s2,s4);

        System.out.println(matrix1.add(matrix2) + "\n");
        System.out.println("mult = " + matrix1.mult(matrix2) + "\n");
        System.out.println("matrix1's det = " + matrix1.det());
        System.out.println("matrix2's det = " + matrix2.det() + "\n");

        System.out.println("matrix1.multVector(t1) = " + matrix1.multVector(t1));
        System.out.println("matrix2.multVector(t2) = " + matrix2.multVector(t2));
    }
}
