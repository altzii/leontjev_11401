package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         990
 */

public class Task990 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] a = new int[n];
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int i = 0;
        while ((i < n) & (flag)) {
            if (((i % 2 == 0 || i % 5 == 0) && a[i] >= 0)) {
                flag = false;
            }
            i++;
        }
        System.out.println(flag);
    }
}
