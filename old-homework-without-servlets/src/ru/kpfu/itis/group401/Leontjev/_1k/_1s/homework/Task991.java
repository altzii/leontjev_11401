package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         991
 */

public class Task991 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] a = new int[n][m];
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; ((i < n) & (flag)); i += 3) {
            for (int j = 0; ((j < m) & (flag)); j += 7) {
                flag = a[i][j] == 0;
            }

        }
        System.out.println(flag);
    }
}

