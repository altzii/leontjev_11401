package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         992
 */

public class Task992 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        int[][][] a = new int[n][n1][n2];
        boolean flag = true;
        int k = 0;
        int k1;

        for (int i = 0; i < n; i++) {
            for (int i1 = 0; i1 < n1; i1++) {
                for (int i2 = 0; i2 < n2; i2++) {
                    a[i][i1][i2] = scanner.nextInt();
                }
            }
        }

        for (int i = 1; ((i < n) & (flag)); i++) {
            for (int i1 = 1; ((i1 < n1) & (flag)); i1++) {
                int j = i;
                int j1 = i1;
                while (j != 0 && j1 != 0) {
                    if (j > j1) {
                        j %= j1;
                    } else {
                        j1 %= j;
                    }
                    k = j + j1;
                }

                for (int i2 = 1; ((i2 < n2) & (flag)); i2++) {
                    if (k == 1) {
                        flag = a[i][i1][i2] == 0;
                    } else {

                        int j2 = i2;
                        while (k != 0 && j2 != 0) {
                            if (k > j2) {
                                k %= j2;
                            } else {
                                j2 %= k;
                            }
                            k1 = k + j2;
                            if (k1 == 1) {
                                flag = a[i][i1][i2] == 0;
                            }
                        }
                    }
                }
            }
        }
        System.out.println(flag);
    }
}