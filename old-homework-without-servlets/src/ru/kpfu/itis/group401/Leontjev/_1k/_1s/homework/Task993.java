package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         993
 */

public class Task993 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        int n3 = scanner.nextInt();
        int[][][][] a = new int[n][n1][n2][n3];
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            for (int i1 = 0; i1 < n1; i1++) {
                for (int i2 = 0; i2 < n2; i2++) {
                    for (int i3 = 0; i3 < n3; i3++) {
                        {
                            a[i][i1][i2][i3] = scanner.nextInt();
                        }
                    }
                }
            }
        }

        for (int i = 0; ((i < n) & (flag)); i++) {
            for (int i1 = 0; ((i1 < n1) & (flag)); i1++) {
                for (int i2 = 0; ((i2 < n2) & (flag)); i2++) {
                    for (int i3 = 0; ((i3 < n3) & (flag)); i3++) {
                        if ((i == i1) || (i == i2) || (i == i3) || (i1 == i2) || (i1 == i3) || (i2 == i3)) {
                            flag = a[i][i1][i2][i3] >= 0;
                        }
                    }
                }
            }
        }

        System.out.println(flag);
    }
}