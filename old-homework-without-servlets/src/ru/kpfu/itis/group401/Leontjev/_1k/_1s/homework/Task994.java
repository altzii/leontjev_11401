package ru.kpfu.itis.group401.Leontjev._1k._1s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         995
 */

public class Task994 {
    public static void main(String[] args) {

        int[][][][][] a = new int[2][2][2][2][2];

        for (int i = 0; i < 2; i++) {
            for (int i1 = 0; i1 < 2; i1++) {
                for (int i2 = 0; i2 < 2; i2++) {
                    for (int i3 = 0; i3 < 2; i3++) {
                        for (int i4 = 0; i4 < 2; i4++) {
                            a[i][i1][i2][i3][i4] = 0;
                        }
                    }
                }
            }
        }
        a[1][1][1][1][1] = 1;
    }
}

