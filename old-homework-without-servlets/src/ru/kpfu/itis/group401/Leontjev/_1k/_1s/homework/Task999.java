package ru.kpfu.itis.group401.Leontjev._1k._1s.homework; /**
 * @author Alexander Leontjev
 * 11-401
 * 999
 */

import java.util.Scanner;

public class Task999 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        double y = scanner.nextDouble();
        double z = scanner.nextDouble();

        double sum = 0;
        double s1 = 0;
        double s2 = 0;
        double s3 = 0;

        double p1 = 0;
        double p2 = 0;
        double p3 = 0;
        double p4 = 0;

        p1 = 100 * x;
        p1 += y;
        p1 += 607;

        p2 = 100 * y;
        p2 += x;
        p2 += 687;

        s1 = p1 / p2;

        p1 = 54 * z;
        p1 += x;

        p2 = y - z;
        p2 *= z;
        p2 -= 365;
        p2 *= 258;
        p2 *= z;

        p3 = p1 / p2;

        p1 = 100 * x;
        p1 += y;
        p1 += 607;

        p2 = z - x;
        p2 -= y;
        p2 *= x;

        p4 = y * z;
        p2 += p4;

        p1 = p1 / p2;

        s2 = p3 * p1;


        p1 = 73 * z;
        p1 += x;
        p1 *= 5021;

        p2 = x - 2;
        p2 *= y;
        p2 -= 365;
        p2 *= 213;

        s3 = p1 / p2;


        sum = s1 - s2 - s3;

        System.out.println(sum);

    }
}



