package ru.kpfu.itis.group401.Leontjev._1k._1s.tests.kr2;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         kpfu.itis.group401.Leontjev.classwork.lesson20141125.Task001
 */

public class Task001 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int[] a = new int[m];
        int[] b = new int[n];

        for (int i = 0; i < m; i++)
            a[i] = scanner.nextInt();
        for (int i = 0; i < n; i++)
            b[i] = scanner.nextInt();

        int m1 = m;
        int n1 = n;
        int k = 0;

        while (m1 != 0 && n1 != 0) {
            if (m1 > n1) {
                m1 %= n1;
            } else {
                n1 %= m1;
            }
            k = n1 + m1;
        }
        if (k == 1) {
            System.out.println("NO!");
        } else {
            System.out.println("YES!");
            System.out.println(m / k + "x" + k + " " + k + "x" + n / k);

            int p;
            p = (m / k) * (n / k);
            int[] c = new int[p];

            /** for (int i = 1; i < p; i++) {
             for (int j = 1; j < k; j++) {
             c[i] = a[j];

             }
             }
             */

        }
    }
}