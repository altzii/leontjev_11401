package ru.kpfu.itis.group401.Leontjev._1k._1s.tests.kr2;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         ru.kpfu.itis.group401.Leontjev._1k._1s.classwork.lesson20141125.Task002
 */

public class Task002 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[][] a = new int[n][n];
        int k;
        k = n * n - 1;
        int j = 0;


        while (k >= 0) {

            for (int i = 0; i < n; i++) {
                a[i][j] = k;
                k--;
            }
            j++;

            for (int i = n - 1; (i >= 0) & (k >= 0); i--) {
                a[i][j] = k;
                k--;
            }
            j++;
        }

        for (int i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
