package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BackTracking;

/**
 * Created by Alexander on 14.04.15.
 */

public class Task001 {
    public static void main(String[] args) {
        backTracking("", 10, 0);
    }

    static void backTracking(String s, int n, int count) {
        if (s.length() == n) {
            System.out.println(s);
        } else {
            int i = 0;

            while (i <= 1) {
                int count1 = count;
                if (i == 1) {
                    count1++;
                }

                if (count1 < 3) {
                    backTracking(s + Integer.toString(i), n, count1);
                }

                i++;
            }
        }
    }
}
