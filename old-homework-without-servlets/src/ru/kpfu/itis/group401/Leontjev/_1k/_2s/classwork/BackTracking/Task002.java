package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BackTracking;

/**
 * Created by Alexander on 14.04.15.
 */

public class Task002 {
    public static void main(String[] args) {
        backTracking(8, 7, 0, 0);
    }

    static void backTracking(int s, int n, int length, int sum) {
        if (length == n) {
            System.out.println(s);
        } else {
            int i = 0;

            while (i <= 9) {
                if (sum < 13) {
                    backTracking(s * 10 + i,n , length+1, sum += i);
                }
                i++;
            }
        }
    }
}
