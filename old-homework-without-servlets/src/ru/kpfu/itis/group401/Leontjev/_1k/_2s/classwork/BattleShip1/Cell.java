package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;


public class Cell {
    protected boolean deck;
    private Ship ship;
    private boolean life;
    private boolean shot;

    public Cell() {
        this.shot = false;
        this.deck = false;
        this.life=true;
    }

    public boolean getDeck() {
        return this.deck;
    }

    public void setDeck(boolean c) {
        this.deck = c;
    }

    public boolean getLife() {
        return this.life;
    }

    public void setLife() {
        this.life = false;
    }

    public Ship getShip() {
        return this.ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }


    public void setShot(boolean c) {
        this.shot = c;
    }
    public boolean getShot() {
        return this.shot;
    }
}
