package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Scanner;


public class Connection {
    public DatagramSocket sendDS = null;
    public DatagramSocket receiveDS = null;
    public static int begin;

    public void connect() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter '1' if you make first move or '2' if your opponent do it");
        int r = sc.nextInt();
        this.begin = r;
        System.out.println("Write your enemy ip address");
        String ip = sc.next();
        if (r == 1) {
            try {
                sendDS = new DatagramSocket(1137);
                receiveDS = new DatagramSocket(1637);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            try {
                sendDS.connect(new InetSocketAddress(ip, 1537));
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        if (r == 2) {
            try {
                sendDS = new DatagramSocket(1037);
                receiveDS = new DatagramSocket(1537);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            try {
                sendDS.connect(new InetSocketAddress(ip, 1637));
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        if (sendDS.isConnected()) {
            System.out.println("Successfully connected to:  " + ip);
        }
    }

    public void sendCell(String s) {
        DatagramPacket packet = new DatagramPacket(s.getBytes(), s.getBytes().length);
        try {
            sendDS.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendResult(int[] m) {
        String s = (m[0]+""+ m[1]+""+ m[2] + "");
        DatagramPacket packet = new DatagramPacket(s.getBytes(), s.getBytes().length);
        try {
            sendDS.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCell() {
        DatagramPacket packet2 = new DatagramPacket(new byte[4], 4);
        try {
            receiveDS.receive(packet2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] data = packet2.getData();
        String buf = new String(data).trim();
        return buf;
    }

    public int[] getResult() {
        DatagramPacket packet2 = new DatagramPacket(new byte[3], 3);
        try {
            receiveDS.receive(packet2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] data = packet2.getData();
        String res = new String(data).trim();
        int[] m = new int[3];
        m[0] = Character.getNumericValue(res.charAt(0));
        m[1] = Character.getNumericValue(res.charAt(1));
        m[2] = Character.getNumericValue(res.charAt(2));
        return m;
    }
}
