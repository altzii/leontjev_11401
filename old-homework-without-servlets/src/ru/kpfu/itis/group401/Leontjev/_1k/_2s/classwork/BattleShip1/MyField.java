package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;


public class MyField {
    private Cell[][] f;
    private Ship[] ships;

    public MyField() {
        this.f = new Cell[10][10];
        this.ships = new Ship[10];
    }

    public boolean life() {
        boolean c = false;
        for (int i = 0; i < ships.length && !c; i++) {
            c = ships[i].life();
        }
        return c;
    }

    private boolean readFromFile() throws IOException {
        String s = "field.txt";
        boolean c = false;
        File file = new File(s);
        file.createNewFile();
        FileInputStream file2 = null;
        try {
            file2 = new FileInputStream(file);
            Scanner sc = new Scanner(file2);
            c = true;
            for (int i = 0; c && i < 10; i++) {
                String s1 = "";
                if (sc.hasNextLine()) {
                    s1 = sc.nextLine();
                } else {
                    c = false;
                }
                if (s1.length() != 10) {
                    c = false;
                }
                for (int j = 0; c && j < 10; j++) {
                    char ch = s1.charAt(j);
                    if (ch == (char) 35) {
                        f[i][j] = new Cell();
                        f[i][j].setDeck(false);
                    } else {
                        if (ch == (char) 42) {
                            f[i][j] = new Cell();
                            f[i][j].setDeck(true);
                        } else {
                            c = false;
                        }
                    }
                }
            }

        } catch (IOException e) {
            System.out.println("Ошибка записи");
        } finally {
            if (file2 != null) {
                file2.close();
            }
        }
        return c;
    }

    public void sout(int i) {
        for (int j = 0; j < 10; j++) {
            if (!f[i][j].getDeck()) {
                if (!f[i][j].getShot()) {
                    System.out.print("# ");
                }
                else{
                    System.out.print("o ");
                }
            } else {
                if (f[i][j].getLife()) {
                    System.out.print("* ");
                } else {
                    System.out.print("X ");
                }
            }
        }
    }

    public void shipsSetting() throws IOException {
        if (readFromFile()) {
            int ships = 0;
            int[] s = new int[4];
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    if (f[i][j].getDeck() && f[i][j].getShip() == null) {
                        int i1 = i;
                        int j1 = j;
                        this.ships[ships] = new Ship();
                        this.ships[ships].addDeck(f[i][j]);
                        f[i][j].setShip(this.ships[ships]);
                        if (j1 + 1 < 10 && f[i][j1 + 1].getDeck()) {
                            while (j1 + 1 < 10 && f[i][j1 + 1].getDeck()) {
                                j1++;
                                if (!checkHorizontal(i, j1)) {
                                    throw new FieldException();
                                }
                                this.ships[ships].addDeck(f[i][j1]);
                                f[i][j1].setShip(this.ships[ships]);
                            }
                            if (j1 - j < 4) {
                                s[j1 - j]++;
                                if (s[1] > 3 && s[2] > 2 || s[3] > 1) {
                                    throw new FieldException();
                                }
                            } else {
                                throw new FieldException();
                            }
                        } else {
                            if (i1 + 1 < 10 && f[i1 + 1][j].getDeck()) {
                                while (i1 + 1 < 10 && f[i1 + 1][j].getDeck()) {
                                    i1++;
                                    if (!checkVertical(i1, j)) {
                                        throw new FieldException();
                                    }
                                    this.ships[ships].addDeck(f[i1][j]);
                                    f[i1][j].setShip(this.ships[ships]);
                                }
                                if (i1 - i < 4) {
                                    s[i1 - i]++;
                                    if (s[1] > 3 && s[2] > 2 || s[3] > 1) {
                                        throw new FieldException();
                                    }
                                } else {
                                    throw new FieldException();
                                }
                            } else {
                                s[0]++;
                                if (s[0] > 4) {
                                    throw new FieldException();
                                }
                            }
                        }
                        ships++;
                    }
                }
            }
            if (ships < 10) {

            }
        } else {
            throw new FieldException();
        }
    }

    public int[] shot(String s) {
        char ch = s.charAt(0);
        int j = -1;
        int i = -1;
        if ((int) ch < 91) {
            j = (int) ch - 65;
        } else {
            j = (int) ch - 97;
        }
        ch = s.charAt(1);
        if (s.length() == 2) {
            i = (int) ch - 49;
        } else {
            i = 9;
        }
        int ar[]= new int[3];
        ar[1]=i;
        ar[2]=j;
        if (f[i][j].getDeck()) {
            if (!f[i][j].getLife()) {
                System.out.println("Opponent hit the shattered deck");
                ar[0]=0;
                return ar;
            } else {
                f[i][j].setLife();
                if (!life()) {
                    System.out.println("You are loser. Opponent sank your last ship and win");
                    System.out.println("You lose");
                    ar[0]=3;
                    return ar;
                } else {
                    if (!f[i][j].getShip().life()) {
                        System.out.println("Opponent sank your ship");
                        ar[0]=2;
                        return ar;
                    } else {
                        System.out.println("Opponent hit your ship");
                        ar[0]=1;
                        return ar;
                    }
                }
            }
        } else {
            f[i][j].setShot(true);
            System.out.println("Opponent don't hit your ship");
            ar[0]=0;
            return ar;
        }
    }

    private boolean checkHorizontal(int i, int j) {
        boolean c = true;
        if (i - 1 >= 0 && f[i - 1][j].getDeck()) {
            c = false;
            return c;
        }
        if (i - 1 >= 0 && j - 1 >= 0 && f[i - 1][j - 1].getDeck()) {
            c = false;
            return c;
        }
        if (i - 1 >= 0 && j + 1 < 10 && f[i - 1][j + 1].getDeck()) {
            c = false;
            return c;
        }
        if (i + 1 < 10 && f[i + 1][j].getDeck()) {
            c = false;
            return c;
        }
        if (i + 1 < 10 && j - 1 >= 0 && f[i + 1][j - 1].getDeck()) {
            c = false;
            return c;
        }
        if (i + 1 < 10 && j + 1 < 10 && f[i + 1][j + 1].getDeck()) {
            c = false;
            return c;
        }
        return c;
    }

    private boolean checkVertical(int i, int j) {
        boolean c = true;
        if (j - 1 >= 0 && f[i][j - 1].getDeck()) {
            c = false;
            return c;
        }
        if (i - 1 >= 0 && j - 1 >= 0 && f[i - 1][j - 1].getDeck()) {
            c = false;
            return c;
        }
        if (j - 1 >= 0 && i + 1 < 10 && f[i + 1][j - 1].getDeck()) {
            c = false;
            return c;
        }
        if (j + 1 < 10 && f[i][j + 1].getDeck()) {
            c = false;
            return c;
        }
        if (j + 1 < 10 && i - 1 >= 0 && f[i - 1][j + 1].getDeck()) {
            c = false;
            return c;
        }
        if (i + 1 < 10 && j + 1 < 10 && f[i + 1][j + 1].getDeck()) {
            c = false;
            return c;
        }
        return c;
    }
}
