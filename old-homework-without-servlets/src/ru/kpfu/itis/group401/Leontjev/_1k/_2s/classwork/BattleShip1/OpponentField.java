package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;

public class OpponentField {
    private Cell[][] f;

    public OpponentField() {
        f = new Cell[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                f[i][j]= new Cell();
                f[i][j].setDeck(false);
            }
        }
    }

    public void sout(int i) {
        for (int j = 0; j < 10; j++) {
            if (f[i][j].getDeck()) {
                System.out.print("X ");
            } else {
                if (!f[i][j].getShot()) {
                    System.out.print("# ");
                }
                else{
                    System.out.print("o ");
                }
            }
        }
    }

    public boolean shot( String s) {
        if (s.length() == 2 || s.length() == 3) {
            char ch = s.charAt(0);
            int j = -1;
            int i = -1;
            if ((int) ch < 91) {
                j = (int) ch - 65;
            } else {
                j = (int) ch - 97;
            }
            if (j < 0 || j > 9) {
                System.out.println("Illegal cell for attacking!! Enter new cell.");
                return false;
            }
            ch = s.charAt(1);
            if (s.length() == 2) {
                i = (int) ch - 49;
                if (i < 0 || i > 8) {
                    System.out.println("Illegal cell for attacking!! Enter new cell.");
                    return false;
                }
            } else {
                if ((int) ch == 49) {
                    ch = s.charAt(2);
                    if ((int) ch != 48) {
                        System.out.println("Illegal cell for attacking!! Enter new cell.");
                        return false;
                    }
                } else {
                    System.out.println("Illegal cell for attacking!! Enter new cell.");
                    return false;
                }
            }
            return true;
        } else {
            System.out.println("Illegal cell for attacking!! Enter new cell.");
            return false;
        }
    }

    public void shot1(int[] m) {
        if (m[0]==1){
            System.out.println("You hit the ship");
            f[m[1]][m[2]].setLife();
            f[m[1]][m[2]].setDeck(true);
        }
        if (m[0]==0 ){
            System.out.println("You are loser!! You don't hit the ship");
            f[m[1]][m[2]].setShot(true);
        }
        if (m[0] == 2 ) {
            System.out.println("Congratulations!! You sunk the ship.");
            f[m[1]][m[2]].setLife();
            f[m[1]][m[2]].setDeck(true);
        }
        if (m[0] == 3) {
            System.out.println("Congratulations!! You sunk the last ship and you win");
            f[m[1]][m[2]].setLife();
            f[m[1]][m[2]].setDeck(true);
        }
    }
}
