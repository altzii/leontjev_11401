package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;

import java.io.IOException;
import java.util.Scanner;


public class Play {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        MyField m = new MyField();
        OpponentField o = new OpponentField();
        SoutFields s1 = new SoutFields(m,o);
        m.shipsSetting();
        Connection c = new Connection();
        c.connect();
        boolean v;
        String s;
        int[] mas;
        int k=-1;
        if (c.begin == 1) {
            s1.sout();
            while (k!=0) {
                System.out.println("Enter cell for attacking");
                s = sc.next();
                v = o.shot(s);
                while (!v) {
                    s = sc.next();
                    v = o.shot(s);
                }
                c.sendCell(s);
                mas = c.getResult();
                k=mas[0];
                o.shot1(mas);
                s1.sout();
            }
        }
        if (c.begin==2) {
            s1.sout();
        }
        while(k!=3){
            k=-1;
            while (k!=0) {
                s = c.getCell();
                mas = m.shot(s);
                k = mas[0];
                c.sendResult(mas);
                s1.sout();
                if (k == 3) {
                    break;
                }
            }
            if (k == 3) {
                break;
            }
            k=-1;
            while (k!=0) {
                System.out.println("Enter cell for attacking");
                s = sc.next();
                v = o.shot(s);
                while (!v) {
                    s = sc.next();
                    v = o.shot(s);
                }
                c.sendCell(s);
                mas = c.getResult();
                o.shot1(mas);
                k = mas[0];
                s1.sout();
                if (k == 3) {
                    break;
                }
            }
        }
    }
}
