package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;

import java.util.ArrayList;


public class Ship {
    private ArrayList<Cell> decks;

    public Ship(){
        this.decks = new ArrayList<Cell>();
    }

    public boolean life() {
        boolean c = false;
        for (int i = 0; i<decks.size() && !c ; i++) {
            c=decks.get(i).getLife();
        }
        return c;
    }
    public void addDeck (Cell c){
        this.decks.add(decks.size(),c);
    }
}
