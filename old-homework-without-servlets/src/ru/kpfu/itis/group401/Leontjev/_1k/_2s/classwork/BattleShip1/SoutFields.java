package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.BattleShip1;

/**
 * Created by Timur_Zakirov on 10.05.2015.
 */
public class SoutFields {
    private static MyField m;
    private static OpponentField o;

    public SoutFields(MyField m, OpponentField o) {
        this.o = o;
        this.m = m;
    }

    public void sout() {
        System.out.println();
        System.out.println("              Your                      Opponent ");
        System.out.println("       A B C D E F G H I J        A B C D E F G H I J ");
        for (int i = 1; i < 11; i++) {
            if (i!=10) {
                System.out.print("     " + i + " ");
                m.sout(i - 1);
                System.out.print("     " + i + " ");
                o.sout(i - 1);
                System.out.println();
            }
            else{
                System.out.print("    " + i + " ");
                m.sout(i - 1);
                System.out.print("    " + i + " ");
                o.sout(i - 1);
                System.out.println();
            }
        }
    }
}
