package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.Multithreading;

/**
 * Created by Alexander on 30.04.15.
 */

public class MyThread extends Thread {
    public MyThread(String string) {
        super(string);
    }

    public static void main(String[] args) throws InterruptedException {

        MyThread myThread = new MyThread("1");    //поток создан, но не запущен
        MyThread myThread2 = new MyThread("\t\t\t2");

        myThread.start();  //чтобы запустить, надо вызвать метод start()
        Thread.sleep(500);
        myThread2.start();


        //run - это не запуск потока, это просто метод

    }

    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(this.getName() + " : " + i);
            //Thread.yield();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //поток нельзя перезапустить повторно, поток работает один раз

}


