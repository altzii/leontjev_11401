package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.ParseTree;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Alexander on 07.04.15.
 */

class Node {
    String value;
    Node son;
    Node next;


    public Node(String value) {
        this.value = value;
    }

    public void createSons(Map<String, ArrayList<String>> map) {
        ArrayList<String> arrayList = map.get(this.value);

        for (String elem : arrayList) {
            Node node = new Node(elem);
            this.son = node;

        }

    }
}