package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.ParseTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Alexander on 07.04.15.
 */

public class Test {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("grammar.txt"));
        Map<String, ArrayList<String>> grammar = new LinkedHashMap<String, ArrayList<String>>();

        while (scanner.hasNextLine()) {
            String[] strings = scanner.nextLine().split("->");
            ArrayList<String> arrayList = new ArrayList<String>();

            String[] strings1 = strings[1].split("\\|");

            for (String string1 : strings1) {
                arrayList.add(string1.trim());
            }
            grammar.put(strings[0].trim(), arrayList);
        }
        System.out.println(grammar);
    }


}
