package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.Privet;

/**
 * Created by Alexander on 26.03.15.
 */
public class List implements Cloneable {
    Element head;

    public void insert(Element k) {
        k.setNext(this.head);
        this.head = k;

    }

    public void privet() throws CloneNotSupportedException {
        List list = (List) this.clone();

        Element elem1 = list.head;
        System.out.println("Net у клонированного " + elem1.getNext());
        Element elem2 = this.head;
        System.out.println("Next у исходного " + elem2.getNext());



        System.out.println(list.getSize());
        System.out.println(this.getSize());

        elem1.getNext().i = 12;
        System.out.println(elem2.getNext().i);
        System.out.println(elem1.getNext().i);


    }

    public int getSize() {
        Element elem = this.head;
        int i = 0;

        while (elem != null) {
            i++;
            elem = elem.getNext();
        }
        return i;
    }
}
