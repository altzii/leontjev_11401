package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.java_io;

import java.io.*;

/**
 * Created by Alexander on 16.04.15.
 */

public class Test {
    public static void main(String[] args) throws IOException {
        //FileInputStream - FileOutputStream
        double t1 = System.nanoTime();

        InputStream inputStream = new FileInputStream("in.txt");
        OutputStream outputStream = new FileOutputStream(new File("out.txt"));

        int p = 0;
        while (p != -1) {
            p = inputStream.read();
            if (p != -1) {
                outputStream.write(p);
            }
        }
        inputStream.close();
        outputStream.close();
        double t2 = System.nanoTime();
        System.out.println("FileInputStream - FileOutputStream = " + (t2 - t1));

        //BufferedInputStream - BufferedOutputStream
        t1 = System.nanoTime();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(
                new FileInputStream("in.txt") {
                }
        );

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
                new FileOutputStream("out1.txt") {
                }
        );

        p = 0;
        while (p != -1) {
            p = bufferedInputStream.read();
            if (p != -1) {
                bufferedOutputStream.write(p);
                bufferedOutputStream.flush();
            }
        }

        bufferedInputStream.close();
        bufferedOutputStream.close();
        t2 = System.nanoTime();

        System.out.println("BufferedInputStream - BufferedOutputStream = " + (t2 - t1));

        //FileReader - FileWriter
        t1 = System.nanoTime();
        FileReader fileReader = new FileReader("in.txt");
        FileWriter fileWriter = new FileWriter("out2.txt");

        p = 0;
        while (p != -1) {
            p = fileReader.read();
            if (p != -1) {
                fileWriter.write(p);
            }
        }
        fileReader.close();
        fileWriter.close();
        t2 = System.nanoTime();
        System.out.println("FileReader - FileWriter = " + (t2 - t1));

        //BufferedReader - PrintWriter
        t1 = System.nanoTime();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("in.txt"));
        PrintWriter printWriter = new PrintWriter(new FileWriter("out3.txt"));

        p = 0;
        while (p != -1) {
            p = bufferedReader.read();
            if (p != -1) {
                printWriter.write(p);
                printWriter.flush();
            }
        }
        bufferedReader.close();
        printWriter.close();
        t2 = System.nanoTime();
        System.out.println("BufferedReader - PrintWriter = " + (t2 - t1));
    }
}
