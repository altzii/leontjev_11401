package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150217;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * Created by Alexander on 17.02.15.
 */

public class task001 {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int n = scanner.nextInt();

        for (int i = 0; i < n + 1; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        double result = p.getValue();

        for (int i = 1; i <= n; i++) {
            result *= x;
            p = p.getNext();
            result += p.getValue();
        }
        System.out.println(result);
    }
}
