package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150217;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * Created by Alexander on 17.02.15.
 */

public class task002 {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        //2 4 1 3


        int y = n;
        int z = 0;

        for (int k = 0; k < n - 1; k++) {
            y = y - 1;
            for (int j = 0; j < y; j++) {
                z = p.getValue();
                if (p.getValue() > p.getNext().getValue()) {
                    p.setValue(p.getNext().getValue());
                    p = p.getNext();
                    p.setValue(z);
                } else {
                    p = p.getNext();
                }
            }
            p = head;
        }

        for (int f = 0; f < n; f++) {
            System.out.println(p.getValue());
            p = p.getNext();

        }
    }
}
