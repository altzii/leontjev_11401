package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150217;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * Created by Alexander on 17.02.15.
 */

public class task003 {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();


        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        for (int i = 0; i < n; i++) {
            if (p.getValue() % 2 == 0) {
                Elem q = new Elem();
                q.setValue(0);
                q.setNext(p.getNext());
                p.setNext(q);
                p = q.getNext();
            } else p = p.getNext();
        }

        p = head;

        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
