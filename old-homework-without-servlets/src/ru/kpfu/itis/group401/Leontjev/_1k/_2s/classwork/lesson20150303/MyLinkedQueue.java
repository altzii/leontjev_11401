package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150303;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14.Elem;

/**
 * Created by Alexander on 03.03.15.
 */

public class MyLinkedQueue<T> implements MyQueue<T> {
    private Elem<T> head = null;
    private Elem<T> tail = null;       //добавлять будем к хвосту, забирать с головы

    @Override
    public void offer(T t) {
        Elem<T> elem = new Elem<T>();
        elem.setValue(t);

        if (this.isEmpty()) {
            elem.setNext(tail);
            head = elem;
            tail = elem;
        } else {
            elem.setNext(tail);
            tail = elem;
        }
    }


    @Override
    public T peek() {
        return head.getValue();
    }

    @Override
    public T poll() {
        if (!this.isEmpty()) {
            T t = head.getValue();
            head = head.getNext();
            return t;
        } else {
            return null;
        }
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
