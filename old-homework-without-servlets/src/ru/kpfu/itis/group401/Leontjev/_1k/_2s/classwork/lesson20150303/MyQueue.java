package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150303;

/**
 * Created by Alexander on 03.03.15.
 */

public interface MyQueue<T> {
    public void offer(T t);
    public T peek();
    public T poll();
    public boolean isEmpty();

}
