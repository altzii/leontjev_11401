package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150303;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Alexander on 03.03.15.
 */

public class Task001 {
    public static void main(String[] args) {
        ArrayList<String> stringArrayList = new ArrayList<String>();

        stringArrayList.add("aaa");
        stringArrayList.add("qwerty");
        stringArrayList.add("w");

        stringArrayList.sort(c);


        int i = 0;

        while (i != stringArrayList.size()) {
            System.out.println(stringArrayList.get(i));
            i++;
        }
    }

    static Comparator<String> c = new Comparator<String>() {
        public int compare(String s1, String s2) {
            return (int) Math.signum(s1.length() - s2.length());
        }
    };
}
