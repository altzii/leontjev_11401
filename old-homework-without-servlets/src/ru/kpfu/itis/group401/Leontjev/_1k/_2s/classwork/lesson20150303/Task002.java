package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150303;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task050.RationalFraction;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by Alexander on 03.03.15.
 */

public class Task002 {
    public static void main(String[] args) {
        PriorityQueue<RationalFraction> priorityQueue = new PriorityQueue<RationalFraction>();

        RationalFraction rf = new RationalFraction();
        RationalFraction rf2 = new RationalFraction();

        rf.setN(2);
        rf.setD(11);

        rf2.setN(2);
        rf2.setD(4);

        priorityQueue.add(rf);
        priorityQueue.add(rf2);

        System.out.println(priorityQueue);



    }

    Comparator<RationalFraction> c = new Comparator<RationalFraction>() {
        public int compare(RationalFraction rf, RationalFraction rf2) {
            return (int) Math.signum(rf.getN() - rf2.getN());
        }
    };

    Comparator<RationalFraction> c1 = new Comparator<RationalFraction>() {
        public int compare(RationalFraction rf, RationalFraction rf2) {
            return rf.compareTo(rf2);
        }
    };


}


