package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150305;

/**
 * Created by Alexander on 05.03.15.
 */

public class Student implements Comparable<Student> {
    String lastName;
    String firstName;
    int year;
    char gender;
    int scoreProgramming;
    int scoreMA;
    int hashCode;

    public Student() {

    }

    public Student(String lastName, String firstName, int year, char gender) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.year = year;
        this.gender = gender;
    }

    public void setScoreProgramming(int scoreProgramming) {
        this.scoreProgramming = scoreProgramming;
    }

    public void setScoreMA(int scoreMA) {
        this.scoreMA = scoreMA;
    }

    public int getScoreProgramming() {
        return this.scoreProgramming;
    }

    public int getScoreMA() {
        return this.scoreMA;
    }

    public String toString() {
        return lastName + " " + firstName;
    }

    @Override
    public int compareTo(Student student) {
        return (int) -Math.signum(this.year - student.year);
    }


}
