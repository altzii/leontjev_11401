package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150305;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Alexander on 05.03.15.
 */
public class TestClass {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<Student>();

        Student s1 = new Student("Leontjev", "Alexander", 1995, 'm');
        Student s2 = new Student("Khakov", "Rustam", 1996, 'f');
        Student s3 = new Student("Chernov", "Filipp", 1997, 'm');

        s1.setScoreProgramming(97);
        s2.setScoreProgramming(98);
        s3.setScoreProgramming(58);

        s1.setScoreMA(92);
        s2.setScoreMA(96);
        s3.setScoreMA(56);

        students.add(s1);
        students.add(s2);
        students.add(s3);

        System.out.println(students);

        Collections.sort(students);
        System.out.println(students);

        Collections.sort(students, byScoreProgramming);
        System.out.println(students);

        Collections.sort(students, byScoreMA);
        System.out.println(students);

        Collections.sort(students, byGender);
        System.out.println(students);


    }

    static Comparator<Student> byScoreProgramming = new Comparator<Student>() {       //по убыванию
        public int compare(Student s1, Student s2) {
            return (int) -Math.signum(s1.getScoreProgramming() - s2.getScoreProgramming());
        }
    };

    static Comparator<Student> byScoreMA = new Comparator<Student>() {             //по возрастанию
        public int compare(Student s1, Student s2) {
            return (int) Math.signum(s1.getScoreMA() - s2.getScoreMA());
        }
    };

    static Comparator<Student> byGender = new Comparator<Student>() {             //сначала m, потом f
        public int compare(Student s1, Student s2) {
            if (s1.gender == s2.gender) {
                return 0;
            }
            if (s1.gender > s2.gender) {
                return 1;
            } else return -1;
        }
    };




}
