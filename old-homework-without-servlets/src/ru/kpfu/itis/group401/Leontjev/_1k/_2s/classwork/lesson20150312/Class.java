package ru.kpfu.itis.group401.Leontjev._1k._2s.classwork.lesson20150312;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Alexander on 12.03.15.
 */

public class Class {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("english.txt"));
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();

        String string = "";

        while (scanner.hasNextLine()) {
            string = scanner.nextLine().toLowerCase();
        }

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) <= 'z' & string.charAt(i) >= 'a') {
                if (!map.containsKey(string.charAt(i))) {
                    map.put(string.charAt(i), 1);
                } else {
                    int k = map.get(string.charAt(i));
                    map.put(string.charAt(i), ++k);
                }
            }
        }
        System.out.println(map);
    }
}
