package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task002c
 */

public class Task002c {
    public static void main(String[] args) {
        System.out.println(pow(2, 10));
    }

    static double pow(double x, int n) {
        double res;
        if (n == 0) {
            return 1;
        } else {
            return x * pow(x, n - 1);
        }
    }
}

