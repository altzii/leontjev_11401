package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task003b029
 */

public class Task003b029 {
    public static void main(String[] args) {

        System.out.println(convert(2342, 5));
    }

    static int convert(int n, int k) {
        if (n == 0) {
            return 0;
        } else {
            return n % 10 + convert(n / 10, k) * k;
        }
    }
}