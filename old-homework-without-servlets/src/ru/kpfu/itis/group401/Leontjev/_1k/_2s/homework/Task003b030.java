package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task003b030
 */

public class Task003b030 {
    public static void main(String[] args) {


        hey(4);

    }

    static boolean bool1(int x) {
        if (x == 0) {
            return true;
        } else {
            return (x % 2 == 0) && (bool1(x / 10));
        }
    }

    static boolean bool2(int x) {
        if (x == 0) {
            return true;
        } else {
            return (x % 2 != 0) && (bool2(x / 10));
        }
    }

    static boolean bool(int x) {
        return bool1(x) || bool2(x);
    }

    static int m = 0;

    static int he(int n) {
        if (m > 2) {
            return -1;
        } else {
            if (n == 0) {
                return m;
            } else {
                Scanner scanner = new Scanner(System.in);
                int x = scanner.nextInt();

                if ((bool(x)) & (((x > 99) && (x < 1000)) || ((x > 9999) && (x < 100000)))) {
                    m++;
                    return he(n - 1);
                } else {
                    return he(n - 1);
                }
            }
        }
    }

    static void hey(int n) {
        m = he(n);
        if ((m == -1) || (m != 2)) {
            System.out.println("FALSE");
        } else {
            System.out.println("TRUE");
        }
        m = 0;
    }

}




