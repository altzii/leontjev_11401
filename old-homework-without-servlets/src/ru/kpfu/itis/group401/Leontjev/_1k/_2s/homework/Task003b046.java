package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task003b046
 */

public class Task003b046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String str1 = scanner.nextLine();
        String s1 = str1.toLowerCase();
        String str2 = scanner.nextLine();
        String s2 = str2.toLowerCase();

        func(s1, s2);
    }

    static void func(String s1, String s2, int i) {
        if ((i < Math.min(s1.length(), s2.length()))) {
            if (s1.charAt(i) < s2.charAt(i)) {
                System.out.println(s1);

            } else if (s1.charAt(i) > s2.charAt(i)) {
                System.out.println(s2);

            } else {
                func(s1, s2, i + 1);
            }

        } else if (i == s1.length()) {
            System.out.println(s1);
        } else System.out.println(s2);
    }

    static void func(String s1, String s2) {
        func(s1, s2, 0);
    }
}





