package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task003c008
 */

public class Task003c008 {
    public static void main(String[] args) {
        multiplicationTable(8);
    }

    static void multiplicationTable(int k, int n) {
        if (n == 10) {
        } else {
            System.out.println(k + " * " + n + " = " + (k * n));
            multiplicationTable(k, n + 1);
        }
    }

    static void multiplicationTable(int k) {
        multiplicationTable(k, 1);
    }
}
