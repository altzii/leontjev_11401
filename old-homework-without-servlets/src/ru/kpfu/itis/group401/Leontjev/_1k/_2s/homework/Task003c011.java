package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task003c011
 */

public class Task003c011 {
    public static void main(String[] args) {
        System.out.println(doubleFact(3));
    }

    static int doubleFact(int n) {
        if ((n == 0) | (n == 1)) {
            return 1;
        } else {
            return n * doubleFact(n - 2);
        }
    }

}
