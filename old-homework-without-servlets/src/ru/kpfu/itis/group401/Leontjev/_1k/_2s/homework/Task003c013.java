package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task003c013
 */

public class Task003c013 {
    public static void main(String[] args) {
        System.out.println(func(6));
    }

    static double func(int n) {
        if (n == 1) {
            return 4.0 / 3;
        } else {
            return (4.0 * n * n) / (4 * n * n - 1) * func(n - 1);
        }
    }
}

