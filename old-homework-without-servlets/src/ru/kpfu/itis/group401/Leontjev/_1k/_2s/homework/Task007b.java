package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task007b
 */

public class Task007b {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        while (p != null && p.getValue() % 2 != 0) {
            p = p.getNext();
            head = p;
        }

        while (p != null && p.getNext() != null) {
            if (p.getNext().getValue() % 2 != 0) {
                p.setNext(p.getNext().getNext());
            } else p = p.getNext();
        }

        p = head;
        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
