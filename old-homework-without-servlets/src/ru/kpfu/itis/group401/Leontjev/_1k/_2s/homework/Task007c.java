package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task007c
 */

public class Task007c {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        if (n > 3) {
            while (p.getNext().getNext().getNext().getNext() != null) {
                p = p.getNext();
            }
            p.setNext(null);

        } else {
            head = null;
        }

        p = head;

        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
