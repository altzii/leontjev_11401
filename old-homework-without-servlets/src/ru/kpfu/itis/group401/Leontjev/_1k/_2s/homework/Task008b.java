package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task008b
 */

public class Task008b {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        while (p != null) {
            if (p.getValue() % 2 == 0) {
                Elem q = new Elem();
                q.setValue(100);
                q.setNext(p.getNext());
                p.setNext(q);

                q.setValue(p.getValue());
                p.setValue(100);
                p = p.getNext().getNext();
            } else p = p.getNext();
        }

        p = head;

        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
