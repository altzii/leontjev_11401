package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task009a
 */

public class Task009a {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        while (p != null) {
            if (isPrime(p.getValue())) {
                Elem q = new Elem();
                q.setValue(p.getValue() % 10);
                q.setNext(p.getNext());
                p.setNext(q);

                Elem r = new Elem();
                int m = getFirst(p.getValue());
                r.setValue(m);
                r.setNext(p.getNext());
                p.setNext(r);

                r.setValue(p.getValue());
                p.setValue(m);

                p = p.getNext().getNext().getNext();
            } else p = p.getNext();
        }

        p = head;

        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }

    public static int getFirst(int a) {
        while (a / 10 > 0) {
            a = a / 10;
        }
        return a;

    }

    public static boolean isPrime(int a) {
        if (a == 1) {
            return false;
        } else {
            for (int i = 2; i <= a / 2; i++) {
                if (a % i == 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
