package ru.kpfu.itis.group401.Leontjev._1k._2s.homework;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06.Elem;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task010a
 */

public class Task010a {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();
        Elem tail;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int a = scanner.nextInt();
        p = new Elem();
        p.setValue(a);

        head = p;
        tail = p;

        for (int i = 1; i < n; i++) {
            a = scanner.nextInt();
            p = new Elem();
            p.setValue(a);
            p.setNext(tail.getNext());
            tail.setNext(p);
            tail = p;
        }

        p = head;

        int res = 0;

        while (p != null) {
            if (p.getNext() != null) {
                res += p.getValue();
                res *= 2;
            } else {
                res = res + p.getValue();
            }
            p = p.getNext();
        }

        head = null;
        p = null;
        int x = 0;

        if (res != 0) {
            while (res > 0) {
                p = new Elem();
                x = res % 10;
                p.setValue(x);
                res = res / 10;
                p.setNext(head);
                head = p;
            }
        } else {
            p = new Elem();
            p.setValue(0);
            head = p;
        }

        p = head;

        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}