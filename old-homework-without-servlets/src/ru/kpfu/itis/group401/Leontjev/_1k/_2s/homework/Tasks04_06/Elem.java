package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06;

/**
 * @author Alexander Leontjev
 *         11-401
 *         class Elem for Tasks04_06
 */

public class Elem {
    private int value;
    private Elem next;

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Elem() {
    }

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


}
