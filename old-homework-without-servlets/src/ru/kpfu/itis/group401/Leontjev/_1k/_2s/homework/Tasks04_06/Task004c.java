package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task004c
 */


public class Task004c {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        boolean flag = false;

        for (int i = 0; i < n & !flag; i++) {
            flag = p.getValue() == 0;
            p = p.getNext();
        }

        System.out.println(flag);


    }
}


