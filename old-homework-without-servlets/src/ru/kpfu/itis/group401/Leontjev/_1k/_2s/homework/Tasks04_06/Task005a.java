package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task005a
 */


public class Task005a {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        int k = 0;

        for (int i = 0; i < n - 2; i++) {
            int x = p.getValue();
            p = p.getNext();

            if (((p.getValue() > x) & (p.getValue() > p.getNext().getValue())) || ((p.getValue() < x) & (p.getValue() < p.getNext().getValue()))) {
                k++;
            }
        }
        System.out.println(k);
    }
}