package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task005b
 */

public class Task005b {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            p = new Elem();
            int a = scanner.nextInt();
            p.setValue(a);
            p.setNext(head);
            head = p;
        }

        int m = 1;

        for (int i = 0; i < n; i += 2) {
            m *= p.getValue();
            p = p.getNext();

            if (p != null) {
                p = p.getNext();
            }
        }
        System.out.println(m);
    }
}