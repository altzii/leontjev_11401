package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks04_06;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task006a
 */

public class Task006a {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();
        Elem tail;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int a = scanner.nextInt();
        p = new Elem();
        p.setValue(a);

        head = p;
        tail = p;

        for (int i = 1; i < n; i++) {
            a = scanner.nextInt();
            p = new Elem();
            p.setValue(a);
            p.setNext(tail.getNext());
            tail.setNext(p);
            tail = p;
        }

        p = head;

        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}



