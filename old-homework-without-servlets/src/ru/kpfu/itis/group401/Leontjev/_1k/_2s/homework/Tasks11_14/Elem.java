package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

/**
 * Created by Alexander on 26.02.15.
 */

public class Elem<T> {
    private T value;
    private Elem<T> next;

    public Elem() {
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem<T> next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}

