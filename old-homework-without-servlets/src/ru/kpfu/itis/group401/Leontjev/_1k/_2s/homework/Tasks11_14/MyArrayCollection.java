package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task011c
 */

public class MyArrayCollection implements Collection<Integer> {
    protected int size = 0;
    protected final int CAPACITY = 10000;
    protected int array[] = new int[CAPACITY];
    static int i = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < this.size; i++) {
            if (((Integer) o).equals(array[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {
       return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        if (this.size + 1 <= CAPACITY) {
            array[size] = integer;
            this.size++;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        int i;
        for (i = 0; i < this.size; i++) {
            if (((Integer) o).equals(array[i])) {
                array[i] = array[i + 1];
                this.size--;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }


    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        if (this.CAPACITY - this.size >= c.size()) {
            for (Integer o : c) {
                this.add(o);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean removed = false;
        for (Object o : c) {
            while (remove(o)) {
                removed = true;
            }
        }
        return removed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        MyArrayCollection arrayCollection = new MyArrayCollection();
        boolean added = false;
        for (int i = 0; i < this.size(); i++) {
            for (Object o : c) {
                if (((Integer) o).equals(this.array[i])) {
                    arrayCollection.add((Integer) o);
                    added = true;
                }
            }
        }

        this.clear();

        int i = 0;

        while (i < arrayCollection.size()) {
            this.add(arrayCollection.array[i]);
            i++;
        }
        return added;
    }


    @Override
    public void clear() {
        this.size = 0;
    }
}
