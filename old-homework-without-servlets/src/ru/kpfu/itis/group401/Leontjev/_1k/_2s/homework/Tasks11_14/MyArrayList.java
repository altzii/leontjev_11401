package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task011b
 */

public class MyArrayList extends MyArrayCollection implements List<Integer> {
    @Override

    public boolean addAll(int index, Collection<? extends Integer> c) {
        if (this.CAPACITY - this.size >= c.size()) {
            for (Integer o : c) {
                this.add(index, o);
                index++;
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Integer get(int index) {
        return this.array[index];
    }

    @Override
    public Integer set(int index, Integer element) {
        Integer wasReplaced = this.get(index);
        this.array[index] = element;
        return wasReplaced;
    }

    @Override
    public void add(int index, Integer element) {
        for (int i = this.size; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = element;
        this.size++;
    }

    @Override
    public Integer remove(int index) {
        Integer integer = this.get(index);

        while (index < this.size) {
            array[index] = array[index + 1];
            index++;
        }
        this.size--;
        return integer;
    }


    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < this.size; i++) {
            if (o.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int k = -1;
        for (int i = 0; i < this.size; i++) {
            if (o.equals(array[i])) {
                k = i;
            }
        }
        return k;
    }

    @Override
    public ListIterator<Integer> listIterator() {
       return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public MyArrayList subList(int fromIndex, int toIndex) {
        MyArrayList list = new MyArrayList();
        int i = fromIndex;
        while (i <= toIndex) {
            list.add(this.get(i));
            i++;
        }
        return list;
    }
}
