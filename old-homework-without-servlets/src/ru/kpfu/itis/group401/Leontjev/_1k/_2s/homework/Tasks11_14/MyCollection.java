package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task011a
 */

public class MyCollection<T> implements Collection<T> {
    protected Elem<T> head = new Elem<T>();
    protected int size = 0;

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Elem<T> elem = head;
        boolean contains = true;

        while (elem != null & contains) {
            if (elem.getValue().equals(o)) {
                contains = false;
            }
            elem = elem.getNext();
        }
        return contains;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        Elem<T> elem = new Elem<T>();
        elem.setValue(t);
        elem.setNext(this.head);
        this.head = elem;
        this.size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (this.isEmpty()) {
            return false;
        } else {
            Elem<T> elem;
            boolean flag = ((T) o).equals(this.head.getValue());

            if (flag) {
                head = head.getNext();
            } else {
                elem = head;
                while (elem.getNext() != null & !flag) {
                    flag = ((T) o).equals(elem.getNext().getValue());
                    if (!flag) {
                        elem = elem.getNext();
                    }
                }
                if (flag) {
                    this.size--;
                    elem.setNext(elem.getNext().getNext());
                }
            }
            return flag;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T o : c) {
            this.add(o);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean removed = false;
        for (Object o : c) {
            while (remove((T) o)) {
                removed = true;
            }
        }
        return removed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        MyCollection<T> myCollection = new MyCollection<T>();
        Elem<T> elem = this.head;
        boolean added = false;

        while (elem != null) {
            for (Object o : c) {
                if (((T) o).equals(elem.getValue())) {
                    myCollection.add((T) o);
                    added = true;
                    elem = elem.getNext();
                }
            }
        }
        this.clear();
        elem = myCollection.head;

        while (elem != null) {
            this.add(elem.getValue());
            elem = elem.getNext();
        }
        return added;
    }

    @Override
    public void clear() {
        this.size = 0;
        this.head = null;
    }
}
