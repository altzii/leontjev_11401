package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task012b
 */

public class MyLinkedList extends MyLinkedCollection implements List<Integer> {

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        for (Integer o : c) {
            this.add(index, o);
            index++;
        }
        return true;
    }

    @Override
    public Integer get(int index) {
        Elem<Integer> elem = this.head;
        int i = 0;

        while (i < index) {
            elem = elem.getNext();
            i++;
        }
        return elem.getValue();
    }

    @Override
    public Integer set(int index, Integer element) {
        Elem<Integer> elem = this.head;
        int i = 0;
        while (i < index) {
            elem = elem.getNext();
            i++;
        }
        int wasReplaced = elem.getValue();
        elem.setValue(element);
        return wasReplaced;
    }

    @Override
    public void add(int index, Integer element) {
        Elem<Integer> elem = this.head;
        int i = 0;
        while (i < index - 1) {
            elem = elem.getNext();
            i++;
        }
        Elem<Integer> q = new Elem<Integer>();
        q.setValue(element);
        q.setNext(elem.getNext());
        elem.setNext(q);
        this.size++;
    }

    @Override
    public Integer remove(int index) {
        Elem<Integer> elem = this.head;
        Integer integer = this.head.getValue();

        if (index == 0) {
            this.head = this.head.getNext();
        } else {
            int i = 0;
            while (i < index - 1) {
                elem = elem.getNext();
                i++;
            }
            integer = elem.getNext().getValue();
            elem.setNext(elem.getNext().getNext());
            this.size--;
        }
        return integer;
    }

    @Override
    public int indexOf(Object o) {
        Elem<Integer> elem = this.head;

        for (int i = 0; i < this.size; i++) {
            if (((Integer) o).equals(elem.getValue())) {
                return i;
            }
            elem = elem.getNext();
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Elem<Integer> elem = this.head;
        int k = -1;

        for (int i = 0; i < this.size; i++) {
            if (((Integer) o).equals(elem.getValue())) {
                k = i;
            }
            elem = elem.getNext();
        }
        return k;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        Elem<Integer> elem = this.head;
        MyLinkedList list = new MyLinkedList();
        int i = 0;
        while (i < fromIndex) {
            elem = elem.getNext();
            i++;
        }

        i = fromIndex;
        while (i <= toIndex) {
            list.add(elem.getValue());
            elem = elem.getNext();
            i++;
        }
        return list;
    }
}
