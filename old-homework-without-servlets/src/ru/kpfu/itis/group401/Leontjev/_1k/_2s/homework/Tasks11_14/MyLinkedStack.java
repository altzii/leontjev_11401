package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task013c
 */

public class MyLinkedStack<T> implements MyStack<T> {
    private Elem<T> head = null;

    @Override
    public void push(T t) {
        Elem<T> elem = new Elem<T>();
        elem.setValue(t);
        elem.setNext(head);
        head = elem;
    }

    @Override
    public T pop() {
        T t = head.getValue();
        head = head.getNext();
        return t;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T peek() {
        T t = head.getValue();
        return t;
    }
}
