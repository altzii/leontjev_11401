package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task012a
 */

public class MyList<T> extends MyCollection<T> implements List<T> {

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        for (T o : c) {
            this.add(index, o);
            index++;
        }
        return true;
    }

    @Override
    public T get(int index) {
        Elem<T> elem = this.head;
        int i = 0;

        while (i < index) {
            elem = elem.getNext();
            i++;
        }
        return elem.getValue();
    }

    @Override
    public T set(int index, T element) {
        Elem<T> elem = this.head;
        int i = 0;
        while (i < index) {
            elem = elem.getNext();
            i++;
        }
        T wasReplaced = elem.getValue();
        elem.setValue(element);
        return wasReplaced;
    }

    @Override
    public void add(int index, T element) {
        Elem<T> elem = this.head;
        int i = 0;
        while (i < index - 1) {
            elem = elem.getNext();
            i++;
        }
        Elem<T> q = new Elem<T>();
        q.setValue(element);
        q.setNext(elem.getNext());
        elem.setNext(q);
        this.size++;

    }

    @Override
    public T remove(int index) {
        Elem<T> elem = this.head;
        T t = this.head.getValue();

        if (index == 0) {
            this.head = this.head.getNext();
        } else {
            int i = 0;
            while (i < index - 1) {
                elem = elem.getNext();
                i++;
            }
            t = elem.getNext().getValue();
            elem.setNext(elem.getNext().getNext());
            this.size--;
        }
        return t;
    }

    @Override
    public int indexOf(Object o) {
        Elem<T> elem = this.head;

        for (int i = 0; i < this.size; i++) {
            if (((T) o).equals(elem.getValue())) {
                return i;
            }
            elem = elem.getNext();
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Elem<T> elem = this.head;
        int k = -1;

        for (int i = 0; i < this.size; i++) {
            if (((Integer) o).equals(elem.getValue())) {
                k = i;
            }
            elem = elem.getNext();
        }
        return k;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        Elem<T> elem = this.head;
        MyList<T> list = new MyList<T>();
        int i = 0;
        while (i < fromIndex) {
            elem = elem.getNext();
            i++;
        }

        i = fromIndex;
        while (i <= toIndex) {
            list.add(elem.getValue());
            elem = elem.getNext();
            i++;
        }
        return list;
    }
}
