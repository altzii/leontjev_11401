package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task013c
 */

public interface MyStack<T> {
    public void push(T t);
    public T pop();
    public boolean isEmpty();
    public T peek();
}
