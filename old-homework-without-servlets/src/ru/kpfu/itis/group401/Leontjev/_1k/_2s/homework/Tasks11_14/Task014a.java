package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task014a
 */

public class Task014a {
    public static void main(String[] args) {
        String string = "2,2,*,3,+,4,-";

        MyLinkedStack<Double> myStack = new MyLinkedStack<Double>();

        int i = 0;
        double res = 0;
        while (i < string.length()) {
            if ("123456789".contains(Character.toString(string.charAt(i)))) {
                myStack.push((double) Character.getNumericValue(string.charAt(i)));

            }

            if ("/*-+".contains(Character.toString(string.charAt(i)))) {
                reversePolishNotation(string.charAt(i), myStack);
            }
            i++;
        }
        System.out.println(myStack.peek());
    }

    static public void reversePolishNotation(Character character, MyLinkedStack<Double> myStack) {
        double res = 0;

        switch (character) {
            case '*': {
                res = myStack.pop() * myStack.pop();
                myStack.push(res);
                break;
            }
            case '/': {
                double k = myStack.peek();
                myStack.pop();
                res = myStack.pop() / k;
                myStack.push(res);
                break;
            }
            case '+': {
                res = myStack.pop() + myStack.pop();
                myStack.push(res);
                break;
            }
            case '-': {
                double k = myStack.peek();
                myStack.pop();
                res = myStack.pop() - k;
                myStack.push(res);
                break;
            }
        }

    }
}
