package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task014b
 */

public class Task014b {
    public static void main(String[] args) {
        String string;
        string = "<<([])>)>";
        check(string);
    }


    static public Character getOpening(Character character) {
        Character openingCharacter = ' ';
        switch (character) {
            case ']':
                openingCharacter = '[';
                break;
            case ')':
                openingCharacter = '(';
                break;
            case '>':
                openingCharacter = '<';
                break;
        }
        return openingCharacter;

    }

    static public void check(String string) {
        MyLinkedStack<Character> linkedStack = new MyLinkedStack<Character>();
        int i = 0;
        int flag = 0;
        boolean b = false;

        while (i < string.length() & flag == 0) {
            if ((string.charAt(i) == '[') || (string.charAt(i) == '(') || (string.charAt(i) == '<')) {
                linkedStack.push(string.charAt(i));
            }
            if ((string.charAt(i) == ']') || (string.charAt(i) == ')') || (string.charAt(i) == '>')) {
                if (linkedStack.isEmpty()) {
                    flag = -1;
                } else {
                    if (getOpening(string.charAt(i)) == linkedStack.peek()) {
                        linkedStack.pop();
                        flag = 0;
                    } else {
                        if (string.length() % 2 != 0) {
                            b = true;
                            while (!linkedStack.isEmpty() && flag != 2) {
                                if (getOpening(string.charAt(i)) == linkedStack.pop()) {
                                    flag = 2;
                                }
                            }
                        } else {
                            if (string.length() % 2 != 0) {
                                flag = -1;
                            } else {
                                flag = 1;
                            }
                        }
                    }
                }
            }
            i++;
        }


        if (!linkedStack.isEmpty() & flag == 0) {
            flag = 2;
        }

        if (b & flag == 0) {
            flag = 1;
        }

        if (flag == 1) {
            System.out.println("Скобки не соответсвуют");
        }

        if (flag == -1) {
            System.out.println("Встретилась лишняя закрывающая");
        }

        if (flag == 0) {
            System.out.println("Everything is OK!");
        }

        if (flag == 2) {
            System.out.println("Не все открывающие закрыты");
        }
    }
}

