package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task014c
 */

public class Task014c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int a[] = new int[n];

        int i = 0;

        while (i < n) {
            a[i] = scanner.nextInt();
            i++;
        }

        turnAround(a);

        i = 0;
        while (i < n) {
            System.out.println(a[i]);
            i++;
        }
    }

    static public void turnAround(int[] array) {
        MyLinkedStack<Integer> arrayStack = new MyLinkedStack<Integer>();

        int i = 0;
        while (i < array.length) {
            arrayStack.push(array[i]);
            i++;
        }

        i = 0;
        while (!arrayStack.isEmpty()) {
            array[i] = arrayStack.pop();
            i++;
        }
    }
}
