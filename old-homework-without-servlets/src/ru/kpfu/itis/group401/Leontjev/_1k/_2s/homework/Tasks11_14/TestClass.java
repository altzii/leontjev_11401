package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks11_14;

import java.util.ArrayList;

/**
 * Created by Alexander on 08.03.15.
 */

public class TestClass {
    public static void main(String[] args) {

        //MyArrayCollection test
        MyArrayCollection myArrayCollection = new MyArrayCollection();

        myArrayCollection.add(2);
        myArrayCollection.add(3);
        myArrayCollection.add(11);
        myArrayCollection.add(134);
        myArrayCollection.add(4);


        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(8);
        list.add(9);
        list.add(4);
        list.add(134);


        myArrayCollection.addAll((list));

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(134);
        list2.add(999);

        myArrayCollection.retainAll(list2);

        System.out.println(myArrayCollection.size());

        System.out.println(myArrayCollection.array[0]);
        System.out.println(myArrayCollection.array[1]);

        System.out.println("\n");

        


        //MyArrayList test

        MyArrayList myArrayList = new MyArrayList();

        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(2);

        System.out.println(myArrayList.size());

        System.out.println(myArrayList.get(2));

        myArrayList.addAll(0, list2);

        System.out.println(myArrayList.size());

        MyArrayList list3;

        list3 = myArrayList.subList(3, 4);

        System.out.println(list3.get(0));
        System.out.println(list3.get(1));


        //MyCollection

        System.out.println("\n");

        MyCollection<Integer> myCollection = new MyCollection<Integer>();
        myCollection.add(1);
        myCollection.add(3);
        myCollection.add(2);
        myCollection.add(1337);
        myCollection.add(17);
        myCollection.add(12);

        System.out.println(myCollection.size());


        myCollection.remove(3);
        myCollection.remove(1337);


        Elem elem = myCollection.head;

        System.out.println("size = " + myCollection.size());

        while (elem.getNext() != null) {
            System.out.println(elem.getValue());
            elem = elem.getNext();
        }


        ArrayList list1 = new ArrayList();
        list1.add(17);
        list1.add(2);



        myCollection.removeAll(list1);

        elem = myCollection.head;
        while (elem.getNext() != null) {
            System.out.println(elem.getValue());
            elem = elem.getNext();
        }


        //MyLinkedList

        MyLinkedList list4 = new MyLinkedList();


        list4.add(1);
        list4.add(2);
        list4.add(3);
        list4.add(4);
        list4.add(4);


        System.out.println(list4.lastIndexOf(4));




    }
}
