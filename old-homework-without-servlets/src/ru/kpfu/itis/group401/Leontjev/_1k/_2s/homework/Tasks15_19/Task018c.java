package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks15_19;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task049.Vector2D;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Alexander on 23.03.15.
 */

public class Task018c {
    public static void main(String[] args) {
        ArrayList<Vector2D> list = new ArrayList<Vector2D>();
        Vector2D v1 = new Vector2D(3, 4);
        Vector2D v2 = new Vector2D(0, 3);
        Vector2D v3 = new Vector2D(1, 1);

        list.add(v1);
        list.add(v2);
        list.add(v3);

        System.out.println(list);
        Collections.sort(list);      //sort by length
        System.out.println(list);

    }
}
