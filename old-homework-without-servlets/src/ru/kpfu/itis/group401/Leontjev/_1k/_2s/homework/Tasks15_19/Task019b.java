package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks15_19;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Alexander on 23.03.15.
 */

public class Task019b {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("in.txt"));
        ArrayList<Integer> list = new ArrayList<Integer>();
        String string = "";
        while (scanner.hasNextLine()) {
            string = scanner.nextLine();
            String[] array = string.split(" ");

            for (String a : array) {
                list.add(Integer.parseInt(a));
            }
        }
        System.out.println(list);
        Collections.sort(list, byLength);
        System.out.println(list);

        PrintWriter printWriter = new PrintWriter(new FileOutputStream("outputFile.txt"));

        Iterator<Integer> i = list.iterator();
        while (i.hasNext()) {
            printWriter.print(i.next());
            printWriter.print(" ");
        }

        printWriter.close();
    }

    static int getLength(int i) {
        int count = 0;
        while (i > 0) {
            count++;
            i /= 10;
        }
        return count;
    }

    static Comparator<Integer> byLength = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return (int) Math.signum(getLength(o1) - getLength(o2));
        }
    };
}
