package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks15_19;

import ru.kpfu.itis.group401.Leontjev._1k._1s.homework.Task049.Vector2D;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Alexander on 23.03.15.
 */

public class Task019c {
    public static void main(String[] args) {
        ArrayList<Vector2D> list = new ArrayList<Vector2D>();
        Vector2D v1 = new Vector2D(3, 4);
        Vector2D v2 = new Vector2D(1, 3);
        Vector2D v3 = new Vector2D(1, 1);
        Vector2D v4 = new Vector2D(1, 3);

        list.add(v1);
        list.add(v2);
        list.add(v3);
        list.add(v4);


        System.out.println(list);
        list.sort(byLexicographical);
        System.out.println(list);
    }

    static Comparator<Vector2D> byLexicographical = new Comparator<Vector2D>() {
        @Override
        public int compare(Vector2D o1, Vector2D o2) {
            if (o1.getX() == o2.getX()) {
                if (o1.getY() == o2.getY()) {
                    return 0;
                }
                if (o1.getY() > o2.getY()) {
                    return 1;
                } else {
                    return -1;
                }

            } else {
                if (o1.getX() > o2.getX()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    };
}
