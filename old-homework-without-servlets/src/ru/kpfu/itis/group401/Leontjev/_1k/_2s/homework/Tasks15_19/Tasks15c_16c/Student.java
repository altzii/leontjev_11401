package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks15_19.Tasks15c_16c;

import java.util.ArrayList;

/**
 * Created by Alexander on 17.03.15.
 */

public class Student {
    private String name;
    private ArrayList<String> results = new ArrayList<String>();

    public Student() {
    }

    public Student(String[] string) {
        this.name = string[0].trim();
        int i = 1;

        while (i < string.length) {
            this.results.add(string[i]);
            i++;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getResults() {
        return results;
    }

    public void setResults(ArrayList<String> results) {
        this.results = results;
    }

    public String toString() {
        return this.name;
    }

    public void printResults() {
        System.out.println(this.name + " " + this.results);
    }

    public void addData(String[] arrayData) {
        int i = 1;
        while (i < arrayData.length) {
            this.results.add(arrayData[i]);
            i++;
        }
    }


    public void addBlankTasks(int delta) {
        for (int i = 0; i < delta; i++) {
            results.add(" ");
        }
    }
}
