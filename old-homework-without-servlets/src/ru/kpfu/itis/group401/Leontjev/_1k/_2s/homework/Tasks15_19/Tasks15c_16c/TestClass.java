package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks15_19.Tasks15c_16c;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Alexander on 17.03.15.
 */

public class TestClass {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("11401.txt"));
        HashSet<Student> students = new HashSet<Student>();
        ArrayList<String> numTasks = new ArrayList<String>();
        int currentTasksNum = 0;

        String string;

        scanner.nextLine();
        scanner.nextLine();

        while (scanner.hasNextLine()) {
            string = scanner.nextLine().trim();
            String[] arrayData = string.split("\t");
            if (isInfo(arrayData[0])) {
                int delta = currentTasksNum - (arrayData.length - 1);
                //we have students here
                Student st = getStudentByLastName(students, arrayData[0]);
                if (st != null) {
                    st.addData(arrayData);
                    //students exists

                } else {
                    //student does not exists. Creating Student
                    st = new Student(arrayData);
                    students.add(st);
                }
                st.addBlankTasks(delta);

            } else {
                for (String number : arrayData) {
                    numTasks.add(number.trim());
                    currentTasksNum = arrayData.length;
                }
                //numbers of tasks
            }
        }

        printResults(students);
        System.out.printf("\n");

        Map<Student, Integer> map = new HashMap<Student, Integer>(); //количество решенных каждым студентом задач

        int result = 0;
        for (Student student : students) {
            for (String point : student.getResults()) {
                if (point.equals("1")) {
                    result++;
                }
            }
            map.put(student, result);
            result = 0;
        }
        System.out.println(map);

        Map<Student, Double> map2 = new HashMap<Student, Double>(); //средний балл студента по всем задачам

        for (Student student : students) {
            for (String point : student.getResults()) {
                if (point.equals("1")) {
                    result++;
                }

                if (point.equals("0.5")) {
                    result += 0.5;
                }
            }
            map2.put(student, (double) result / numTasks.size());
            result = 0;
        }
        System.out.println(map2);

        Map<String, Integer> map3 = new HashMap<String, Integer>(); //количество студентов, решивших каждую задачу
        int[] array = new int[numTasks.size()];

        int i = 0;

        for (Student student : students) {
            for (String point : student.getResults()) {
                if (point.equals("1")) {
                    array[i]++;
                }
                i++;
            }
            i = 0;
        }

        for (String numTask : numTasks) {
            map3.put(numTask, array[i]);
            i++;
        }
        System.out.println(map3);

        Map<String, Double> map4 = new HashMap<String, Double>(); //средний балл по каждой решенной задаче


        for (i = 0; i < array.length; i++) {
            array[i] = 0;
        }

        i = 0;
        for (Student student : students) {
            for (String point : student.getResults()) {
                if (point.equals("1")) {
                    array[i]++;
                }

                if (point.equals("0.5")) {
                    array[i] += 0.5;
                }
                i++;
            }
            i = 0;

        }

        for (String numTask : numTasks) {
            map4.put(numTask, (double) array[i] / students.size());
            i++;
        }

        System.out.println(map4);
    }

    public static boolean isInfo(String string) {
        return string.length() != 0 && ((string.charAt(0) <= 'Я') && (string.charAt(0) >= 'А') || (string.charAt(0) == 'Ё'));
    }

    public static Student getStudentByLastName(HashSet<Student> students, String lastName) {
        Iterator<Student> i = students.iterator();
        while (i.hasNext()) {
            Student tmp = i.next();
            if (tmp.getName().equals(lastName.trim())) {
                return tmp;
            }
        }
        return null;
    }

    public static void printResults(HashSet<Student> students) {
        for (Student student : students) {
            student.printResults();
        }
    }
}

