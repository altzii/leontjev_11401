package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22;

import java.util.*;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task021a
 */

public class MyTreeSet<T extends Comparable<T>> implements Set<T> {
    int size;
    Node<T> root;
    int i;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Queue<Node<T>> queue = new LinkedList<Node<T>>();
        queue.offer(this.root);

        while (!queue.isEmpty()) {
            Node<T> p = queue.poll();

            if (p.getLeft() != null) {

                queue.offer(p.getLeft());
            }

            if (p.getRight() != null) {
                queue.offer(p.getRight());
            }

            if (p.getValue().equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        i = 0;
        Iterator<T> iterator = new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return i < size() - 1;
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                } else {
                    return (T) toArray()[i++];
                }
            }
        };
        return null;
    }


    @Override
    public Object[] toArray() {
        Object[] array = new Object[this.size];
        int i = 0;

        Queue<Node<T>> queue = new LinkedList<Node<T>>();
        queue.offer(this.root);

        while (!queue.isEmpty()) {
            Node<T> p = queue.poll();

            if (p.getLeft() != null) {

                queue.offer(p.getLeft());
            }

            if (p.getRight() != null) {
                queue.offer(p.getRight());
            }
            array[i] = p.getValue();
            i++;
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (!this.contains(t)) {
            Node<T> node = new Node<T>(t);
            if (root == null) {
                root = node;
                this.size++;
                return true;
            } else {
                root.add(t);
                this.size++;
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        if (this.contains(o)) {
            Node<T> node = root;
            Node<T> parent = new Node<T>();

            while (node.getValue().compareTo((T) o) != 0) {

                if (node.getValue().compareTo((T) o) == 1) {
                    if (node.getLeft() != null) {
                        parent = node;
                        node = node.getLeft();
                    }
                } else {
                    if (node.getRight() != null) {
                        parent = node;
                        node = node.getRight();
                    }
                }
            }

            Node<T> node2 = node;

            if (node2.getRight() == null) {
                if (node2.getLeft() == null) {
                    if (parent.getLeft() == node) {
                        parent.setLeft(null);
                    } else {
                        parent.setRight(null);
                    }
                } else {
                    parent.setLeft(parent.getLeft());
                }
            } else {
                node2 = node2.getRight();

                if (node2.getLeft() == null) {
                    parent.setRight(node2);
                } else {
                    while (node2.getLeft().getLeft() != null) {
                        node2 = node2.getLeft();
                    }
                    node.setValue(node2.getLeft().getValue());
                    node2.setLeft(node2.getLeft().getRight());
                }
            }
            this.size--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T o : c) {
            if (!this.add(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean flag;
        boolean flag1 = false;
        for (T o : this) {
            flag = !c.contains(o);
            flag1 |= flag;
            if (flag) {
                this.remove(o);
            }
        }
        return flag1;
    }


    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.remove(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void clear() {
        this.size = 0;
        this.root = null;
    }
}
