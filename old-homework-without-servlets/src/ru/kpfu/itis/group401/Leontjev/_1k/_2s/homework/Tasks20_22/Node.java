package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22;

/**
 * @author Alexander Leontjev
 *         11-401
 *         class Node for Task020c
 */

public class Node<T extends Comparable<T>> implements Comparable<Node<T>> {
    protected boolean visited;

    public void setValue(T value) {
        this.value = value;
    }

    protected T value;

    public void setRight(Node<T> right) {
        this.right = right;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
    }

    protected Node<T> left, right;

    public Node(T t) {
        this.value = t;
    }

    public Node(boolean visited) {
        this.visited = visited;
    }

    public Node(Object o) {
    }

    public Node() {

    }

    public boolean add(T t) {
        Node<T> node = new Node<T>(t);

        if (t.compareTo(this.value) == -1) {
            if (this.left != null) {
                this.left.add(t);
            } else this.left = node;
        } else if (this.right != null) {
            this.right.add(t);
        } else this.right = node;
        return true;
    }


    public void visited() {
        visited = true;

    }

    public boolean isVisited() {
        return visited;

    }

    public Node(T i, Node<T> left, Node<T> right) {
        this.value = i;
        this.left = left;
        this.right = right;
    }

    public T getValue() {
        return value;
    }

    public Node<T> getLeft() {
        return left;
    }

    public Node<T> getRight() {
        return right;
    }

    @Override
    public int compareTo(Node<T> o) {
        return this.value.compareTo(o.value);
    }
}