package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22.Task020c.Tree;

import java.util.Stack;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task020a
 */

public class Task020a {
    public static void lpk(Tree tree) {
        Stack<Node<Integer>> stack = new Stack<Node<Integer>>();
        Node<Integer> p = tree.getRoot();
        stack.push(p);

        while (!stack.isEmpty()) {
            p = stack.pop();
            if (!p.isVisited()) {
                p.visited();
                stack.push(p);
                if (p.getRight() != null) {
                    stack.push(p.getRight());
                }
                if (p.getLeft() != null) {
                    stack.push(p.getLeft());
                }
            } else {
                System.out.println(p.getValue());

            }
        }
    }

    public static void main(String[] args) {
        Tree t = new Tree(6);
        t.print();
        lpk(t);
    }

}
