package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22.Task020c.Tree;

import java.util.Stack;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task020b
 */

public class Task020b {
    public static void lkp(Tree tree) {
        Stack<Node<Integer>> stack = new Stack<Node<Integer>>();
        Node<Integer> p = tree.getRoot();

        while (p != null || !stack.isEmpty()) {
            if (p != null) {
                if (!p.isVisited()) {
                    stack.push(p);
                    p.visited();
                    p = p.getLeft();
                } else {
                    System.out.println(p.getValue());
                    p = p.getRight();
                }
            } else {
                p = stack.pop();
            }
        }
    }

    public static void main(String[] args) {
        Tree t = new Tree(6);
        t.print();
        lkp(t);
    }
}
