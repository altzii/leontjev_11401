package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22.Task020c;
import static ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22.Task020c.Tree.mult;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task020c
 */

public class TestClass {
    public static void main(String[] args) {
        Tree t = new Tree(6);
        t.print();
        System.out.println("mult = " + mult(t.getRoot()));
        System.out.println("sum = " + t.sum());
        System.out.println("max = " + t.max());

    }
}
