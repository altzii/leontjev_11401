package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22.Task020c;

import ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks20_22.Node;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task020c
 */

public class Tree {
    private Node<Integer> root;

    public Node<Integer> getRoot() {
        return root;
    }



    public Tree(int n) {
        root = getBalancedTree(n);
    }

    private static Node<Integer> getBalancedTree(int n) {
        if (n == 0) {
            return null;
        } else {
            int nl = n / 2;
            int nr = n - nl - 1;
            return new Node<Integer>(
                    n,
                    getBalancedTree(nl),
                    getBalancedTree(nr)
            );
        }
    }

    public void print() {
        printTree(root, 0);
    }

    private static void printTree(Node p, int level) {
        if (p != null) {
            printTree(p.getRight(), level + 1);
            for (int i = 1; i <= 2 * level; i++)
                System.out.print(" ");
            System.out.println(p.getValue());
            printTree(p.getLeft(), level + 1);
        }
    }

    public int sum() {
        Queue<Node<Integer>> queue = new LinkedList<Node<Integer>>();
        queue.offer(this.root);
        int sum = 0;

        while (!queue.isEmpty()) {
            Node<Integer> p = queue.poll();

            if (p.getLeft() != null) {

                queue.offer(p.getLeft());
            }

            if (p.getRight() != null) {
                queue.offer(p.getRight());
            }

            sum += p.getValue();
        }
        return sum;
    }

    public static int mult(Node<Integer> node) {
        if (node != null) {
            return node.getValue() * mult(node.getLeft()) * mult(node.getRight());
        } else return 1;
    }


    public int max() {
        int max;
        max = root.getValue();

        Stack<Node<Integer>> stack = new Stack<Node<Integer>>();
        Node<Integer> p = root;
        while (p != null || !stack.isEmpty()) {
            if (p != null) {
                if (p.getValue() > max) {
                    max = p.getValue();
                }
                if (p.getRight() != null) {
                    stack.push(p.getRight());
                }
                p = p.getLeft();
            } else {
                p = stack.pop();
            }
        }
        return max;
    }
}