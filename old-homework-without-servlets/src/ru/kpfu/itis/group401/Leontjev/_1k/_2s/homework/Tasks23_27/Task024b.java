package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks23_27;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task024b
 */

public class Task024b {
    public static boolean isCorrect(String string, int k) {
        int num = Integer.parseInt(string);
        int sum = 0;

        while (num != 0) {
            sum += num % 10;
            num /= 10;
        }
        return sum < k;
    }

    public static void backTracking(String string, int m, int k) {
        if (string.length() == m) {
            System.out.println(string);
        } else {
            for (char i = '0'; i <= '9'; i++) {
                if (isCorrect(string + i, k)) {
                    backTracking(string + i, m, k);
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int m = scanner.nextInt();
        for (char s = '1'; s <= '9'; s++) {
            backTracking(Character.toString(s), k, m);
        }
    }
}

