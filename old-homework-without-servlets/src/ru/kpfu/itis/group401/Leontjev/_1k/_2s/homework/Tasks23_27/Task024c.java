package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks23_27;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task024c
 */

public class Task024c {
    public static boolean isVowel(char c) {
        return ((c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u') || (c == 'y'));
    }

    public static void backTracking(String s, int n, int count) {
        if (count == 3) {
            return;
        }

        if (s.length() == n) {
            {
                System.out.println(s);
            }
        } else {
            for (char i = 'a'; i <= 'z'; i++) {
                if (isVowel(i)) {
                    count++;
                    backTracking(s + i, n, count);
                    count--;
                } else {
                    backTracking(s + i, n, count);
                }
            }
        }
    }

    public static void main(String[] args) {
        for (char s = 'a'; s <= 'z'; s++) {
            backTracking(Character.toString(s), 4, 0);
        }
    }
}
