package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks23_27;

import java.io.*;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task026c
 */

public class Task026c {
    public static void main(String[] args) throws IOException {
        //FileReader - FileWriter
        double t1 = System.nanoTime();
        FileReader fileReader = new FileReader("in.txt");
        FileWriter fileWriter = new FileWriter("out2.txt");

        int p = 0;
        while (p != -1) {
            p = fileReader.read();
            if (p != -1) {
                fileWriter.write(p);
            }
        }
        fileReader.close();
        fileWriter.close();
        double t2 = System.nanoTime();
        System.out.println("FileReader - FileWriter = " + (t2 - t1));

        //BufferedReader - PrintWriter
        t1 = System.nanoTime();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("in.txt"));
        PrintWriter printWriter = new PrintWriter(new FileWriter("out3.txt"));

        String q = "";
        while (q != null) {
            q = bufferedReader.readLine();
            if (q != null) {
                printWriter.write(q + "\n");
                printWriter.flush();
            }
        }
        bufferedReader.close();
        printWriter.close();
        t2 = System.nanoTime();
        System.out.println("BufferedReader - PrintWriter = " + (t2 - t1));
    }
}
