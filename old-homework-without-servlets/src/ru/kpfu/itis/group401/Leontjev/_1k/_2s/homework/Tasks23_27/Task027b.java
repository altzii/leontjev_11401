package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks23_27;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task027b
 */

public class Task027b {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();

        ArrayList<TimeThread> timeThreads = new ArrayList<TimeThread>();
        int i = 1;

        while (i <= n) {
            String[] information = scanner.nextLine().split(" ");
            int value = 0;
            int k = 1;

            if (information[1].charAt(0) != '0') {
                if (information[1].charAt(0) == '+') {
                    for (int j = information[1].length() - 1; j > 0; j--) {
                        value += Integer.parseInt(String.valueOf(information[1].charAt(j))) * k;
                        k *= 10;
                    }
                } else {
                    for (int j = information[1].length() - 1; j > 0; j--) {
                        value += Integer.parseInt(String.valueOf(information[1].charAt(j))) * k;
                        k *= 10;
                    }
                    value *= -1;
                }
            } else {
                value = 0;
            }
            TimeThread timeThread = new TimeThread(information[0], value);
            timeThreads.add(timeThread);
            i++;
        }

        for (TimeThread timeThread : timeThreads) {
            timeThread.go();
        }
    }
}


