package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks23_27;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Alexander Leontjev
 *         11-401
 *         class TimeThread
 */

public class TimeThread implements Runnable {
    Thread thread;
    int value;
    String city;

    @Override
    public void run() {
        for (int i = 0; i < 123456789; i++) {
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

            int time;
            if ((calendar.get(Calendar.HOUR_OF_DAY) + value < 0)) {
                time = (calendar.get(Calendar.HOUR_OF_DAY) + value) + 24;
            } else {
                if ((calendar.get(Calendar.HOUR_OF_DAY) + value > 24)) {
                    time = (calendar.get(Calendar.HOUR_OF_DAY) + value) - 24;
                } else {
                    time = calendar.get(Calendar.HOUR_OF_DAY) + value;
                }
            }

            System.out.println(city + ": " + time + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
            this.sleep(1000);
        }
    }


    public TimeThread(String city, int value) {
        thread = new Thread(this);
        this.city = city;
        this.value = value;

    }

    public void go() {
        thread.start();
    }

    public void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}



