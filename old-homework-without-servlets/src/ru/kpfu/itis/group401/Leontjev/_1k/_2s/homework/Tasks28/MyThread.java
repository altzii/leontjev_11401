package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks28;

/**
 * @author Alexander Leontjev
 *         11-401
 *         class MyThread for Task028b
 */

public class MyThread implements Runnable {
    int[] array;
    Thread thread;
    int from;
    int to;
    int sum;


    @Override
    public void run() {
        for (int i = from; i < to; i++) {
            sum += array[i];
        }
    }

    public MyThread(int[] array, int from, int to) {
        thread = new Thread(this);
        this.array = array;
        this.from = from;
        this.to = to;
        thread.start();
    }

    public int getSum() {
        return sum;
    }


    public void join() {
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
