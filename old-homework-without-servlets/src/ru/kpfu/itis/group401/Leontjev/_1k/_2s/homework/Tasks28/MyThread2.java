package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks28;

/**
 * @author Alexander Leontjev
 *         11-401
 *         class MyThread2 for Task028c
 */

public class MyThread2 implements Runnable {
    int[] array;
    Thread thread;

    int sum;

    public int getSum() {
        return sum;
    }

    @Override
    public void run() {
        for (int i = array.length / 2; i < array.length; i++) {
            sum += array[i];
        }

    }

    public MyThread2(int[] array) {
        thread = new Thread(this);
        this.array = array;
        thread.start();
    }

    public void join() {
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
