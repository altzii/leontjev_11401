package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks28;

import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task028b
 */

public class Task028b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];


        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        int k = scanner.nextInt();
        int[] sizes = new int[k + 1];
        sizes[0] = 0;

        //  n/k + 1     -     n % k потоков
        //  n/k         -     k - l - потоков

        int i = 1;

        while (i <= n % k) {
            sizes[i] = (n / k + 1) * i;
            i++;
        }

        while (i <= k) {
            sizes[i] = sizes[i - 1] + n / k;
            i++;
        }

        System.out.println("Интервалы:");
        for (int s : sizes) {
            System.out.println(s);
        }

        i = 0;
        int sum = 0;

        while (i < k) {
            MyThread thread = new MyThread(array, sizes[i], sizes[i + 1]);
            thread.join();
            sum += thread.getSum();
            i++;
        }

        System.out.println("sum = " + sum);
    }
}
