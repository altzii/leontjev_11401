package ru.kpfu.itis.group401.Leontjev._1k._2s.homework.Tasks28;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task028c
 */

public class Task028c {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 5, 6, 7};

        MyThread1 t1 = new MyThread1(array);
        MyThread2 t2 = new MyThread2(array);

        t1.join();
        t2.join();

        System.out.println(t1.getSum() + t2.getSum());
    }
}
