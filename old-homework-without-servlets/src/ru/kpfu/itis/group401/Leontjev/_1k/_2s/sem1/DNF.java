package ru.kpfu.itis.group401.Leontjev._1k._2s.sem1;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Alexander on 23.02.15.
 */

public class DNF implements Cloneable {
    private Konj head;
    private int size = 0;

    public DNF() {
    }

    public DNF(String s) {
        String[] str = s.split("V");
        for (String k : str) {
            Konj konj = new Konj();
            this.size++;
            String[] str2 = k.split("&");
            for (String p : str2) {
                if (p.charAt(0) == '-') {
                    konj.add(-Character.getNumericValue(p.charAt(2)));
                } else {
                    konj.add(Character.getNumericValue(p.charAt(1)));
                }
            }
            konj.setNext(this.head);
            this.head = konj;
        }
    }

    public String toString() {
        Konj konj = this.head;
        String str = "";
        while (konj != null) {
            if (konj.size() != 0) {
                str = str + konj + " V ";
            }
            konj = konj.getNext();
        }
        if (this.size != 0) {
            str = str.substring(0, str.length() - 3);
        }
        return str;
    }


    public void insert(Konj k) {
        if (!(this.containsKonj(k))) {
            k.setNext(this.head);
            this.head = k;
            this.size++;
        }
    }

    public boolean containsKonj(Konj k) {
        Konj konj;
        konj = this.head;
        while (konj != null) {
            if (konj.equals(k)) {
                return true;
            } else konj = konj.getNext();
        }
        return false;
    }

    public DNF dnfWith(int i) {
        Konj konj = this.head;
        DNF dnf = new DNF();
        while (konj != null) {
            if (konj.contains(i)) {
                dnf.insert((Konj) konj.clone());
            }
            konj = konj.getNext();
        }
        return dnf;
    }

    public int getSize() {
        return this.size;
    }

    public DNF disj(DNF d) throws CloneNotSupportedException {
        DNF dnf;
        dnf = (DNF) this.clone();
        Konj konj = d.head;
        while (konj != null) {
            Konj konj1;
            konj1 = (Konj) konj.clone();
            konj1.setNext(null);
            dnf.insert(konj1);
            konj = konj.getNext();
        }
        return dnf;
    }

    public void sortByLength() {
        ArrayList<Konj> konjArray = new ArrayList<Konj>();
        int n = 0;
        Konj konj = this.head;

        while (n < this.size) {
            konjArray.add((Konj) konj.clone());
            konj = konj.getNext();
            n++;
        }
        Collections.sort(konjArray);

        this.head = null;

        n = 0;
        while (n < konjArray.size()) {
            this.insert(konjArray.get(n));
            n++;
        }

    }

    public boolean value(boolean[] v) {
        boolean b = false;
        Konj konj = this.head;

        while (!b & konj != null) {
            b = konj.value(v);
            konj = konj.getNext();
        }
        return b;
    }
}


