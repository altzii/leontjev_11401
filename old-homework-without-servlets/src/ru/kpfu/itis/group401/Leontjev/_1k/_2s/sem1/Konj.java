package ru.kpfu.itis.group401.Leontjev._1k._2s.sem1;

import java.util.ArrayList;

/**
 * Created by Alexander on 23.02.15.
 */

public class Konj extends ArrayList<Integer> implements Comparable<Konj> {
    private Konj next;

    public Konj getNext() {
        return next;
    }

    public void setNext(Konj next) {
        this.next = next;
    }

    public String toString() {
        String str = "";
        for (Integer integer : this) {
            if (integer > 0) {
                str = str + "X" + integer + "&";
            } else {
                str = str + "-X" + -integer + "&";
            }
        }
        if (this.size() != 0) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @Override
    public int compareTo(Konj konj) {
        return (int) -Math.signum(this.size() - konj.size());
    }

    public boolean value(boolean[] v) {
        boolean b = true;
        int i = 0;
        while (b & i < this.size()) {
            if (this.get(i) > 0) {
                b = v[this.get(i) - 1];
            } else {
                b = !v[Math.abs(this.get(i)) - 1];
            }
            i++;
        }
        return b;
    }
}


