package ru.kpfu.itis.group401.Leontjev._1k._2s.sem1;

/**
 * Created by Alexander on 23.02.15.
 */

public class TestClass {
    public static void main(String[] args) throws CloneNotSupportedException {
        String s = "X1&X4VX3&X4VX1&X2&X4";
        DNF dnf = new DNF(s);

        System.out.println(dnf);
        System.out.println(dnf.getSize());

        String s1 = "X1&X2&X4VX1&X2&X5VX1&X2&X3VX3&X4VX1&X6";
        DNF dnf2 = new DNF(s1);

        System.out.println(dnf2);
        System.out.println(dnf2.getSize());

        DNF dnf3;
        dnf3 = dnf.disj(dnf2);
        System.out.println(dnf3);
        System.out.println(dnf3.getSize());

        dnf3.sortByLength();
        System.out.println(dnf3);

        DNF dnf4;
        dnf4 = dnf2.dnfWith(2);
        System.out.println(dnf4);
        System.out.println(dnf4.getSize());


        dnf.sortByLength();
        System.out.println(dnf);

        Konj konj = new Konj();
        konj.add(1);
        konj.add(2);
        konj.add(3);
        konj.add(-4);

        boolean[] bool = new boolean[4];
        bool[0] = true;
        bool[1] = true;
        bool[2] = false;
        bool[3] = false;

        System.out.println(konj.value(bool));
        System.out.println(dnf.value(bool));
    }
}
