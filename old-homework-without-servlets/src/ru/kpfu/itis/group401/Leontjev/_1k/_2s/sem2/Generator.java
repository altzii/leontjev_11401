package ru.kpfu.itis.group401.Leontjev._1k._2s.sem2;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Generator for sem2
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class Generator {
    public static void main(String[] args) throws IOException {
        Random random = new Random();

        PrintWriter printWriter = new PrintWriter(new FileOutputStream("arraysData.txt"));
        for (int i = 100; i <= 10000; i += 50) {
            for (int j = 0; j < i; j++) {
                printWriter.write(Integer.toString(random.nextInt(i)) + " ");
            }
            printWriter.write('\n');
        }
        printWriter.close();
    }
}