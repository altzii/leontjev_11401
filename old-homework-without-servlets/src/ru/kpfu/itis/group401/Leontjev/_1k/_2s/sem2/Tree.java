package ru.kpfu.itis.group401.Leontjev._1k._2s.sem2;

/**
 * @author Alexander Leontjev
 *         11-401
 *         sem2
 */

class Tree {
    public Tree left;            // левое и правое поддеревья и ключ
    public Tree right;
    public int key;
    static int count = 1;

    public Tree(int k) {        // конструктор с инициализацией ключа
        key = k;
    }

    /*  insert - добавление нового ключа
        K - ключ добавляемого дерева
        X - ключ дерева, к которому добавляют
        Если K>=X, рекурсивно добавить новое дерево в правое поддерево.
        Если K<X, рекурсивно добавить новое дерево в левое поддерево.
    */

    public void insert(Tree tree) {
        count++;

        if (tree.key < this.key) {
            if (this.left != null) {
                this.left.insert(tree);
            } else this.left = tree;
        } else if (this.right != null) {
            this.right.insert(tree);
        } else this.right = tree;
    }

    /*  traverse - рекурсивный обход в порядке:
        обойти левое поддерево - обработать корень - обойти правое поддерево
    */

    public void traverse() {
        count++;

        if (left != null)
            left.traverse();

        //обработка корня

        if (right != null)
            right.traverse();
    }
}