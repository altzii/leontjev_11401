package ru.kpfu.itis.group401.Leontjev._1k._2s.sem2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Alexander Leontjev
 *         11-401
 *         sem2
 */

class TreeSort {
    public static void main(String args[]) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(new FileOutputStream("iteration.txt"));
        PrintWriter printWriter2 = new PrintWriter(new FileOutputStream("time.txt"));
        Scanner scanner = new Scanner(new File("arraysData.txt"));
        List<Integer> list = new ArrayList<Integer>();     //создаем List, в который будем считывать данные из файла

        while (scanner.hasNextLine()) {
            String[] data = scanner.nextLine().split(" ");
            for (String a : data) {                //считываем данные из файла и закидываем их List
                list.add(Integer.valueOf(a));
            }

            double t1 = System.nanoTime();
            int iter = treeSort(list);            //применяем метод сортировки на построенном List'е и измеряем время работы алгоритма
            double t2 = System.nanoTime();

            printWriter.write(iter + "\n");       //записываем измеренное количество итераций в файл
            printWriter2.write(t2 - t1 + "\n");   //записываем измеренное время работы алогритма в файл

            list.clear();
        }
        printWriter2.close();
        printWriter.close();
    }

    public static int treeSort(List<Integer> list) {  //метод сортировки, возвращающий кол-во итераций
        Tree tree = new Tree(list.get(0));   //создаем дерево, ключ корня которого - первый элемент List'а

        int i = 1;

        while (i < list.size()) {
            tree.insert(new Tree(list.get(i)));
            i++;
        }

        tree.traverse();     //применяем сам алгоритм сортировки дерева, который и заключается в обходе в порядке ЛКП

        int k = Tree.count;
        Tree.count = 1;      //инициализируем счетчик кол-ва итераций
        return k;            //возвращаем количество итераций

    }
}



