package ru.kpfu.itis.group401.Leontjev._2k._1s.classwork.app;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Alexander on 14/09/15.
 */

public class App {
    public static void tiGenerator(String param, int k) throws FileNotFoundException {

        File file = new File("src/konj.html");

        PrintWriter printWriter = new PrintWriter(file);
        printWriter.println("<table border>");
        printWriter.print("<tr>\n<td>x </td>\n<td>y</td>\n<td>x|y</td>\n</tr>");

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                printWriter.print("<tr>\n<td>" + i + "</td>\n<td>" + j + "</td>\n<td>");
                if (param.equals("ti/disj")) {
                    printWriter.print(i | j);
                } else {
                    printWriter.print(i & j);
                }
                printWriter.print("</td>\n</tr>");
            }
        }
        printWriter.print("</table>");
        printWriter.close();

    }
    public static void main(String[] args) throws FileNotFoundException {

        Scanner scanner = new Scanner(System.in);

        boolean exit = false;
        int k = 0;

        while (!exit) {
            String string = scanner.nextLine();

            if (string.equals("exit")) {
                exit = true;
            }

            if (string.equals("ti/disj")) {
                tiGenerator("ti/disj", k);
            }

            if (string.equals("ti/konj")) {
                tiGenerator("ti/konj", k);
            }

            if (string.equals("gsearch")) {
                System.out.println("gsearch");
            }
        }

    }
}
