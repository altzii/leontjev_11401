package ru.kpfu.itis.group401.Leontjev._2k._1s.homework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task001a
 */

public class Task001a {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[+-]?[0-9]\\.[0-9][1-9]*|[+-]?[1-9][0-9]*\\.?([1-9][0-9]*[1-9])?|0|[+-]?[0-9]\\.\\d*\\(\\d*\\)|[+-]?[1-9][0-9]*\\.\\d*\\(\\d*\\)");
        Matcher matcher = pattern.matcher("0.750");
        System.out.println(matcher.matches());
    }
}
