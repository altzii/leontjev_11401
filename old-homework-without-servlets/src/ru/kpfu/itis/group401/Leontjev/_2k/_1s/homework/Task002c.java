package ru.kpfu.itis.group401.Leontjev._2k._1s.homework;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task002c
 */

public class Task002c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите кол-во строк");
        int n = scanner.nextInt();
        String[] strings = new String[n];

        for (int i = 0; i < n; i++) {
            strings[i] = scanner.nextLine();
        }

        Pattern pattern = Pattern.compile("1+|0?(10)*1?|0+");

        for (int i = 0; i < n; i++) {
            Matcher matcher = pattern.matcher(strings[i]);
            if (matcher.matches()) {
                System.out.println(strings[i]);
            }
        }
    }
}
