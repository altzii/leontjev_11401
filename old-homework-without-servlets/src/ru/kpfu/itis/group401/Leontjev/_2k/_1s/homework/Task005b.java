package ru.kpfu.itis.group401.Leontjev._2k._1s.homework;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task005b
 */

public class Task005b {
    public static void main(String[] args) {
        Random random = new Random();
        int count = 0;
        int all = 0;
        Pattern pattern = Pattern.compile("(\\d*[02468]{2}\\d*[02468]{2}\\d*)+");

        while (count != 10) {
            all++;
            int number = random.nextInt(Integer.MAX_VALUE);
            String stringNumber = Integer.toString(number);
            Matcher matcher = pattern.matcher(stringNumber);


            if (matcher.matches()) {
                System.out.println(number);
                count++;
            }
        }

        System.out.println("Общее количество сгенерированных чисел: " + all + "\n");

        count = 0;
        all = 0;
        pattern = Pattern.compile("\\d*[02468]{2}\\d*[02468]{2}\\d*");

        while (count != 10) {
            all++;
            int number = random.nextInt(Integer.MAX_VALUE);
            String stringNumber = Integer.toString(number);
            Matcher matcher = pattern.matcher(stringNumber);

            if (matcher.find()) {
                System.out.println(number);
                count++;
            }
        }
        System.out.println("Общее количество сгенерированных чисел: " + all);

        Pattern pattern3 = Pattern.compile("(\\d*[02468]{2}\\d*[02468]{2}\\d*)+");
        Matcher matcher3 = pattern3.matcher("0276804");
        System.out.println(matcher3.matches());
    }
}
