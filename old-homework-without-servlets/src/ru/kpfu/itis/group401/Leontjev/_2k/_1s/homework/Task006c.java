package ru.kpfu.itis.group401.Leontjev._2k._1s.homework;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Alexander Leontjev
 *         11-401
 *         Task006c
 */

public class Task006c {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader("championat_hockey.html"));
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();

            Pattern pattern = Pattern.compile("(https?:\\/\\/)?(www\\.)?(((/([\\w-]+))+)*/([\\w-]+)\\.(\\w+))\\\"");
            Matcher matcher = pattern.matcher(s);

            while (matcher.find()) {
                System.out.println("FILENAME: " + matcher.group(7));
                System.out.println("EXTENSION: " + matcher.group(8));
            }
        }
    }
}
