package ru.kpfu.itis.group401.Leontjev._2k._1s.homework;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task007c
 */

public class Task007c {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader("championat_hockey.html"));
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();

            Pattern pattern = Pattern.compile("(https?:\\/\\/)?(www\\.)?(((/([\\w-]+))+)*/([\\w-]+))\\?(?<STRING>[\\w\\-=&%/:\\.;\\s'+\\(\\),]+)");
            Matcher matcher = pattern.matcher(s);
            Pattern pattern1 = Pattern.compile("(?<NAME>[\\w%\\.#]+)=(?<VALUE>[\\w%\\.#:]*)");

            while (matcher.find()) {
                System.out.println("PARAM STRING: " + matcher.group("STRING"));
                Matcher matcher1 = pattern1.matcher(matcher.group("STRING"));

                while (matcher1.find()) {
                    System.out.println("NAME: " + matcher1.group("NAME"));
                    System.out.println("VALUE: " + matcher1.group("VALUE"));
                }

            }
        }
    }
}
