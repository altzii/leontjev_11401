package ru.kpfu.itis.group401.Leontjev._2k._1s.homework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Task009b
 */

public class Task009b {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            File file = new File("src/ru/kpfu/itis/group401/Leontjev/_2k/_1s/homework/" + new Date().getTime() + ".html");
            PrintWriter printWriter = new PrintWriter(file);
            String request = scanner.nextLine();
            if (request.equals("/exit")) {
                file.delete();
                break;
            } else  {
                if (request.equals("/getdate")) {
                    printWriter.print(new Date().toString());
                    printWriter.close();
                    continue;
                }

                Pattern pattern = Pattern.compile("/(add|mult)/(\\d+)/(\\d+)");
                Matcher matcher = pattern.matcher(request);

                if (matcher.matches()) {
                    if (matcher.group(1).equals("add")) {
                        printWriter.print(Integer.parseInt(matcher.group(2)) + Integer.parseInt(matcher.group(3)));

                    } else {
                        printWriter.print(Integer.parseInt(matcher.group(2)) * Integer.parseInt(matcher.group(3)));
                    }
                    printWriter.close();
                    continue;
                }

                Pattern searchPattern = Pattern.compile("/(baidu|bing|yahoo|aol).com/search");
                Matcher searchMatcher = searchPattern.matcher(request);

                if (searchMatcher.matches()) {
                    HashMap<String, String > hashMap = new HashMap<String, String>();
                    hashMap.put("baidu", "wd");
                    hashMap.put("bing", "q");
                    hashMap.put("yahoo", "p");
                    hashMap.put("aol", "q");

                    printWriter.print("<!DOCTYPE html>\n" + "<html>\n" + "<head lang=\"en\">\n" + "<meta charset=\"utf-8\">\n" + "</head>\n" +
                    "<body>\n");

                    if (searchMatcher.group(1).equals("baidu")) {
                        printWriter.print("<form action=\"http://www.baidu.com/s?ie=utf-8&wd=\" method=\"GET\">");
                    }

                    if (searchMatcher.group(1).equals("bing")) {
                        printWriter.print("<form action=\"http://www.bing.com/search?q=f\" method=\"GET\">");
                    }

                    if (searchMatcher.group(1).equals("yahoo")) {
                        printWriter.print("<form action=\"https://search.yahoo.com/search?p=\" method=\"GET\">");
                    }

                    if (searchMatcher.group(1).equals("aol")) {
                        printWriter.print("<form action=\"http://search.aol.com/search?q=\" method=\"GET\">");
                    }
                    printWriter.print("\n<input type=\"text\" name=\"" + hashMap.get(searchMatcher.group(1)) +"\"/>");
                    printWriter.print("<input type=\"submit\" value=\"Search on " + searchMatcher.group(1) + "\"/>\n" + "</form>\n" + "</body>\n" +
                    "</html>");
                    printWriter.close();
                }
            }
            printWriter.print("404 / Not Found");
            printWriter.close();
        }
    }
}
