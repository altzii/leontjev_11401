package ru.kpfu.itis.group401.Leontjev._2k._1s.sem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Leontjev
 *         11-401
 *         Sem01
 */

public class Sem01 {
    public static void main(String[] args) {
        //E коды пищевых добавок
        Pattern patternE = Pattern.compile("E[- ]?[\\d]{3}([a-zA-Z]|[\\d])?");

        //Штрих-код продукта
        Pattern pattern = Pattern.compile("((\\d){13}|(\\d){8})");

        //Логин пользователя
        Pattern patternLogin = Pattern.compile("([a-zA-Z]?[a-zA-Z\\d_\\-.]*[a-zA-Z\\d_\\-]){6,20}");

    }
}
